QT += core quick quickcontrols2
CONFIG += c++11 debug

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/config/config.cpp \
        src/config/loadconfig.cpp \
        src/controller/categorycontroller.cpp \
        src/controller/clientcontroller.cpp \
        src/controller/logincontroller.cpp \
        src/controller/notoficationcontroller.cpp \
        src/controller/objectcontroller.cpp \
        src/controller/ordercontroller.cpp \
        src/core/core.cpp \
        src/entity/adressobjectentity.cpp \
        src/entity/clientobjectentity.cpp \
        src/entity/modiferlistobjectentity.cpp \
        src/entity/modiferobjectentity.cpp \
        src/entity/modiferwithnameobjectentity.cpp \
        src/entity/modificationgroupobjectentity.cpp \
        src/entity/modificationobjecentity.cpp \
        src/entity/orderentityobject.cpp \
        src/entity/paymentlist.cpp \
        src/entity/paymentobjectentity.cpp \
        src/entity/poductlistobjectentity.cpp \
        src/entity/product.cpp \
        src/entity/producthash.cpp \
        src/entity/productlist.cpp \
        src/entity/productobjectentity.cpp \
        src/entity/productofferdata.cpp \
        src/entity/productofferdatahash.cpp \
        src/model/clientlistmodel.cpp \
        src/model/modificationlistmodel.cpp \
        src/model/mordificationgrouplistmodel.cpp \
        src/model/newproductmodel.cpp \
        src/model/orderadedmodel.cpp \
        src/model/orderlistmodel.cpp \
        src/model/productinordermodel.cpp \
        src/model/productobjectmodel.cpp \
        src/model/statuslistmodel.cpp \
        src/model/saveorderlistpoxymodel.cpp \
        src/model/orderfiltervisiblemodel.cpp \
        src/router/router.cpp \
        src/entity/notification.cpp \
        main.cpp \
        src/entity/productincashier.cpp \
        src/model/categorylistmodel.cpp \
        src/model/notificationlistmodel.cpp \
        src/model/notificationproxymodel.cpp \
        src/model/orderproxymodel.cpp \
        src/model/productlistmodel.cpp \
        src/model/productlistproxymodel.cpp \
        src/service/menu/categoryservice.cpp \
        src/service/objectservice.cpp \
        src/service/servicelocator.cpp \
        src/service/staff/postservice.cpp \
        src/service/staff/shiftservice.cpp \
        src/service/staff/userservice.cpp \
        src/service/storageservice.cpp\

RESOURCES += qml.qrc


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../clientLibQt/ -lclientLibQt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../clientLibQt/debug/ -lclientLibQt
else:unix: LIBS += -L$$OUT_PWD/../clientLibQt/ -lclientLibQt
INCLUDEPATH += $$PWD/../clientLibQt
DEPENDPATH +=  $$PWD/../clientLibQt


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/config/config.h \
    src/config/loadconfig.h \
    src/controller/categorycontroller.h \
    src/controller/clientcontroller.h \
    src/controller/logincontroller.h \
    src/controller/notoficationcontroller.h \
    src/controller/objectcontroller.h \
    src/controller/ordercontroller.h \
    src/core/core.h \
    src/entity/adressobjectentity.h \
    src/entity/clientobjectentity.h \
    src/entity/modiferlistobjectentity.h \
    src/entity/modiferobjectentity.h \
    src/entity/modiferwithnameobjectentity.h \
    src/entity/modificationgroupobjectentity.h \
    src/entity/modificationobjecentity.h \
    src/entity/orderentityobject.h \
    src/entity/paymentlist.h \
    src/entity/paymentobjectentity.h \
    src/entity/poductlistobjectentity.h \
    src/entity/product.h \    
    src/entity/producthash.h \
    src/entity/productlist.h \
    src/entity/productobjectentity.h \
    src/entity/productofferdata.h \
    src/entity/productofferdatahash.h \
    src/model/clientlistmodel.h \
    src/model/orderfiltervisiblemodel.h\
    src/model/modificationlistmodel.h \
    src/model/mordificationgrouplistmodel.h \
    src/model/newproductmodel.h \
    src/model/orderadedmodel.h \
    src/model/productinordermodel.h \
    src/model/productobjectmodel.h \
    src/model/statuslistmodel.h \
    src/model/saveorderlistpoxymodel.h\
    src/model/orderfiltervisiblemodel.h \
    src/router/router.h \
    src/entity/notification.h \
    src/entity/productincashier.h \
    src/model/categorylistmodel.h \
    src/model/notificationlistmodel.h \
    src/model/notificationproxymodel.h \
    src/model/orderlistmodel.h \
    src/model/orderproxymodel.h \
    src/model/productlistmodel.h \
    src/model/productlistproxymodel.h \
    src/service/menu/categoryservice.h \
    src/service/objectservice.h \
    src/service/servicelocator.h \
    src/service/staff/postservice.h \
    src/service/staff/shiftservice.h \
    src/service/staff/userservice.h \
    src/service/storageservice.h

DISTFILES += \
    ../config.ini



