QT += core quick quickcontrols2
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/config/config.cpp \
        src/config/loadconfig.cpp \
        src/controller/categorycontroller.cpp \
        src/controller/logincontroller.cpp \
        src/controller/notoficationcontroller.cpp \
        src/controller/objectcontroller.cpp \
    src/controller/ordercontroller.cpp \
    src/controller/storagecontroller.cpp \
        src/core/core.cpp \
#        src/entity/Menu/category.cpp \
#        src/entity/Menu/categorylist.cpp \
#        src/entity/Menu/cost.cpp \
#        src/entity/Menu/costlist.cpp \
#        src/entity/Menu/dish.cpp \
#        src/entity/Menu/dishlist.cpp \
#        src/entity/Menu/goods.cpp \
#        src/entity/Menu/goodslist.cpp \
#        src/entity/Menu/ingredient.cpp \
#        src/entity/Menu/ingredientList.cpp \
#        src/entity/Menu/modifers.cpp \
#        src/entity/Menu/modification.cpp \
#        src/entity/Menu/modificationslist.cpp \
#        src/entity/Menu/prepack.cpp \
#        src/entity/Menu/prepacklist.cpp \
#        src/entity/Menu/unit.cpp \
#        src/entity/Menu/unitlist.cpp \
#        src/entity/Menu/workshop.cpp \
#        src/entity/Menu/workshoplist.cpp \
#        src/entity/clientinfo.cpp \
        src/entity/notification.cpp \
#        src/entity/order.cpp \
#        src/entity/orderlist.cpp \
        main.cpp \
    src/entity/productincashier.cpp \
    src/model/categorylistmodel.cpp \
        src/model/notificationlistmodel.cpp \
        src/model/notificationproxymodel.cpp \
        src/model/orderlistmodel.cpp \
        src/model/orderproxymodel.cpp \
    src/model/productlistmodel.cpp \
    src/model/productlistproxymodel.cpp \
        src/service/menu/categoryservice.cpp \
        src/service/objectservice.cpp \
        src/service/servicelocator.cpp \
    src/service/staff/postservice.cpp \
        src/service/staff/shiftservice.cpp \
        src/service/staff/userservice.cpp \
    src/service/storageservice.cpp

RESOURCES += qml.qrc


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../clientLibQt/ -lclientLibQt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../clientLibQt/debug/ -lclientLibQt
else:unix: LIBS += -L$$OUT_PWD/../clientLibQt/ -lclientLibQt
INCLUDEPATH += $$PWD/../clientLibQt
DEPENDPATH +=  $$PWD/../clientLibQt


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/config/config.h \
    src/config/loadconfig.h \
    src/controller/categorycontroller.h \
    src/controller/clientcontroller.h \
    src/controller/logincontroller.h \
    src/controller/notoficationcontroller.h \
    src/controller/objectcontroller.h \
    src/controller/ordercontroller.h \
    src/controller/storagecontroller.h \
    src/core/core.h \
#    src/entity/Menu/category.h \
#    src/entity/Menu/categorylist.h \
#    src/entity/Menu/cost.h \
#    src/entity/Menu/costlist.h \
#    src/entity/Menu/dish.h \
#    src/entity/Menu/dishlist.h \
#    src/entity/Menu/goods.h \
#    src/entity/Menu/goodslist.h \
#    src/entity/Menu/ingredient.h \
#    src/entity/Menu/ingredientList.h \
#    src/entity/Menu/modifers.h \
#    src/entity/Menu/modification.h \
#    src/entity/Menu/modificationslist.h \
#    src/entity/Menu/prepack.h \
#    src/entity/Menu/prepacklist.h \
#    src/entity/Menu/unit.h \
#    src/entity/Menu/unitlist.h \
#    src/entity/Menu/workshop.h \
#    src/entity/Menu/workshoplist.h \
#    src/entity/clientinfo.h \
    src/entity/notification.h \
#    src/entity/order.h \
#    src/entity/orderlist.h \
    src/entity/productincashier.h \
    src/model/categorylistmodel.h \
    src/model/notificationlistmodel.h \
    src/model/notificationproxymodel.h \
    src/model/orderlistmodel.h \
    src/model/orderproxymodel.h \
    src/model/productlistmodel.h \
    src/model/productlistproxymodel.h \
    src/service/menu/categoryservice.h \
    src/service/objectservice.h \
    src/service/servicelocator.h \
    src/service/staff/postservice.h \
    src/service/staff/shiftservice.h \
    src/service/staff/userservice.h \
    src/service/storageservice.h



