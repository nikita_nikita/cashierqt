#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include "src/core/core.h"
#include <QInputMethod>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    Core core;
    core.run();

    return app.exec();
}
