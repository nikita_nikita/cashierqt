#include "category.h"

Category::Category(QJsonObject jObj)
{
this->setData(jObj);
}

QString Category::getId() const
{
    return id;
}

void Category::setId(const QString &value)
{
    id = value;
}

QString Category::getName() const
{
    return name;
}

void Category::setName(const QString &value)
{
    name = value;
}

QString Category::getParent() const
{
    return parent;
}

void Category::setParent(const QString &value)
{
    parent = value;
}

int Category::getType() const
{
    return type;
}

void Category::setType(int value)
{
    type = value;
}

int Category::getSort() const
{
    return sort;
}

void Category::setSort(int value)
{
    sort = value;
}

QJsonObject Category::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["parent"] = this->parent;
    jObj["type"] = this->type;
    jObj["sort"] = this->sort;
    return  jObj;
}

void Category::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setParent(jObj.value("parent").toString());
    setType(jObj.value("type").toInt());
    setSort(jObj.value("sort").toInt());


}
