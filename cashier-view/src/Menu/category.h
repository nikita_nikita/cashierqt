#ifndef CATEGORY_H
#define CATEGORY_H
#include <QJsonObject>

class Category
{
public:
    Category(QJsonObject jObj);
    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getParent() const;
    void setParent(const QString &value);

    int getType() const;
    void setType(int value);

    int getSort() const;
    void setSort(int value);

    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString parent;
    int type;
    int sort;
};

#endif // CATEGORY_H
