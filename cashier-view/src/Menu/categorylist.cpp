#include "categorylist.h"

CategoryList::CategoryList(QJsonArray jArr)
{
this->setData(jArr);
}

QJsonArray CategoryList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Category categoty = this->at(i);
        jArr.append(categoty.toJson());
    }
    return  jArr;
}

void CategoryList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Category(jArr.at(i).toObject()));
    }
}
