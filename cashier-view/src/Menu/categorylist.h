#ifndef CATEGORYLIST_H
#define CATEGORYLIST_H
#include "category.h"
#include <QJsonArray>
#include <QList>
class CategoryList: QList<Category>
{
public:
    CategoryList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // CATEGORYLIST_H
