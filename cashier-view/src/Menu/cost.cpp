#include "cost.h"

Cost::Cost(QJsonObject jObj)
{
this->setData(jObj);
}

QString Cost::getProduct() const
{
    return product;
}

void Cost::setProduct(const QString &value)
{
    product = value;
}

QString Cost::getPoint() const
{
    return point;
}

void Cost::setPoint(const QString &value)
{
    point = value;
}

bool Cost::getVisible() const
{
    return visible;
}

void Cost::setVisible(bool value)
{
    visible = value;
}

int Cost::getPrice() const
{
    return price;
}

void Cost::setPrice(int value)
{
    price = value;
}

QJsonObject Cost::toJson()
{
    QJsonObject jObj;
    jObj["product"] = this->product;
    jObj["point"] = this->point;
    jObj["visible"] = this->visible;
    jObj["price"] = this->price;
    return  jObj;
}

void Cost::setData(QJsonObject jObj)
{
    setProduct(jObj.value("product").toString());
    setPoint(jObj.value("point").toString());
    setPrice(jObj.value("price").toInt());
    setVisible(jObj.value("visible").toBool());
}
