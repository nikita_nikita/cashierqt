﻿#ifndef COST_H
#define COST_H
#include <QJsonObject>

class Cost
{
public:
    Cost(QJsonObject jObj);
    QString getProduct() const;
    void setProduct(const QString &value);

    QString getPoint() const;
    void setPoint(const QString &value);

    bool getVisible() const;
    void setVisible(bool value);

    int getPrice() const;
    void setPrice(int value);

    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString product;
    QString point;
    bool visible;
    int price;
};

#endif // COST_H
