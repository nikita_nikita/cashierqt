#include "costlist.h"

CostList::CostList(QJsonArray jArr)
{
this->setData(jArr);
}

QJsonArray CostList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Cost cost = this->at(i);
        jArr.append(cost.toJson());
    }
    return  jArr;
}

void CostList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Cost(jArr.at(i).toObject()));
    }
}
