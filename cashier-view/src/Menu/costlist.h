#ifndef COSTLIST_H
#define COSTLIST_H
#include "cost.h"
#include <QJsonArray>
#include <QList>
class CostList:QList<Cost>
{
public:
    CostList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // COSTLIST_H
