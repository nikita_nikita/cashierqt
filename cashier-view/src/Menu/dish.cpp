#include "dish.h"

Dish::Dish(QJsonObject jObj)
{
this->setData(jObj);
}

QString Dish::getId() const
{
    return id;
}

void Dish::setId(const QString &value)
{
    id = value;
}

QString Dish::getName() const
{
    return name;
}

void Dish::setName(const QString &value)
{
    name = value;
}

QString Dish::getDescriprion() const
{
    return descriprion;
}

void Dish::setDescriprion(const QString &value)
{
    descriprion = value;
}

QString Dish::getUnit() const
{
    return unit;
}

void Dish::setUnit(const QString &value)
{
    unit = value;
}

QString Dish::getCategory() const
{
    return category;
}

void Dish::setCategory(const QString &value)
{
    category = value;
}

QString Dish::getWorkshop() const
{
    return workshop;
}

void Dish::setWorkshop(const QString &value)
{
    workshop = value;
}

QString Dish::getImage() const
{
    return image;
}

void Dish::setImage(const QString &value)
{
    image = value;
}

QString Dish::getColor() const
{
    return color;
}

void Dish::setColor(const QString &value)
{
    color = value;
}

QString Dish::getBarcode() const
{
    return barcode;
}

void Dish::setBarcode(const QString &value)
{
    barcode = value;
}

bool Dish::getWeight_flag() const
{
    return weight_flag;
}

void Dish::setWeight_flag(bool value)
{
    weight_flag = value;
}

QString Dish::getTimeForPreparing() const
{
    return timeForPreparing;
}

void Dish::setTimeForPreparing(const QString &value)
{
    timeForPreparing = value;
}

int Dish::getSort() const
{
    return sort;
}

void Dish::setSort(int value)
{
    sort = value;
}

IngredientList Dish::getIngredients() const
{
    return ingredients;
}

void Dish::setIngredients(const IngredientList &value)
{
    ingredients = value;
}

//QJsonArray Dish::getModificationgroup() const
//{
//    return modificationgroup;
//}

//void Dish::setModificationgroup(const QJsonArray &value)
//{
//    modificationgroup = value;
//}

QJsonObject Dish::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["sort"] = this->sort;
    jObj["unit"] = this->unit;
    jObj["color"] = this->color;
    jObj["image"] = this->image;
    jObj["barcode"] = this->barcode;
    jObj["category"] = this->category;
    jObj["workshop"] = this->workshop;
    jObj["descriprion"] = this->descriprion;
    jObj["ingredients"] = this->ingredients.toJson();
    jObj["time_for_preparing"] = this->timeForPreparing;
    jObj["modificationgroup"] = this->modificationgroup;
    return jObj;

}

void Dish::setData(QJsonObject jObj)
{
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["sort"] = this->sort;
    jObj["unit"] = this->unit;
    jObj["color"] = this->color;
    jObj["image"] = this->image;
    jObj["barcode"] = this->barcode;
    jObj["category"] = this->category;
    jObj["workshop"] = this->workshop;
    jObj["descriprion"] = this->descriprion;
    jObj["ingredients"] = this->ingredients.toJson();
    jObj["modificationgroup"] = this->modificationgroup;
    jObj["time_for_preparing"] = this->timeForPreparing;

    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setSort(jObj.value("sort").toInt());
    setUnit(jObj.value("unit").toString());
    setColor(jObj.value("color").toString());
    setImage(jObj.value("image").toString());
    setBarcode(jObj.value("barcode").toString());
    setWorkshop(jObj.value("workshop").toString());
    setDescriprion(jObj.value("descriprion").toString());
    setIngredients(jObj.value("ingredients").toArray());
    setTimeForPreparing(jObj.value("time_for_preparing").toString());
//    setModificationgroup(jObj.value("modificationgroup").toArray());

}

//QJsonArray Dish::getModificationgroup() const
//{
//    return modificationgroup;
//}

//void Dish::setModificationgroup(const QJsonArray &value)
//{
//    modificationgroup = value;
//}
