#ifndef DISH_H
#define DISH_H
#include <QTime>
#include <QJsonObject>
#include "ingredientList.h"
class Dish
{
public:
    Dish(QJsonObject jObj);
    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getDescriprion() const;
    void setDescriprion(const QString &value);

    QString getUnit() const;
    void setUnit(const QString &value);

    QString getCategory() const;
    void setCategory(const QString &value);

    QString getWorkshop() const;
    void setWorkshop(const QString &value);

    QString getImage() const;
    void setImage(const QString &value);

    QString getColor() const;
    void setColor(const QString &value);

    QString getBarcode() const;
    void setBarcode(const QString &value);

    bool getWeight_flag() const;
    void setWeight_flag(bool value);

    QString getTimeForPreparing() const;
    void setTimeForPreparing(const QString &value);

    int getSort() const;
    void setSort(int value);

    IngredientList getIngredients() const;
    void setIngredients(const IngredientList &value);

//    QJsonArray getModificationgroup() const;
//    void setModificationgroup(const QJsonArray &value);
    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString descriprion;
    QString unit;
    QString category;
    QString workshop;
    QString image;
    QString color;
    QString barcode;
    bool weight_flag;
    QString timeForPreparing;
    int sort;
    IngredientList ingredients;
    QJsonArray modificationgroup;

};

#endif // DISH_H
