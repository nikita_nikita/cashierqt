#include "dishlist.h"

DishList::DishList()
{

}

DishList::DishList(QJsonArray jArr)
{
    this->setData(jArr);
}

QJsonArray DishList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Dish dish = this->at(i);
        jArr.append(dish.toJson());
    }
    return  jArr;
}


void DishList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
    this->append(Dish(jArr.at(i).toObject()));
}
}
