#ifndef DISHLIST_H
#define DISHLIST_H

#include <QList>
#include "dish.h"

class DishList:QList<Dish>
{
public:
    DishList();
    DishList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);

};

#endif // DISHLIST_H
