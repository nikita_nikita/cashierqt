#include "goods.h"

Goods::Goods(QJsonObject jObj)
{
this->setData(jObj);
}

QString Goods::getId() const
{
    return id;
}

void Goods::setId(const QString &value)
{
    id = value;
}

QString Goods::getName() const
{
    return name;
}

void Goods::setName(const QString &value)
{
    name = value;
}

QString Goods::getUnit() const
{
    return unit;
}

void Goods::setUnit(const QString &value)
{
    unit = value;
}

QString Goods::getCategory() const
{
    return category;
}

void Goods::setCategory(const QString &value)
{
    category = value;
}

WorkshopList Goods::getWorkshops() const
{
    return workshops;
}

void Goods::setWorkshops(const WorkshopList &value)
{
    workshops = value;
}

QString Goods::getImage() const
{
    return image;
}

void Goods::setImage(const QString &value)
{
    image = value;
}

QString Goods::getColor() const
{
    return color;
}

void Goods::setColor(const QString &value)
{
    color = value;
}

QString Goods::getBarcode() const
{
    return barcode;
}

void Goods::setBarcode(const QString &value)
{
    barcode = value;
}

QString Goods::getWeight_flag() const
{
    return weight_flag;
}

void Goods::setWeight_flag(const QString &value)
{
    weight_flag = value;
}

int Goods::getSort() const
{
    return sort;
}

void Goods::setSort(int value)
{
    sort = value;
}

QJsonObject Goods::toJson()
{
 QJsonObject jObj;
 jObj["id"] = this->id;
 jObj["name"] = this->name;
 jObj["unit"] = this->unit;
 jObj["category"] = this->category;

 jObj["workshops"] = this->workshops.toJson();

 jObj["image"] = this->image;
 jObj["color"] = this->color;
 jObj["barcode"] = this->barcode;
 jObj["weight_flag"] = this->weight_flag;
 jObj["sort"] = this->sort;
 return  jObj;

}

void Goods::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setUnit(jObj.value("unit").toString());
    setCategory(jObj.value("category").toString());
    setWorkshops(WorkshopList(jObj.value("workshops").toArray()));
    setImage(jObj.value("image").toString());
    setColor(jObj.value("color").toString());
    setBarcode(jObj.value("barcode").toString());
    setWeight_flag(jObj.value("weight_flag").toString());
    setSort(jObj.value("sort").toInt());
}
