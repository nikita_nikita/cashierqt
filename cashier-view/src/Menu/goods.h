#ifndef GOODS_H
#define GOODS_H

#include <QJsonObject>
#include <QJsonArray>
#include "workshoplist.h"
class Goods
{
public:
    Goods(QJsonObject jObj);

    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getUnit() const;
    void setUnit(const QString &value);

    QString getCategory() const;
    void setCategory(const QString &value);

    WorkshopList getWorkshops() const;
    void setWorkshops(const WorkshopList &value);

    QString getImage() const;
    void setImage(const QString &value);

    QString getColor() const;
    void setColor(const QString &value);

    QString getBarcode() const;
    void setBarcode(const QString &value);

    QString getWeight_flag() const;
    void setWeight_flag(const QString &value);

    int getSort() const;
    void setSort(int value);

    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString unit;
    QString category;
    WorkshopList workshops;
    QString image;
    QString color;
    QString barcode;
    QString weight_flag;
    int sort;
};

#endif // GOODS_H
