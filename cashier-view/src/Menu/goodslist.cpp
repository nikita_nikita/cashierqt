#include "goodslist.h"

GoodsList::GoodsList(QJsonArray jArr)
{
    this->setData(jArr);
}

QJsonArray GoodsList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Goods goods = this->at(i);
        jArr.append(goods.toJson());
    }
    return  jArr;
}


void GoodsList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Goods(jArr.at(i).toObject()));
    }
}
