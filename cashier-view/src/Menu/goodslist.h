#ifndef GOODSLIST_H
#define GOODSLIST_H

#include "goods.h"
#include <QJsonArray>
#include<QList>
class GoodsList: QList<Goods>
{
public:
    GoodsList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // GOODSLIST_H
