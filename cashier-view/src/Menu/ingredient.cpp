#include "ingredient.h"

Ingredient::Ingredient(QJsonObject jObj)
{
    this->setData(jObj);
}


int Ingredient::getSort() const
{
    return sort;
}

void Ingredient::setSort(int value)
{
    sort = value;
}

double Ingredient::getCarbohydrates() const
{
    return carbohydrates;
}

void Ingredient::setCarbohydrates(double value)
{
    carbohydrates = value;
}

double Ingredient::getProteins() const
{
    return proteins;
}

void Ingredient::setProteins(double value)
{
    proteins = value;
}

double Ingredient::getFats() const
{
    return fats;
}

void Ingredient::setFats(double value)
{
    fats = value;
}

double Ingredient::getLoss_hot() const
{
    return loss_hot;
}

void Ingredient::setLoss_hot(double value)
{
    loss_hot = value;
}

double Ingredient::getLoss_cold() const
{
    return loss_cold;
}

void Ingredient::setLoss_cold(double value)
{
    loss_cold = value;
}

double Ingredient::getWeight_flag() const
{
    return weight_flag;
}

void Ingredient::setWeight_flag(double value)
{
    weight_flag = value;
}

QString Ingredient::getBarcode() const
{
    return barcode;
}

void Ingredient::setBarcode(const QString &value)
{
    barcode = value;
}

QString Ingredient::getImage() const
{
    return image;
}

void Ingredient::setImage(const QString &value)
{
    image = value;
}

QString Ingredient::getCategory() const
{
    return category;
}

void Ingredient::setCategory(const QString &value)
{
    category = value;
}

QString Ingredient::getDescriprion() const
{
    return descriprion;
}

void Ingredient::setDescriprion(const QString &value)
{
    descriprion = value;
}

QString Ingredient::getUnit() const
{
    return unit;
}

void Ingredient::setUnit(const QString &value)
{
    unit = value;
}

QString Ingredient::getName() const
{
    return name;
}

void Ingredient::setName(const QString &value)
{
    name = value;
}

double Ingredient::getCalorie() const
{
    return calorie;
}

void Ingredient::setCalorie(double value)
{
    calorie = value;
}

QString Ingredient::getId() const
{
    return id;
}

void Ingredient::setId(const QString &value)
{
    id = value;
}

QJsonObject Ingredient::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["unit"] = this->unit;
    jObj["descriprion"] = this->descriprion;
    jObj["category"] = this->category;
    jObj["image"] = this->image;
    jObj["barcode"] = this->barcode;
    jObj["weight_flag"] = this->weight_flag;
    jObj["loss_cold"] = this->loss_cold;
    jObj["fats"] = this->fats;
    jObj["proteins"] = this->proteins;
    jObj["carbohydrates"] = this->carbohydrates;
    jObj["calorie"] = this->calorie;
    jObj["sort"] = this->sort;

    return  jObj;
}

void Ingredient::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setUnit(jObj.value("unit").toString());
    setDescriprion(jObj.value("descriprion").toString());
    setCategory(jObj.value("category").toString());
    setImage(jObj.value("image").toString());
    setBarcode(jObj.value("barcode").toString());
    setWeight_flag(jObj.value("weight_flag").toDouble());
    setLoss_cold(jObj.value("loss_cold").toDouble());
    setLoss_hot(jObj.value("loss_hot").toDouble());
    setFats(jObj.value("fat").toDouble());
    setProteins(jObj.value("proteins").toDouble());
    setCarbohydrates(jObj.value("carbohydrates").toDouble());
    setCalorie(jObj.value("calorie").toDouble());
    setSort(jObj.value("sort").toInt());



}

