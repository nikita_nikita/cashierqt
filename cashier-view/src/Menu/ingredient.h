#ifndef INGREDIENT_H
#define INGREDIENT_H

#include <QJsonObject>
class Ingredient
{
public:
    Ingredient(QJsonObject jObj);

    QString getId() const;
    void setId(const QString &value);


    QString getName() const;
    void setName(const QString &value);

    QString getUnit() const;
    void setUnit(const QString &value);

    QString getDescriprion() const;
    void setDescriprion(const QString &value);

    QString getCategory() const;
    void setCategory(const QString &value);

    QString getImage() const;
    void setImage(const QString &value);

    QString getBarcode() const;
    void setBarcode(const QString &value);

    double getWeight_flag() const;
    void setWeight_flag(double value);

    double getLoss_cold() const;
    void setLoss_cold(double value);

    double getLoss_hot() const;
    void setLoss_hot(double value);

    double getFats() const;
    void setFats(double value);

    double getProteins() const;
    void setProteins(double value);

    double getCarbohydrates() const;
    void setCarbohydrates(double value);

    double getCalorie() const;
    void setCalorie(double value);

    int getSort() const;
    void setSort(int value);

    QJsonObject toJson();
private:
    void setData(QJsonObject jObj);

    QString id;
    QString name;
    QString unit;
    QString descriprion;
    QString category;
    QString image;
    //int или QString?
    QString barcode;
    double weight_flag;
    double loss_cold;
    double loss_hot;
    double fats;
    double proteins;
    double carbohydrates;
    double calorie;
    int sort;



};

#endif // INGREDIENT_H
