#include "ingredientList.h"

IngredientList::IngredientList()
{

}
IngredientList::IngredientList(QJsonArray jArr)
{
this->setData(jArr);
}

QJsonArray IngredientList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Ingredient ingredients = this->at(i);
        jArr.append(ingredients.toJson());
    }
    return  jArr;
}

void IngredientList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Ingredient(jArr.at(i).toObject()));
    }
}
