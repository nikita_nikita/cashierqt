#ifndef INGREDIENTS_H
#define INGREDIENTS_H

#include "ingredient.h"
#include <QList>
#include <QJsonArray>
class IngredientList:QList<Ingredient>

{
public:
    IngredientList();
    IngredientList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // INGREDIENTS_H
