#include "modifers.h"

Modifers::Modifers()
{

}

Modifers::Modifers(QJsonObject jObj)
{

}

QString Modifers::getId() const
{
    return id;
}

void Modifers::setId(const QString &value)
{
    id = value;
}

QString Modifers::getImage() const
{
    return image;
}

void Modifers::setImage(const QString &value)
{
    image = value;
}

ModificationsList Modifers::getModifications() const
{
    return modifications;
}

void Modifers::setModifications(const ModificationsList &value)
{
    modifications = value;
}

int Modifers::getMinNum() const
{
    return minNum;
}

void Modifers::setMinNum(int value)
{
    minNum = value;
}

int Modifers::getMaxNum() const
{
    return maxNum;
}

void Modifers::setMaxNum(int value)
{
    maxNum = value;
}

int Modifers::getSort() const
{
    return sort;
}

void Modifers::setSort(int value)
{
    sort = value;
}

QString Modifers::getName() const
{
    return name;
}

void Modifers::setName(const QString &value)
{
    name = value;
}

QJsonObject Modifers::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["sort"] = this->sort;
    jObj["type"] = this->type;
    jObj["image"] = this->image;
    jObj["maxNum"] = this->maxNum;
    jObj["minNum"] = this->minNum;
    jObj["modifications"] = this->modifications.toJson();
    return  jObj;
}

void Modifers::setData(QJsonObject jObj)
{
    setId(jObj["id"].toString());
    setName(jObj["name"].toString());
    setSort(jObj["sort"].toInt());
    setType(jObj["type"].toInt());
    setImage(jObj["image"].toString());
    setMaxNum(jObj["maxNum"].toInt());
    setMinNum(jObj["minNum"].toInt());
    setModifications(jObj["modifications"].toArray());

}

int Modifers::getType() const
{
    return type;
}

void Modifers::setType(int value)
{
    type = value;
}
