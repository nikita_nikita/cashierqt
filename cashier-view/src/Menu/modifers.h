#ifndef MODIFERS_H
#define MODIFERS_H
#include "modificationslist.h"
#include <QJsonObject>
#include <type_traits>
class Modifers
{
public:
    Modifers();
    Modifers(QJsonObject jObj);
    QString getId() const;
    void setId(const QString &value);

    QString getImage() const;
    void setImage(const QString &value);

    ModificationsList getModifications() const;
    void setModifications(const ModificationsList &value);

    int getMinNum() const;
    void setMinNum(int value);

    int getMaxNum() const;
    void setMaxNum(int value);

    int getSort() const;
    void setSort(int value);

    QString getName() const;
    void setName(const QString &value);

    QJsonObject toJson();

    int getType() const;
    void setType(int value);

private:
    void setData(QJsonObject jObj);
    QString id;
    int type;
    QString image;
    ModificationsList modifications;
    int minNum;
    int maxNum;
    int sort;
    QString name;

};

#endif // MODIFERS_H
