#include "modification.h"

Modification::Modification(QJsonObject jObj)
{
this->setData(jObj);
}

QString Modification::getProduct() const
{
    return product;
}

void Modification::setProduct(const QString &value)
{
    product = value;
}

QString Modification::getName() const
{
    return name;
}

void Modification::setName(const QString &value)
{
    name = value;
}

bool Modification::getDefaults() const
{
    return defaults;
}

void Modification::setDefaults(bool value)
{
    defaults = value;
}

int Modification::getBrutto() const
{
    return brutto;
}

void Modification::setBrutto(int value)
{
    brutto = value;
}

QJsonObject Modification::toJson()
{
    QJsonObject jObj;
    jObj["product"]=this->product;
    jObj["name"]=this->product;
    jObj["defaults"]=this->defaults;
    jObj["brutto"]=this->brutto;
    return  jObj;

}

void Modification::setData(QJsonObject jObj)
{
 setName(jObj.value("name").toString());
 setProduct(jObj.value("product").toString());
 setDefaults(jObj.value("name").toBool());
 setBrutto(jObj.value("brutto").toInt());
}
