#ifndef MODIFICATION_H
#define MODIFICATION_H

#include <QJsonObject>
class Modification
{
public:
    Modification(QJsonObject jObj);
    QString getProduct() const;
    void setProduct(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    bool getDefaults() const;
    void setDefaults(bool value);

    int getBrutto() const;
    void setBrutto(int value);

    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString product;
    QString name;
    bool defaults;
    int brutto;

};

#endif // MODIFICATION_H
