#include "modificationslist.h"

ModificationsList::ModificationsList()
{

}

ModificationsList::ModificationsList(QJsonArray jArr)
{
    this->setData(jArr);
}

QJsonArray ModificationsList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Modification modification = this->at(i);
        jArr.append(modification.toJson());
    }
    return  jArr;
}

void ModificationsList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Modification(jArr.at(i).toObject()));
    }

}
