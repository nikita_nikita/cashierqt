#ifndef MODIFICATIONSLIST_H
#define MODIFICATIONSLIST_H
#include "modification.h"
#include <QList>
#include <QJsonArray>
class ModificationsList:QList<Modification>
{
public:
    ModificationsList();
    ModificationsList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // MODIFICATIONSLIST_H
