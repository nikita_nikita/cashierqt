#include "prepack.h"

Prepack::Prepack(QJsonObject jObj)
{
    this->setData(jObj);
}

QString Prepack::getId() const
{
    return id;
}

void Prepack::setId(const QString &value)
{
    id = value;
}

QString Prepack::getName() const
{
    return name;
}

void Prepack::setName(const QString &value)
{
    name = value;
}

QString Prepack::getDescription() const
{
    return descriprion;
}

void Prepack::setDescription(const QString &value)
{
    descriprion = value;
}

QString Prepack::getCategory() const
{
    return category;
}

void Prepack::setCategory(const QString &value)
{
    category = value;
}

IngredientList Prepack::getIngridients() const
{
    return IngredientList(ingridients);
}

void Prepack::setIngridients(const QJsonArray &value)
{
    ingridients = value;
}

QString Prepack::getImage() const
{
    return image;
}

void Prepack::setImage(const QString &value)
{
    image = value;
}

int Prepack::getSort() const
{
    return sort;
}

void Prepack::setSort(int value)
{
    sort = value;
}

QJsonObject Prepack::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["descriprion"] = this->descriprion;
    jObj["category"] = this->category;
    jObj["ingridients"] = this->ingridients;
    jObj["image"] = this->image;
    jObj["sort"] = this->sort;
    return  jObj;
}

void Prepack::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setDescription(jObj.value("descriprion").toString());
    setCategory(jObj.value("category").toString());
    setIngridients(jObj.value("ingridients").toArray());
    setImage(jObj.value("image").toString());
    setSort(jObj.value("sort").toInt());
}
