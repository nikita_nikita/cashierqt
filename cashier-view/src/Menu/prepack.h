#ifndef PREPACK_H
#define PREPACK_H

#include <QJsonDocument>
#include "ingredientList.h"
class Prepack
{
public:
    Prepack(QJsonObject jObj);
    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getCategory() const;
    void setCategory(const QString &value);

    IngredientList getIngridients() const;
    void setIngridients(const QJsonArray &value);

    QString getImage() const;
    void setImage(const QString &value);

    int getSort() const;
    void setSort(int value);
    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString descriprion;
    QString category;
    QJsonArray ingridients;
    QString image;
    int sort;
};

#endif // PREPACK_H
