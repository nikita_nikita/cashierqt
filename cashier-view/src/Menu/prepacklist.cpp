#include "prepacklist.h"

PrepackList::PrepackList(QJsonArray jArr)
{
this->setData(jArr);
}

QJsonArray PrepackList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Prepack  prepack = this->at(i);
        jArr.append(prepack.toJson());
    }
    return  jArr;
}

void PrepackList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Prepack(jArr.at(i).toObject()));
    }
}
