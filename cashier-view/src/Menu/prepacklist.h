#ifndef PREPACKLIST_H
#define PREPACKLIST_H
#include "prepack.h"
#include <QList>
#include <QJsonArray>

class PrepackList:QList<Prepack>
{
public:
    PrepackList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);

};

#endif // PREPACKLIST_H
