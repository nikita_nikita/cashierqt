#include "unit.h"

Unit::Unit(QJsonObject jObj)
{
    this->setData(jObj);
}

QString Unit::getId() const
{
    return id;
}

void Unit::setId(const QString &value)
{
    id = value;
}

QString Unit::getName() const
{
    return name;
}

void Unit::setName(const QString &value)
{
    name = value;
}

QString Unit::getShorts() const
{
    return shorts;
}

void Unit::setShorts(const QString &value)
{
    shorts = value;
}

QString Unit::getCode() const
{
    return code;
}

void Unit::setCode(const QString &value)
{
    code = value;
}

bool Unit::getIsFloat() const
{
    return isFloat;
}

void Unit::setIsFloat(bool value)
{
    isFloat = value;
}

QJsonObject Unit::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    jObj["shorts"] = this->shorts;
    jObj["code"] = this->code;
    jObj["isFloat"] = this->isFloat;

    return  jObj;
}

void Unit::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());
    setShorts(jObj.value("shorts").toString());
    setCode(jObj.value("code").toString());
    setIsFloat(jObj.value("isFloat").toBool());

}
