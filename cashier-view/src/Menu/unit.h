#ifndef UNIT_H
#define UNIT_H

#include <QJsonObject>
class Unit
{
public:
    Unit(QJsonObject jObj);
    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getShorts() const;
    void setShorts(const QString &value);

    QString getCode() const;
    void setCode(const QString &value);

    bool getIsFloat() const;
    void setIsFloat(bool value);

    QJsonObject toJson();

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString shorts;
    QString code;
    bool isFloat;
};

#endif // UNIT_H
