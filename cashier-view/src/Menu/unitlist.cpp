#include "unitlist.h"
#include "unit.h"

UnitList::UnitList(QJsonArray jArr)
{
    this->setData(jArr);
}

QJsonArray UnitList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Unit unit = this->at(i);
        jArr.append(unit.toJson());
    }
    return  jArr;
}

void UnitList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Unit(jArr.at(i).toObject()));
    }
}
