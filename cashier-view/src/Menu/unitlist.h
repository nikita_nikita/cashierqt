#ifndef UNITLIST_H
#define UNITLIST_H
#include "unit.h"
#include <QJsonArray>
#include <QList>
class UnitList: QList<Unit>
{
public:
    UnitList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray);
};

#endif // UNITLIST_H
