#include "workshop.h"

Workshop::Workshop(QJsonObject jObj)
{
    this->setData(jObj);
}


QString Workshop::getName() const
{
    return name;
}

void Workshop::setName(const QString &value)
{
    name = value;
}

QString Workshop::getId() const
{
    return id;
}

void Workshop::setId(const QString &value)
{
    id = value;
}

QJsonObject Workshop::toJson()
{
    QJsonObject jObj;
    jObj["id"] = this->id;
    jObj["name"] = this->name;
    return  jObj;
}

void Workshop::setData(QJsonObject jObj)
{
    setId(jObj.value("id").toString());
    setName(jObj.value("name").toString());

}
