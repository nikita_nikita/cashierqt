#ifndef WORKSHOP_H
#define WORKSHOP_H
#include <QJsonObject>

class Workshop
{
public:
    Workshop(QJsonObject jObj);
    QJsonObject toJson();
    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
};

#endif // WORKSHOP_H
