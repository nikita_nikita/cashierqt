#include "workshoplist.h"

WorkshopList::WorkshopList()
{

}
WorkshopList::WorkshopList(QJsonArray jArr)
{
this->setData(jArr);
}

QJsonArray WorkshopList::toJson()
{
    QJsonArray jArr;
    for(int i = 0; i<this->length(); i++){
        Workshop workshop = this->at(i);
        jArr.append(workshop.toJson());
    }
    return  jArr;
}

void WorkshopList::setData(QJsonArray jArr)
{
    for(int i = 0; i < jArr.size(); i++){
        this->append(Workshop(jArr.at(i).toObject()));
    }
}
