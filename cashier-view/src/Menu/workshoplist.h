#ifndef WORKSHOPLIST_H
#define WORKSHOPLIST_H
#include "workshop.h"
#include <QJsonArray>
#include <QList>
class WorkshopList:QList<Workshop>
{
public:
    WorkshopList();
    WorkshopList(QJsonArray jArr);
    QJsonArray toJson();
private:
    void setData(QJsonArray jArr);
};

#endif // WORKSHOPLIST_H
