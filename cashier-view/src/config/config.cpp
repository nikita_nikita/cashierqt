#include "config.h"
#include <QDebug>
Config::Config()
{

}

QString Config::getScheme() const
{
    return scheme;
}

void Config::setScheme(const QString &value)
{
    qDebug()<<"New scheme >>>  "<<value;
    scheme = value;
}

QString Config::getHost() const
{
    return host;
}

void Config::setHost(const QString &value)
{
    qDebug()<<"New host >>>  "<<value;
    host = value;
}

int Config::getPort() const
{
    return port;
}

void Config::setPort(int value)
{
    qDebug()<<"New port >>>  "<<value;
    port = value;
}

QString Config::getPointID() const
{
    return pointID;
}

void Config::setPointID(const QString &value)
{
    qDebug()<<"New PointId >>>  "<<value;
    pointID = value;
}

QString Config::getNewOrderStatus() const
{
    return newOrderStatus;
}

void Config::setNewOrderStatus(const QString &value)
{    
    qDebug()<<"New OrderStatus >>>  "<<value;
    newOrderStatus = value;

}

QString Config::getNotProcessedStatus() const
{
    return notProcessedStatus;
}

void Config::setNotProcessedStatus(const QString &value)
{
    notProcessedStatus = value;
}

QString Config::getCity_id() const
{
    return city_id;
}

void Config::setCity_id(const QString &value)
{
    city_id = value;
}
