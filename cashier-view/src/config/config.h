#ifndef CONFIG_H
#define CONFIG_H
#include <QString>

class Config
{
public:
    Config();
    QString getScheme() const;
    void setScheme(const QString &value);

    QString getHost() const;
    void setHost(const QString &value);

    int getPort() const;
    void setPort(int value);

    QString getPointID() const;
    void setPointID(const QString &value);

    QString getNewOrderStatus() const;
    void setNewOrderStatus(const QString &value);

    QString getNotProcessedStatus() const;
    void setNotProcessedStatus(const QString &value);

    QString getCity_id() const;
    void setCity_id(const QString &value);

private:
    QString pointID;
    QString scheme = "";
    QString host = "";
    int port;
    QString newOrderStatus = "";
    QString notProcessedStatus = "";
    QString city_id = "";
};

#endif // CONFIG_H
