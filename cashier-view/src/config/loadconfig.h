#ifndef LOADCONFIG_H
#define LOADCONFIG_H

#include <QString>
#include <QFile>
#include "config.h"
class LoadConfig
{
public:
    LoadConfig();
    void readFromFile();
    void saveConfiguration();

private:
    
    QString path = "../";
};

#endif // LOADCONFIG_H
