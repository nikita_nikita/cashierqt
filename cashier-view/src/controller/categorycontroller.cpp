#include "categorycontroller.h"

CategoryController::CategoryController(QObject *parent):ObjectController(parent)
{
    m_category_model = new CategorylistModel(this);

}
void CategoryController::setToken(QString token)
{
    ObjectController::setToken(token);
    categoryManager.setToken(token);
    categoryManager.list();
}


void CategoryController::onLoadCategory(CategoryList categories)
{
    for(int i = 0;i<categories.count();i++){
        m_category_model->append(categories.at(i));
    }

}
void CategoryController::setServiceLocator(ServiceLocator *serviceLocator)
{
    ObjectController::setServiceLocator(serviceLocator);
    connect(serviceLocator->getShiftService(), &ShiftService::tokenChanged, this, &CategoryController::setToken);
    connect(serviceLocator->getStorageService(), &StorageService::onLoadCategoriesSuccess, this, &CategoryController::onLoadCategory);

}

