#ifndef CATEGORYCONTROLLER_H
#define CATEGORYCONTROLLER_H
#include "objectcontroller.h"
#include "menu/categorymanager.h"
#include "../model/categorylistmodel.h"
class CategoryController : public ObjectController
{
    Q_OBJECT

public:
    explicit CategoryController(QObject *parent = nullptr);

public slots:
    virtual void setToken(QString token);
    virtual void setServiceLocator(ServiceLocator *serviceLocator);
    void onLoadCategory(CategoryList categories);

private:
    CategoryManager categoryManager;
    CategorylistModel *m_category_model;

};

#endif // CATEGORYCONTROLLER_H
