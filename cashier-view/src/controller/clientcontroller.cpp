#include "clientcontroller.h"

ClientController::ClientController(QObject *parent) : ObjectController(parent)
{
    clientManager = new ClientManager();
    m_client_result_list_model = new ClientListModel(this);
}



QString ClientController::number() const
{
    return m_number;
}

void ClientController::setStorageService(StorageService *value)
{
    storageService = value;
}





void ClientController::setToken(QString &token)
{

    ObjectController::setToken(token);
    clientManager->setToken(token);
}

void ClientController::setServiceLocator(ServiceLocator *serviceLocator)
{
    ObjectController::setServiceLocator(serviceLocator);
    setStorageService(serviceLocator->getStorageService());
    connect(serviceLocator->getShiftService(), &ShiftService::tokenChanged, this, &ClientController::setToken);
    connect(clientManager, &ClientManager::succesFindByPhone, this, &ClientController::setFindClientResult);
    connect(clientManager,&ClientManager::getList,this,&ClientController::addClientList);

}

void ClientController::setConfig(Config *config)
{
    clientManager->setHost(config->getHost());
    clientManager->setPort(config->getPort());
    clientManager->setPoint(config->getPointID());
    clientManager->setScheme(config->getScheme());
}

void ClientController::phindClientByPhone(QString phone)
{
Q_UNUSED(phone)
}

void ClientController::findClientByPhone()
{
    qDebug()<<number()<< "Номер поиска";
    QString phone = number();
    phone.remove("+");
    phone.remove(")");
    phone.remove("(");
    phone.remove(" ");
    phone.remove(0,1);
    clientManager->findByPhone(phone);

}


void ClientController::setNumber(QString number)
{
    if (m_number == number)
        return;

    m_number = number;
    emit numberChanged(m_number);
}

void ClientController::setFindClientResult(ClientList findClientListResult)
{
    qDebug()<<"найдено "<<findClientListResult.count()<<" клиентов";

    m_client_result_list_model->setClientList(findClientListResult);
}

void ClientController::selectClientFromList(int index)
{

appointClientToOrder(m_client_result_list_model->getClientByIndex(index));
router->toProductChoice();


}

void ClientController::onGetClientInfoList(ClientList clientList)
{
    m_clientList = clientList;
}

void ClientController::onNeedClientInfoList(QList<QString> clientPhoneList)
{
    qDebug()<<"onNeedClientInfoList";
    //при первом запуске ничего не сравнивает

    if(m_clientList.count() == 0 && clientPhoneList.count() != 0){
        clientManager->findClientByPhoneList(clientPhoneList);
        return;
    }

    for(int i = m_clientList.count() -1 ; i > -1;i--){
        if(m_clientList.indexOfNumber(clientPhoneList.at(i)) != -1){

            clientPhoneList.removeAt(i);

        }
    }
    if(clientPhoneList.count() > 0){
    clientManager->findClientByPhoneList(clientPhoneList);
    }

}

void ClientController::addClientList(const ClientList &clientList)
{
 qDebug()<<"<><><><><>>>>"<< clientList.count();
 m_clientList.append(clientList);
 emit clientListUpdate(m_clientList);
}

void ClientController::choiceClient(int index)
{
    auto clientPhone =  m_client_result_list_model->getClientByIndex(index).getPhone();
    emit clientChoice(clientPhone);
}


