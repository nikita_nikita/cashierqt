#ifndef CLIENTCONTROLLER_H
#define CLIENTCONTROLLER_H

#include "objectcontroller.h"
#include "sales/clientmanager.h"
#include "../model/clientlistmodel.h"


////////////////////////
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
class ClientController : public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(QString number READ number WRITE setNumber NOTIFY numberChanged)
    Q_PROPERTY(ClientListModel *  client_result_list_model MEMBER  m_client_result_list_model NOTIFY  client_result_listModelchanged)


public:
    explicit ClientController(QObject *parent = nullptr);
    ClientController();
    ClientListModel * clientFindResult() const;
    QString number() const;
    void setStorageService(StorageService *value);
    Client fmtClient() const;




public slots:
    //внутрення настройка
    virtual void setToken(QString &token);
    virtual void setServiceLocator(ServiceLocator *serviceLocator);
    virtual void setConfig(Config *config);



    //работа с клиентской  иформацией
    void choiceClient(int index);
    void phindClientByPhone(QString phone);
    void findClientByPhone();
    void setNumber(QString number);
    void setFindClientResult(ClientList findClientListResult);
    void selectClientFromList(int index);
    void onGetClientInfoList(ClientList clientList);
    void onNeedClientInfoList(QList<QString> clientPhoneList);
signals:
    void numberChanged(QString number);
    void client_result_listModelchanged(ClientListModel * client_result_list_model);
    void appointClientToOrder(Client client);
    void fmtClientChanged(Client fmtClient);
    void clientListUpdate(ClientList clientList);
    void clientChoice(QString phone);

private slots:
    void addClientList(const ClientList &clientList);

private:
    ClientManager *clientManager;
    QString m_number;

    ClientListModel * m_client_result_list_model;
    StorageService *storageService;
    ClientList m_clientList;


};

#endif // CLIENTCONTROLLER_H
