#include "logincontroller.h"
#include <QDebug>
LoginController::LoginController(QObject *parent):ObjectController(parent)
{
}

void LoginController::setServiceLocator(ServiceLocator *serviceLocator)
{
    ObjectController::setServiceLocator(serviceLocator);
    this->serviceLocator =serviceLocator;
    connect(serviceLocator->getShiftService(), &ShiftService::tokenChanged, this, &LoginController::data);
    connect(serviceLocator->getShiftService(), &ShiftService::error, this, &LoginController::showErrorOnLoginScreen);

 }

void LoginController::login(QString login, QString passswd)
{
    serviceLocator->getShiftService()->Open(login,passswd);
}

void LoginController::loginByPin(QString pin)
{
  serviceLocator->getShiftService()->Open(pin);
}

void LoginController::close()
{
    serviceLocator->getShiftService()->Close(serviceLocator->getShiftService()->getToken());
}

void LoginController::data(QString token)
{
    if(m_shiftStatus != false)
        m_shiftStatus = false;
    emit  shiftStatusChanged();



    ObjectController::info("Смена открыта");
   if(token.length()>0) {
       router->toMainPage();
   }
}

void LoginController::showErrorOnLoginScreen(QString message)
{
    qDebug()<<message;
    setErrorMessage(message);
    setErrorMessageVisible(true);
}

bool LoginController::getShiftStatus(){
    return  m_shiftStatus;
}





