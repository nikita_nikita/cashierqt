#ifndef SHIFTCONTROLLER_H
#define SHIFTCONTROLLER_H
#include "../service/staff/shiftservice.h"
#include "objectcontroller.h"
class LoginController:public ObjectController
{
    Q_OBJECT
    Q_PROPERTY(bool shiftStatus READ getShiftStatus NOTIFY shiftStatusChanged)
    Q_PROPERTY(bool errorMessageVisible READ errorMessageVisible WRITE setErrorMessageVisible NOTIFY errorMessageVisibleChanged)
    Q_PROPERTY(QString errorMessage READ errorMessage WRITE setErrorMessage NOTIFY errorMessageChanged)
public:
    explicit LoginController(QObject *parent = nullptr);
    bool shiftIsOpen() const;
    bool errorMessageVisible() const
    {
        return m_errorMessageVisible;
    }

    QString errorMessage() const
    {
        return m_errorMessage;
    }

signals:
    void info(QString message);
    void shiftStatusChanged();
    void errorMessageVisibleChanged(bool errorMessageVisible);

    void errorMessageChanged(QString errorMessage);

public slots:
    bool getShiftStatus();
    void login(QString login, QString passswd);
    void loginByPin(QString pin);
    void close();
    void setErrorMessageVisible(bool errorMessageVisible)
    {
        if (m_errorMessageVisible == errorMessageVisible)
            return;

        m_errorMessageVisible = errorMessageVisible;
        emit errorMessageVisibleChanged(m_errorMessageVisible);
    }

    void setErrorMessage(QString errorMessage)
    {
        if (m_errorMessage == errorMessage)
            return;

        m_errorMessage = errorMessage;
        emit errorMessageChanged(m_errorMessage);
    }

private slots:
    virtual void setServiceLocator(ServiceLocator *serviceLocator);
    void data(QString token);
    void showErrorOnLoginScreen(QString message);
private:
    bool m_shiftStatus = true;
    ServiceLocator *serviceLocator;





    bool m_errorMessageVisible = false;
    QString m_errorMessage = "";
};

#endif // SHIFTCONTROLLER_H
