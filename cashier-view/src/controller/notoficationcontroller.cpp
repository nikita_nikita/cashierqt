#include "notoficationcontroller.h"

NotificationController::NotificationController(QObject *parent):ObjectController(parent)
{
    m_model = new NotificationListModel(this);

    m_notificationVisible = new NotificationProxyModel(this);
    m_notificationVisible->setSourceModel(m_model);
    m_notificationVisible->setFilterRole(NotificationListModel::ReadRole);   
    m_notificationVisible->setReadFilter(false);


}

void NotificationController::setVisibleTrue(bool visible)
{
    m_notificationVisible->setReadFilter(visible);
}





//тип - приоритет
//1. Информирование(Низкий). Уведомление об изменениях.
//   Например: "Обновление ценн".
//             "Посупил новый заказ"
//2. Предупреждение(Средний). Уведомление о некритических ошибках.
//   Например: При выборе ролла недели после 16:00 - "Часы проведения акции истекли"
//             "Пора готовить заказ №149589"
//3. Ошибка(Высокий). Уведомление об ошибках приводящим к остановке проццесса или грубых ошибках
//со стороны пользователя
//   Например: "Смена закрыта", "Неверный токен", "Вы не авторизоллваны".
//4. ПЕРДАК ПЫЛАЕТ СИНИМ ПЛАМЕНЕМ. СЕРВЕР НЕ ОТВЕЧАЕТ. ПРОДУКТЫ КОНЧИЛИСЬ. ЕМКОСТИ КОНЧИЛИСЬ. ЗАМЕНИТЕ ЛЕНТУ В ПРИНТЕРЕ!
//   Возможно размещение поверх всех окон на ГЛАВНОМ ЭКРАНЕ.



void NotificationController::newNotification(int type, QString message)
{

this->m_model->addMessage(Notification(type,message));
}

void NotificationController::errorNotification(QString message)
{
    this->newNotification(ERROR,message);
}

void NotificationController::infoNotification(QString message)
{
     this->newNotification(INFO,message);
}

void NotificationController::warningNotification(QString message)
{
     this->newNotification(WARNING,message);
}

void NotificationController::succesNotification(QString message)
{
    this->newNotification(SUCCES,message);
}

void NotificationController::readMessage(int i)
{
    this->m_model->messageRead(i);

}



