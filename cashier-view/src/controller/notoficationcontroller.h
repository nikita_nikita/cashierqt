﻿#ifndef NOTOFICATIONCONTROLLER_H
#define NOTOFICATIONCONTROLLER_H
#include "../entity/notification.h"
#include "objectcontroller.h"
#include "../model/notificationlistmodel.h"
#include <QSortFilterProxyModel>

#include "../model/notificationproxymodel.h"
class NotificationController :public ObjectController
{
    Q_OBJECT

    Q_PROPERTY(NotificationListModel* model MEMBER m_model NOTIFY modelChanged)
    Q_PROPERTY(NotificationProxyModel* visibleModel MEMBER m_notificationVisible NOTIFY visibleModelChanged)
public:
    NotificationController(QObject *parent = nullptr);
    enum Type{
        ERROR = 2,
        WARNING = 1,
        INFO =0,
        SUCCES = 3
    };

signals:
    void modelChanged(NotificationListModel* model);
    void visibleModelChanged(QSortFilterProxyModel* visibleModel);
public slots:
//    void chengeVisibleStatus(int index);
    void setVisibleTrue(bool visible);
    void newNotification(int type, QString message);
    void errorNotification(QString message);
    void infoNotification(QString message);
    void warningNotification(QString message);
    void succesNotification(QString message);
    void readMessage(int index);

private:
    int visibleMessage;
    NotificationProxyModel *m_notificationVisible;
    NotificationListModel *m_model;

};

#endif // NOTOFICATIONCONTROLLER_H
