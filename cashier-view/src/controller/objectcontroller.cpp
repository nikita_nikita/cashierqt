#include "objectcontroller.h"

ObjectController::ObjectController(QObject *parent) : QObject(parent)
{

}

void ObjectController::setConfig(Config *data)
{
    config = *data;

}

void ObjectController::setServiceLocator(ServiceLocator *serviceLocator)
{
Q_UNUSED(serviceLocator)
}

void ObjectController::setToken(QString &token)
{
    Q_UNUSED(token)
}

void ObjectController::setRouter(Router *router)
{
    this->router = router;
}




