#ifndef OBJECTCONTROLLER_H
#define OBJECTCONTROLLER_H
#include <QObject>
#include "../service/servicelocator.h"
#include "../config/config.h"
#include "entityabstructmanager.h"
#include "../router/router.h"
class  ObjectController : public QObject
{
    Q_OBJECT
public:
    explicit ObjectController(QObject *parent = nullptr);

signals:
    void error( QString message);
    void info( QString message);
    void warning( QString message);
public slots:
    virtual void setConfig(Config *config);
    virtual void setServiceLocator(ServiceLocator *serviceLocator);
    virtual void setToken(QString &token);
    virtual void setRouter(Router * router);

protected:
    Config config;
    QString newOrderStatus;
    Router * router;


};

#endif // OBJECTCONTROLLER_H
