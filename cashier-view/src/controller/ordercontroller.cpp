#include "ordercontroller.h"


OrderController::OrderController(QObject *parent) : ObjectController(parent)
{
    timer = new QTimer(this);
    m_order_model = new OrderListModel(this);


    settingManager = new SettingManager(this);
    connect(settingManager,&SettingManager::loadSettingSuccess, this,&OrderController::onLoadSettingSuccess);

    m_orderObject = new OrderEntityObject(this);
    connect(m_orderObject,&OrderEntityObject::statusChanged,this,&OrderController::setStatusToOrderInProgress);
    m_status_list_model = new StatusListModel(this);
    m_productListModel = new ProductListModel(this);
    connect(m_productListModel, &ProductListModel::onClickedToProduct,this, &OrderController::addToProductToCart);

    m_nameVisibleProduct = new ProductListProxyModel(this);
    m_categoryVisibleProduct = new ProductListProxyModel(this);
    m_orderFilterModel = new OrderFilterVisibleModel(this);
    m_saveOrderProxyModel = new SaveOrderListPoxyModel(this);
    m_saveOrderProxyModel->setSourceModel(m_order_model);
    m_orderFilterModel->setSourceModel(m_order_model);
    m_nameVisibleProduct->setSourceModel(m_productListModel);
    m_categoryVisibleProduct->setSourceModel(m_productListModel);
    m_saveOrderProxyModel->setFilterRole(OrderListModel::StatusRole);
    m_saveOrderProxyModel->setStatus(config.getNotProcessedStatus());
    m_nameVisibleProduct->setFilterRole(ProductListModel::NameRole);
    m_orderFilterModel->setFilterRole(OrderListModel::DeliveryRole);
    m_categoryVisibleProduct->setFilterRole(ProductListModel::CategoryRole);


    m_modificationGroups = new ModificationGrouplistModel();
    connect(m_modificationGroups, &ModificationGrouplistModel::replaceModifications, this, &OrderController::setModificationProductToCart);
    connect(m_modificationGroups, &ModificationGrouplistModel::updateModifications, this, &OrderController::updateProductModificationsInCart);

    m_orderProductListModel = new ProductInOrderListModel();
    connect(m_orderProductListModel, &ProductInOrderListModel::updateModificationsInProduct,this, &OrderController::showModificationpanel);
    connect(m_orderProductListModel, &ProductInOrderListModel::productListChanged,this, &OrderController::onProductInOrderListChanged);
}


void OrderController::setToken(QString &token)
{
    ObjectController::setToken(token);
    settingManager->setToken(token);

    orderManager.setToken(token);
    clientManager.setToken(token);
    settingManager->getPointSetting(config.getPointID());
}

void OrderController::setServiceLocator(ServiceLocator *serviceLocator)
{
    ObjectController::setServiceLocator(serviceLocator);
    setStorageService(serviceLocator->getStorageService());
    connect(serviceLocator->getShiftService(), &ShiftService::tokenChanged, this, &OrderController::setToken);
    connect(&orderManager, &OrderManager::getList, this, &OrderController::onLoadOrderList);
    connect(&orderManager, &OrderManager::succesUpdated, this,&OrderController::onUpdateOrderSuccess);
    //    connect(&orderManager, &OrderManager::offerLoadSuccess, this, &OrderController::onOffersLoadSuccess);
    connect(&orderManager, &OrderManager::offerLoadSuccess, this, &OrderController::onOffersLoadSuccessObject);
    connect(serviceLocator->getStorageService(),&StorageService::productListChange,this,&OrderController::onProductLoadSuccess);
    connect(serviceLocator->getStorageService(),&StorageService::statusListChange,this,&OrderController::onStatusLoadSuccess);
//    connect(timer,&QTimer::timeout,this,&OrderController::load);

    //ДЛЯ теста
//    connect(timer,&QTimer::timeout,this,&OrderController::createNewOrderII);
//    connect(timer,&QTimer::timeout,this,&OrderController::checkUpdate);
}

void OrderController::setConfig(Config *config)
{
    this->config = *config;
    orderManager.setHost(config->getHost());
    orderManager.setPort(config->getPort());
    orderManager.setPoint(config->getPointID());
    orderManager.setScheme(config->getScheme());


    clientManager.setHost(config->getHost());
    clientManager.setPort(config->getPort());
    clientManager.setPoint(config->getPointID());
    clientManager.setScheme(config->getScheme());

}

void OrderController::updateOrder(int index)
{

    auto order = m_order_model->getOrderByIndex(index);
    orderProductListModel()->setList(order.getProducts());
    qDebug()<< order.getStatus()<< " <<< STAtus";
    orderManager.update(order);
    router->toOrderUpdatePage();


}

void OrderController::updateProductInOrder()
{
    router->toProductChoice();
}

void OrderController::createNewOrder(int type)
{
    Order order;
    auto id = orderObject()->generateUUID();
    order.setId(id);
    order.setLocalNumber("A15K17");
    orderObject()->setId(id);
    Address address;
    address.setCity(config.getCity_id());
    order.setAddress(address);
    order.setPoint(config.getPointID());
    order.setDelivery(type);    
    order.setStatus(config.getNotProcessedStatus());
    order.setCookInDate(QDate::currentDate());
    order.setCookInTime(QTime::currentTime());

    createNewOrder(order);
}



void OrderController::onClientListUpdate(ClientList clientList)
{
    qDebug()<<"onClientistUpdate";
    m_order_model->setClientList(clientList);
}

void OrderController::onLoadOrderList(OrderList orderlist)
{

    QList<QString> clientPhoneList;
    for(int i = 0; i< orderlist.count();i++){

        auto client = orderlist.at(i).getClient();
        if (client != ""){
            clientPhoneList.append(orderlist.at(i).getClient());
        }
    }
    emit needClientListInfo(clientPhoneList);
    qDebug()<<"Контроллер получил "<< orderlist.count()<< " заказа(ов)";
    orders = orderlist;
    m_order_model->set(orderlist);
}



void OrderController::onProductLoadSuccess(ProductList products, ProductCostHash productCost, ModificationGroupList modificationGroupList)
{
    qDebug()<<"OrderController получил "<< products.count()<< " продуктов";
    setModiferGroups(modificationGroupList);
    setProductList(products);

    m_productListModel->append(products);
    m_productListModel->setPrices(productCost);
    m_productListModel->setModifications(modificationGroupList);

    m_orderProductListModel->setProductList(products);
    m_orderProductListModel->setProductCostHash(productCost);
    m_orderProductListModel->setModifications(modificationGroupList);
    m_order_model->setProducts(products);
    m_order_model->setProductCost(productCost);
    this->load();
}

void OrderController::onStatusLoadSuccess(StatusList data)
{
    statusList = data;
    m_orderObject->setStatusList(&statusList);
    m_status_list_model->setList(&statusList);
}

void OrderController::setOrderObject(OrderEntityObject *orderObject)
{
    if (m_orderObject == orderObject)
        return;

    m_orderObject = orderObject;
    emit orderObjectChanged(m_orderObject);
}

void OrderController::setProductFilterName(QString name)
{
    qDebug()<< name << "WTF CHECK";
    m_nameVisibleProduct->setFindName(name);
}

void OrderController::setModificationGroups(ModificationGrouplistModel *modificationGroups)
{
    if (m_modificationGroups == modificationGroups)
        return;

    m_modificationGroups = modificationGroups;
    emit modificationGroupsChanged(m_modificationGroups);
}

void OrderController::setModificationPanelStatus(bool modificationPanelStatus)
{
    if (m_modificationPanelStatus == modificationPanelStatus)
        return;

    m_modificationPanelStatus = modificationPanelStatus;
    emit modificationPanelStatusChanged(m_modificationPanelStatus);
}

void OrderController::setOrderProductListModel(ProductInOrderListModel *orderProductListModel)
{
    qDebug()<<"wtF";
    if (m_orderProductListModel == orderProductListModel)
        return;

    m_orderProductListModel = orderProductListModel;
    emit orderProductListModelChanged(m_orderProductListModel);
}

void OrderController::setModificationProductToCart(ModiferList modiferList)
{
    this->tmpProduct->setModifers(modiferList);
    m_orderProductListModel->add(this->tmpProduct);
    delete (this->tmpProduct);

}

void OrderController::setPaymentTypePanelStatus(bool paymentTypePanelStatus)
{
    if (m_paymentTypePanelStatus == paymentTypePanelStatus)
        return;

    m_paymentTypePanelStatus = paymentTypePanelStatus;
    emit paymentTypePanelStatusChanged(m_paymentTypePanelStatus);
}

void OrderController::setOrderPayd(int orderPayd)
{
    if (m_orderPayd == orderPayd)
        return;

    m_orderPayd = orderPayd;
    emit orderPaydChanged(m_orderPayd);
}

void OrderController::setSdachaPanelStatus(bool sdachaPanelStatus)
{
    if (m_paymentPanelStatus == sdachaPanelStatus)
        return;

    m_paymentPanelStatus = sdachaPanelStatus;
    emit sdachaPanelStatusChanged(m_paymentPanelStatus);
}

void OrderController::setStatusChangeOrderPanelVisible(bool statusChangeOrderPanelVisible)
{
    if (m_statusChangeOrderPanelVisible == statusChangeOrderPanelVisible)
        return;

    m_statusChangeOrderPanelVisible = statusChangeOrderPanelVisible;
    emit statusChangeOrderPanelVisibleChanged(m_statusChangeOrderPanelVisible);
}

void OrderController::setTypeChangePanelVisible(bool typeChangePanelVisible)
{
    if (m_typeChangePanelVisible == typeChangePanelVisible)
        return;

    m_typeChangePanelVisible = typeChangePanelVisible;
    emit typeChangePanelVisibleChanged(m_typeChangePanelVisible);
}

void OrderController::setNotProcessedOrderModalVisible(bool notProcessedOrderModalVisible)
{
    if (m_notProcessedOrderModalVisible == notProcessedOrderModalVisible)
        return;

    m_notProcessedOrderModalVisible = notProcessedOrderModalVisible;
    emit notProcessedOrderModalVisibleChanged(m_notProcessedOrderModalVisible);
}

void OrderController::setSaveOrderProxyModel(SaveOrderListPoxyModel *saveOrderProxyModel)
{
    if (m_saveOrderProxyModel == saveOrderProxyModel)
        return;

    m_saveOrderProxyModel = saveOrderProxyModel;
    emit saveOrderProxyModelChanged(m_saveOrderProxyModel);
}

void OrderController::setUpdateOrderSuccess(bool updateOrderSuccess)
{
    if (m_updateOrderSuccess == updateOrderSuccess)
        return;

    m_updateOrderSuccess = updateOrderSuccess;
    emit updateOrderSuccessChanged(m_updateOrderSuccess);
}





void OrderController::addToProductToCart(QString id)
{
    tmpProduct = new ProductInOrder();
    auto product = productList.getById(id);

    this->tmpProduct->setProduct(product.getId());
    this->tmpProduct->setQuantity(1);

    auto modificationList = product.getModifications();
    if(modificationList.count() == 0){
        m_orderProductListModel->add(this->tmpProduct);
        delete (this->tmpProduct);
    }else {
        auto modGroups  = modiferGroups.GetModificationGroups(product.getModifications());

        m_modificationGroups->setList(modGroups);

        this->tmpProduct->setModifers(m_modificationGroups->parametrs());
        setModificationPanelStatus(true);
    }


}

void OrderController::updateProductModificationsInCart(ModiferList modiferList)
{    
    this->tmpProduct->setModifers(modiferList);
    m_orderProductListModel->replace(orderProductListModel()->getProductInUse(), this->tmpProduct);
    delete (this->tmpProduct);
}

void OrderController::arrangeOrder()
{

}


void OrderController::onOffersLoadSuccess(OfferList offers)
{
    qDebug()<<"получены акции" << offers.count();
    orderProductListModel()->processingOffers(offers);
}

void OrderController::onOffersLoadSuccessObject(QJsonDocument jdoc)
{

    auto statusOffersArray=  jdoc.object().value("Item").toArray();
    for(int i =0; i < statusOffersArray.count(); i++){
        auto statusOffer = statusOffersArray.at(i).toObject();
        auto offerList = OfferList(statusOffer.value("offers").toArray());

        orderProductListModel()->processingOffers(offerList);
    }
}

void OrderController::onLoadSettingSuccess(Setting setting)
{

    if(m_setting != setting){
        qDebug()<<"setting chached";
        m_setting = setting;
    }
}


void OrderController::setClientToOrderInProgress(QString phone)
{
    qDebug()<<"телефон " <<phone;
    auto order =  orders.getOrderById(orderObject()->id());
    order.setClient(phone);

    saveOrder(order);
    router->toProductChoice();
}

void OrderController::setTimeToOrderInProgress(QString time)
{
    auto order = orders.getOrderById(orderObject()->id());
    order.setCookInTime(QTime::fromString(time,"hh:mm"));
    saveOrder(order);
}



void OrderController::setStatusToOrderInProgress(QString status)
{
    auto order = orders.getOrderById(orderObject()->id());
    order.setStatus(status);
    m_order_model->updateStatus(order.getId(),status);
    saveOrder(order);
}

void OrderController::updateOrderDeliveryType(int type)
{
    auto order = orders.getOrderById(orderObject()->id());
    if(order.getDelivery() != type){
        order.setDelivery(type);
        saveOrder(order);
    }
}

void OrderController::saveButtonClicked()
{

    router->toMainPage();
}



void OrderController::onProductInOrderListChanged(ProductInOrderList value)
{
    auto order = orders.getOrderById(orderObject()->id());
    order.setProducts(value);
    saveOrder(order);
}

void OrderController::filterAllButtonClicked()
{
    m_orderFilterModel->setDeliveryType(-1);
    saveButtonClicked();

}

void OrderController::filterDeliveryButtonClicked()
{
    m_orderFilterModel->setDeliveryType(1);
    saveButtonClicked();
}

void OrderController::filterPickUpButtonClicked()
{
    m_orderFilterModel->setDeliveryType(0);
    saveButtonClicked();
}

void OrderController::productToEdit(int index)
{
    orderProductListModel()->toEdit(index);
}

void OrderController::productAddQuantity()
{
    if(orderProductListModel()->getProductInUse() != -1){
       orderProductListModel()->add();
    }
}

void OrderController::productReduceQuantity()
{

    if(orderProductListModel()->getProductInUse() != -1){orderProductListModel()->reduceQuantity(orderProductListModel()->getProductInUse());}

}

void OrderController::productRemove()
{
    if(orderProductListModel()->getProductInUse() != -1){orderProductListModel()->remove();}

}

void OrderController::productUpdateModifications()
{
    showModificationpanel(orderProductListModel()->getProductInUse());


}

void OrderController::showPaymentTypePanel()
{
    setPaymentTypePanelStatus(true);
}

void OrderController::closePaymentTypePanel()
{
    setPaymentTypePanelStatus(false);
}

void OrderController::showPaymentPanel(int type)
{
    closePaymentTypePanel();
    setSdachaPanelStatus(true);
    qDebug()<<type;


}

void OrderController::closePaymentPanel()
{
    setSdachaPanelStatus(false);
}



bool OrderController::orderIsPayd()
{
    return false;
}

void OrderController::showModificationpanel(int index)
{
    this->tmpProduct = new ProductInOrder();
    auto productToUpdate = m_orderProductListModel->at(index);
    this->tmpProduct->setProduct(productToUpdate.getProduct());
    this->tmpProduct->setQuantity(productToUpdate.getQuantity());
    this->tmpProduct->setModifers(productToUpdate.getModifers());
    this->tmpProduct->setComment(productToUpdate.getComment());
    auto product = productList.getById(productToUpdate.getProduct());
    auto modGroups  = modiferGroups.GetModificationGroups(product.getModifications());
    m_modificationGroups->updateList(tmpProduct->getModifers(),modGroups);
    setModificationPanelStatus(true);

}

OfferList OrderController::processOffersByStatus(OfferList offers, QString status)
{
    OfferList offerList;
    int indexOfStatus = m_status_list_model->indexOfStatusCode(status);
    for(int i =0; i < indexOfStatus;i++){
        offers.append(offers.at(i));
    }
    return  offerList;
}

void OrderController::checkUpdate()

{
    qDebug()<<"check";
    orderManager.update(orders.at(0));
}






void OrderController::load()
{

    timer->start(100);
    orderManager.list(QDate::currentDate());
}



bool OrderController::statusChangeOrderPanelVisible() const
{
    return m_statusChangeOrderPanelVisible;
}

bool OrderController::typeChangePanelVisible() const
{
    return m_typeChangePanelVisible;
}

bool OrderController::notProcessedOrderModalVisible() const
{
    return m_notProcessedOrderModalVisible;
}

SaveOrderListPoxyModel *OrderController::saveOrderProxyModel() const
{
    return m_saveOrderProxyModel;
}

bool OrderController::updateOrderSuccess() const
{
    return m_updateOrderSuccess;
}


void OrderController::saveOrder(Order order)
{
    qDebug()<<"i am hear >> "<< order.toJson();
    m_order_model->saveOrder(order);
    orders.saveOrder(order);
    orderManager.update(order);
    setUpdateOrderSuccess(false);
}

void OrderController::setAddressToOrder()
{
    auto address =  orderObject()->address()->toOrderAddress();
    auto order = orders.getOrderById(orderObject()->id());
    order.setAddress(address);
    qDebug()<<order.toJson();
    saveOrder(order);

}



bool OrderController::getAllow_inRoom() const
{
    qDebug()<<"BAR = "<<m_setting.getAllow_inRoom();
    return m_setting.getAllow_inRoom();
}


bool OrderController::getAllow_delivery() const
{
    qDebug()<<"delivery = "<<m_setting.getAllow_inRoom();
    return m_setting.getAllow_delivery();
}



bool OrderController::getAllow_pickUp() const
{
    qDebug()<<"pickup = "<<m_setting.getAllow_inRoom();
    return m_setting.getAllow_pickUp();
}


void OrderController::createNewOrder(Order order)
{
    m_order_model->set(order);
    orders.append(order);
    qDebug()<<order.toJson()<< "createOrder";
    m_orderProductListModel->setList(ProductInOrderList());
    orderManager.create(order);
}

void OrderController::onUpdateOrderSuccess(OrderWithCart order)
{
    timer->start(500);
    qDebug()<< "update";
    setUpdateOrderSuccess(true);
    auto bonus = order.getProducts().getBonusProduct();
    bonus.append(order.getCart().getProducts());

    m_orderProductListModel->updateList(bonus);
    m_orderObject->setOrderToProgress(order);
    }






bool OrderController::paymentTypePanelStatus() const
{
    return m_paymentTypePanelStatus;
}

int OrderController::orderPayd() const
{
    return m_orderPayd;
}

bool OrderController::paymentPanelStatus() const
{
    return m_paymentPanelStatus;
}


void OrderController::addNewPayment(int type, int summa)
{
    double summ1a = 0;

    setOrderPayd(orderPayd()+summa);
    PaymentObjectEntity payment;
    payment.setType(type);
    payment.setAmount(summa);
    paymentList.append(&payment);
    for(int i =0; i < paymentList.count(); i++){
        summ1a += paymentList.at(i)->getAmount();
    }
    if(orderPayd() == orderProductListModel()->orderCost()){
        //Marker
    }
}

void OrderController::setProductList(const ProductList &value)
{
    productList = value;
}

ProductInOrderListModel *OrderController::orderProductListModel() const
{
    return m_orderProductListModel;
}



void OrderController::setModiferGroups(const ModificationGroupList &value)
{
    modiferGroups = value;
}

StorageService *OrderController::getStorageService() const
{
    return storageService;
}

void OrderController::setStorageService(StorageService *value)
{
    storageService = value;
}

OrderEntityObject *OrderController::orderObject() const
{    
    return m_orderObject;
}

ModificationGrouplistModel *OrderController::modificationGroups() const
{
    return m_modificationGroups;
}

bool OrderController::modificationPanelStatus() const
{
    return m_modificationPanelStatus;
}

