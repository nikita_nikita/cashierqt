#ifndef ORDERCONTROLLER_H
#define ORDERCONTROLLER_H
#include "entity/Marketing/offer.h"
#include "objectcontroller.h"
#include "sales/ordermanager.h"
#include "sales/clientmanager.h"
#include "../entity/orderentityobject.h"
#include "entity/Sales/orderlist.h"
#include <QTimer>
#include "../model/orderlistmodel.h"
#include "../model/orderproxymodel.h"
#include "../model/orderadedmodel.h"
#include "../model/productlistmodel.h"
#include "../model/productlistproxymodel.h"
#include "../model/mordificationgrouplistmodel.h"
#include "../model/productinordermodel.h"
#include "../model/statuslistmodel.h"
#include "../model/orderfiltervisiblemodel.h"
#include "../model/saveorderlistpoxymodel.h"
class OrderController : public ObjectController
{
    Q_OBJECT
    //Список заказов на главном экране
    Q_PROPERTY(OrderListModel*  order_model MEMBER  m_order_model NOTIFY  orderModelchanged)    
    //Список сохранённых заказов
    Q_PROPERTY(SaveOrderListPoxyModel* saveOrderProxyModel READ saveOrderProxyModel WRITE setSaveOrderProxyModel NOTIFY saveOrderProxyModelChanged)
    //Заказ с которым работаем в данный момент (создание нового, редактирование уже имеющегося)
    Q_PROPERTY(OrderEntityObject * orderObject READ orderObject WRITE setOrderObject NOTIFY orderObjectChanged)
    //Модели списка продуктов
    Q_PROPERTY(ProductListModel* productListModel MEMBER m_productListModel NOTIFY productListModelChanged)
    Q_PROPERTY(bool notProcessedOrderModalVisible READ notProcessedOrderModalVisible WRITE setNotProcessedOrderModalVisible NOTIFY notProcessedOrderModalVisibleChanged)
    //Модели для поиска
    Q_PROPERTY(ProductListProxyModel* nameVisibleProduct MEMBER m_nameVisibleProduct NOTIFY nameVisibleProductChanged)
    Q_PROPERTY(OrderFilterVisibleModel* orderFilterModel MEMBER m_orderFilterModel NOTIFY deliveryFilterModelChanged)
    //Модели для выбора по категориям
    Q_PROPERTY(ProductListProxyModel* categoryVisibleProduct MEMBER m_categoryVisibleProduct NOTIFY categoryVisibleProductChanged)
    //Модели для вывода панели модификаторов
    Q_PROPERTY(ModificationGrouplistModel * modificationGroups READ modificationGroups WRITE setModificationGroups NOTIFY modificationGroupsChanged)
    //управление панелью модификаторов
    Q_PROPERTY(bool modificationPanelStatus READ modificationPanelStatus WRITE setModificationPanelStatus NOTIFY modificationPanelStatusChanged)
    //здесь должнга быть модель списка продуктов в заказе.
    Q_PROPERTY(ProductInOrderListModel * orderProductListModel READ orderProductListModel WRITE setOrderProductListModel NOTIFY orderProductListModelChanged)
    Q_PROPERTY(StatusListModel *  status_list_model MEMBER  m_status_list_model NOTIFY  status_listModelchanged)
    Q_PROPERTY(bool paymentTypePanelStatus READ paymentTypePanelStatus WRITE setPaymentTypePanelStatus NOTIFY paymentTypePanelStatusChanged)
    Q_PROPERTY(int orderPayd READ orderPayd WRITE setOrderPayd NOTIFY orderPaydChanged)
    Q_PROPERTY(bool paymentPanelStatus READ paymentPanelStatus WRITE setSdachaPanelStatus NOTIFY sdachaPanelStatusChanged)
    Q_PROPERTY(bool statusChangeOrderPanelVisible READ statusChangeOrderPanelVisible WRITE setStatusChangeOrderPanelVisible NOTIFY statusChangeOrderPanelVisibleChanged)
    Q_PROPERTY(bool typeChangePanelVisible READ typeChangePanelVisible WRITE setTypeChangePanelVisible NOTIFY typeChangePanelVisibleChanged)
    Q_PROPERTY(bool updateOrderSuccess READ updateOrderSuccess WRITE setUpdateOrderSuccess NOTIFY updateOrderSuccessChanged)


public:
    explicit OrderController(QObject *parent = nullptr);

    ProductInOrderListModel * orderProductListModel() const;
    SaveOrderListPoxyModel* saveOrderProxyModel() const;
    StorageService *getStorageService() const;    
    OrderListModel* orderAdedmodel() const;
    OrderEntityObject* orderObject() const;
    ModificationGrouplistModel * modificationGroups() const;

    void setModiferGroups(const ModificationGroupList &value);
    void setStorageService(StorageService *value);
    void setProductList(const ProductList &value);
    void setProductIndexAtWork(int value);
    void setStatusList(const StatusList &value);

    int orderPayd() const;
    int getProductIndexAtWork() const;
    bool orderIsPayd();
    bool paymentTypePanelStatus() const;
    bool paymentPanelStatus() const;
    bool statusChangeOrderPanelVisible() const;
    bool typeChangePanelVisible() const;
    bool saveOrderModalVisible() const;
    bool notProcessedOrderModalVisible() const;    
    bool updateOrderSuccess() const;
    bool modificationPanelStatus() const;

signals:
    void needClientListInfo(QList<QString> clientPhoneList);
    void orderModelchanged(OrderListModel* order_model);
    void orderObjectChanged(OrderEntityObject* orderObject);
    void categoryVisibleProductChanged(ProductListProxyModel* categoryVisibleProduct);
    void nameVisibleProductChanged(ProductListProxyModel* nameVisibleProduct);
    void productListModelChanged(ProductListModel* productListModel);
    void deliveryFilterModelChanged(OrderFilterVisibleModel* orderFilterModel);
    void modificationGroupsChanged(ModificationGrouplistModel * modificationGroups);
    void modificationPanelStatusChanged(bool modificationPanelStatus);
    void orderProductListChanged(ProductInOrderListModel * orderProductList);
    void orderProductListModelChanged(ProductInOrderListModel * orderProductListModel);
    void paymentTypePanelStatusChanged(bool paymentTypePanelStatus);
    void orderPaydChanged(int orderPayd);
    void sdachaPanelStatusChanged(bool paymentPanelStatus);
    void status_listModelchanged(StatusListModel * status_list_model);
    void statusChangeOrderPanelVisibleChanged(bool statusChangeOrderPanelVisible);
    void typeChangePanelVisibleChanged(bool typeChangePanelVisible);
    void saveOrderModalVisibleChanged(bool saveOrderModalVisible);
    void notProcessedOrderModalVisibleChanged(bool notProcessedOrderModalVisible);
    void saveOrderProxyModelChanged(SaveOrderListPoxyModel* saveOrderProxyModel);
    void updateOrderSuccessChanged(bool updateOrderSuccess);

public slots:
    //получеие разрешённых типов заказа
    bool getAllow_inRoom() const;
    bool getAllow_delivery() const;
    bool getAllow_pickUp() const;

    //работа с заказами
    void createNewOrder(Order order);
    void onUpdateOrderSuccess(OrderWithCart order); 
    void productToEdit(int index);
    void productAddQuantity();
    void productReduceQuantity();
    void productRemove();
    void productUpdateModifications();
    void saveButtonClicked();

    //работа с "панелями" такими как модификаторы, выбор оплаты и т.д.
    void showPaymentTypePanel();
    void closePaymentTypePanel();
    void showPaymentPanel(int type);
    void closePaymentPanel();    
    void setSdachaPanelStatus(bool sdachaPanelStatus);
    void setStatusChangeOrderPanelVisible(bool statusChangeOrderPanelVisible);
    void setTypeChangePanelVisible(bool typeChangePanelVisible);
    void setNotProcessedOrderModalVisible(bool notProcessedOrderModalVisible);
    void setUpdateOrderSuccess(bool updateOrderSuccess);

    //внутрення настройка
    virtual void setToken(QString &token);
    virtual void setServiceLocator(ServiceLocator *serviceLocator);
    void setConfig(Config *value);

    //Кнопки управления верхним меню
    void filterAllButtonClicked();
    void filterDeliveryButtonClicked();
    void filterPickUpButtonClicked();

    //работа с заказами
    void updateOrder(int index);
    void updateProductInOrder();
    void createNewOrder(int type);
    void setClientToOrderInProgress(QString phone);
    void setTimeToOrderInProgress(QString time);
    void setStatusToOrderInProgress(QString status);
    void updateOrderDeliveryType(int type);
    void saveOrder(Order order);
    void setAddressToOrder();


     //работа со списками клиентов, продуктов и тд. .
    void onLoadOrderList(OrderList orders);
    void onProductLoadSuccess(ProductList products, ProductCostHash productCost, ModificationGroupList modificationGrouphash);
    void onStatusLoadSuccess(StatusList statusList);
    void setOrderObject(OrderEntityObject* orderObject);
    void setProductFilterName(QString name);
    void onClientListUpdate(ClientList clientList);
    void setModificationGroups(ModificationGrouplistModel * modificationGroups);
    void addNewPayment(int type, int summa);
    void setModificationPanelStatus(bool modificationPanelStatus);
    void setModificationProductToCart(ModiferList modiferList);
    void setOrderProductListModel(ProductInOrderListModel * orderProductListModel);


    void setPaymentTypePanelStatus(bool paymentTypePanelStatus);

    void setOrderPayd(int orderPayd);
    void setSaveOrderProxyModel(SaveOrderListPoxyModel* saveOrderProxyModel);



private slots:
    void addToProductToCart(QString id);
    void updateProductModificationsInCart(ModiferList modiferList);
    void arrangeOrder();
    void onOffersLoadSuccess(OfferList offers);
    void onOffersLoadSuccessObject(QJsonDocument jdoc);
    void onLoadSettingSuccess(Setting setting);
    void onProductInOrderListChanged(ProductInOrderList value);
    void checkUpdate();



private:
    void load();

    void showModificationpanel(int index);
    OfferList processOffersByStatus(OfferList offers, QString status);

    Setting m_setting;
    QTimer *timer;
    StorageService *storageService;

    OrderList orders;
    OrderListModel *m_order_model;
    OrderProxyModel *m_proxy_model;
    OrderManager orderManager;

    ClientManager clientManager;
    SettingManager *settingManager;

    ModificationGroupList modiferGroups;
    ModificationGrouplistModel * m_modificationGroups;

    ProductList productList;
    ProductInOrder  *tmpProduct;
    ProductListModel *m_productListModel;
    ProductListProxyModel *m_categoryVisibleProduct;
    ProductListProxyModel *m_nameVisibleProduct;
    ProductInOrderListModel *m_orderProductListModel;

    StatusList statusList;
    StatusListModel * m_status_list_model;            
    OrderEntityObject* m_orderObject;
    OrderFilterVisibleModel * m_orderFilterModel;
    SaveOrderListPoxyModel* m_saveOrderProxyModel;
    PaymentListObjectEntity  paymentList;

    bool m_saveOrderModalVisible = false;
    bool m_notProcessedOrderModalVisible = false;
    bool m_updateOrderSuccess = true;
    bool m_modificationPanelStatus = false;
    bool m_statusChangeOrderPanelVisible = false;
    bool m_typeChangePanelVisible = false;
    bool m_paymentPanelStatus = false;
    bool m_paymentTypePanelStatus = false;
    int m_orderPayd;



};

#endif // ORDERCONTROLLER_H
