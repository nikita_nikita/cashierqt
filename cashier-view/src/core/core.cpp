#include "core.h"

Core::Core(QObject *parent) : QObject(parent)
{

    config = new Config;
    router  = new Router(this);
    serviceLocator = new ServiceLocator(this);
    loginController = new LoginController(this);
    orderController  = new OrderController(this);
    notoficationController = new NotificationController(this);
    clientController = new ClientController(this);
    //    registarate
    registrateController(loginController);
    registrateController(notoficationController);
    registrateController(orderController);
    registrateController(clientController);

    connect(orderController,&OrderController::needClientListInfo,clientController,&ClientController::onNeedClientInfoList);
    connect(clientController,&ClientController::clientListUpdate, orderController, &OrderController::onClientListUpdate);
    connect(clientController,&ClientController::clientChoice,orderController,&OrderController::setClientToOrderInProgress);

}

void Core::run()
{    
    setContextProprety(engine.rootContext());
    engine.load(QUrl("qrc:/view/main.qml"));
    this->readConfigFromFile();
    emit configChanged(config);

}
void Core::registrateController(ObjectController *controller)
{
    controller->setRouter(router);
    controller->setServiceLocator(this->serviceLocator);
    controllers.append(controller);

    connect(controller,&ObjectController::error, notoficationController,&NotificationController::errorNotification);
    connect(controller,&ObjectController::info,  notoficationController,&NotificationController::infoNotification);
    connect(controller,&ObjectController::warning,  notoficationController,&NotificationController::warningNotification);
    connect(this, &Core::configChanged, controller, &ObjectController::setConfig);

}


void Core::setContextProprety(QQmlContext *context)
{
    context->setContextProperty("router", router);
    context->setContextProperty("loginController", loginController);
    context->setContextProperty("notificationController", notoficationController);
    context->setContextProperty("orderController", orderController);
    context->setContextProperty("clientController", clientController);
    //    context->setContextProperty("orderController", orderController);

}

void Core::readConfigFromFile()
{
    QSettings conf("/home/nikita/Project/cashier/config.ini", QSettings::IniFormat);
    QString host = conf.value("HOST").toString();
    int port = conf.value("PORT").toInt();
    QString scheme = conf.value("SHCEME").toString();
    QString pointID = conf.value("POINT_ID").toString();
    QString newOrderStatus = conf.value("NEW_ORDER_STATUS").toString();
    QString notProcessedStatus = conf.value("NEW_ORDER_STATUS").toString();
    QString cityId = conf.value("NEW_ORDER_STATUS").toString();
    config->setHost(host);
    config->setPort(port);
    config->setScheme(scheme);
    config->setPointID(pointID);
    config->setNewOrderStatus(newOrderStatus);
    config->setNotProcessedStatus(notProcessedStatus);
    config->setCity_id(cityId);
    serviceLocator->setConfig(*config);

}

