#ifndef CORE_H
#define CORE_H

#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QSettings>


#include "src/service/servicelocator.h"
#include "src/config/config.h"
#include "src/config/loadconfig.h"
#include "../router/router.h"
#include "../controller/notoficationcontroller.h"
#include "../controller/ordercontroller.h"
#include "../controller/logincontroller.h"
#include "../controller/clientcontroller.h"
class Core:public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = nullptr);
    void run();
signals:
    void configChanged(Config *config);
private:
    void registrateController(ObjectController *controller);
    void setContextProprety(QQmlContext *context);
    void readConfigFromFile();
//    LoadConfig loadconfig;
    Config *config;
    Router * router;
    QQmlApplicationEngine engine;    
    ServiceLocator *serviceLocator;
    QList<ObjectController *> controllers;

    NotificationController *notificationController;
    LoginController *loginController;
    ClientController *clientController;
    NotificationController *notoficationController;
    OrderController  *orderController;

};

#endif // CORE_H
