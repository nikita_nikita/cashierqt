#include "adressobjectentity.h"
#include <QDebug>
AddressObjectEntity::AddressObjectEntity(QObject *parent) : QObject(parent)
{

}

Address AddressObjectEntity::toOrderAddress()
{
    Address addres;
    addres.setCity(this->city());
    addres.setRoom(this->room());
    addres.setFloor(this->floor());
    addres.setComment(this->comment());
    addres.setBuilding(this->building());
    addres.setEntrance(this->entrance());
    addres.setStreetId(this->streetId());
    addres.setStreetText(this->streetText());
    return addres;

}

void AddressObjectEntity::setData(Address address)
{
    this->setCity(address.getCity());
    this->setStreetId(address.getStreetId());
    this->setStreetText(address.getStreetText());
    this->setBuilding(address.getBuilding());
    this->setEntrance(address.getEntrance());
    this->setFloor(address.getFloor());
    this->setRoom(address.getRoom());
    this->setComment(address.getComment());

}

AddressObjectEntity::AddressObjectEntity()
{

}

AddressObjectEntity::AddressObjectEntity(Address address)
{
    this->setCity(address.getCity());
    this->setRoom(address.getRoom());
    this->setFloor(address.getFloor());
    this->setComment(address.getComment());
    this->setBuilding(address.getBuilding());
    this->setEntrance(address.getEntrance());
    this->setStreetId(address.getStreetId());
    this->setStreetText(address.getStreetText());
}

bool AddressObjectEntity::operator==(const AddressObjectEntity &right)
{
    if(this->city() != right.city()
            ||this->room() != right.room()
            ||this->floor() != right.floor()
            ||this->comment() != right.comment()
            ||this->building() != right.building()
            ||this->entrance() != right.entrance()
            ||this->streetId() != right.streetId()
            ||this->streetText() != right.streetText()
            ){
        return false;
    }

    return true;
}

void AddressObjectEntity::operator=(AddressObjectEntity &right)
{
    this->setCity(right.city());
    this->setRoom(right.room());
    this->setFloor(right.floor());
    this->setComment(right.comment());
    this->setBuilding(right.building());
    this->setEntrance(right.entrance());
    this->setStreetId(right.streetId());
    this->setStreetText(right.streetText());
}



AddressObjectEntity &AddressObjectEntity::operator*()
{
    return *this;
}


QJsonObject AddressObjectEntity::  toJson()
{
    QJsonObject jObj;
    jObj["city"] = this->city();
    jObj["street_id"] = this->streetId();
    jObj["street_text"] = this->streetText();
    jObj["building"] = this->building();
    jObj["entrance"] = this->entrance();
    jObj["floor"] = this->floor();
    jObj["room"] = this->room();
    jObj["comment"] =this->comment();
    return jObj;

}





QString AddressObjectEntity::city() const
{
    return m_city;
}

QString AddressObjectEntity::streetId() const
{
    return m_streetId;
}

QString AddressObjectEntity::streetText() const
{

    return m_streetText;
}

QString AddressObjectEntity::building() const
{
    return m_building;
}

int AddressObjectEntity::entrance() const
{
    return m_entrance;
}

int AddressObjectEntity::floor() const
{
    return m_floor;
}

int AddressObjectEntity::room() const
{
    return m_room;
}

QString AddressObjectEntity::comment() const
{
    return m_comment;
}

void AddressObjectEntity::setCity(QString city)
{
    if (m_city == city)
        return;

    m_city = city;
    emit cityChanged(m_city);
}

void AddressObjectEntity::setStreetId(QString streetId)
{
    if (m_streetId == streetId)
        return;

    m_streetId = streetId;
    emit streetIdChanged(m_streetId);
}

void AddressObjectEntity::setStreetText(QString streetText)
{
    if (m_streetText == streetText)
        return;

    m_streetText = streetText;
    emit streetTextChanged(m_streetText);
}

void AddressObjectEntity::setBuilding(QString building)
{
    if (m_building == building)
        return;

    m_building = building;
    emit buildingChanged(m_building);
}

void AddressObjectEntity::setEntrance(int entrance)
{
    if (m_entrance == entrance)
        return;

    m_entrance = entrance;
    emit entranceChanged(m_entrance);
}

void AddressObjectEntity::setFloor(int floor)
{
    if (m_floor == floor)
        return;

    m_floor = floor;
    emit floorChanged(m_floor);
}

void AddressObjectEntity::setRoom(int room)
{
    if (m_room == room)
        return;

    m_room = room;
    emit roomChanged(m_room);
}

void AddressObjectEntity::setComment(QString comment)
{
    if (m_comment == comment)
        return;

    m_comment = comment;
    emit commentChanged(m_comment);
}
