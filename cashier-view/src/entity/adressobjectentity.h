#ifndef ADRESSOBJECTENTITY_H
#define ADRESSOBJECTENTITY_H

#include <QObject>
#include <QJsonObject>
#include "entity/Sales/address.h"
class AddressObjectEntity : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString city READ city WRITE setCity NOTIFY cityChanged)
    Q_PROPERTY(QString streetId READ streetId WRITE setStreetId NOTIFY streetIdChanged)
    Q_PROPERTY(QString streetText READ streetText WRITE setStreetText NOTIFY streetTextChanged)
    Q_PROPERTY(QString building READ building WRITE setBuilding NOTIFY buildingChanged)
    Q_PROPERTY(int entrance READ entrance WRITE setEntrance NOTIFY entranceChanged)
    Q_PROPERTY(int floor READ floor WRITE setFloor NOTIFY floorChanged)
    Q_PROPERTY(int room READ room WRITE setRoom NOTIFY roomChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)


public:
    AddressObjectEntity();
    AddressObjectEntity(Address address);
    explicit AddressObjectEntity(QObject *parent = nullptr);

    Address toOrderAddress();
    void setData(Address address);


    QString city() const;

    QString streetId() const;

    QString streetText() const;

    QString building() const;

    int entrance() const;

    int floor() const;

    int room() const;

    QString comment() const;

    bool operator==(const AddressObjectEntity & right);
    void operator=(AddressObjectEntity & right);

    AddressObjectEntity &operator*();




    QJsonObject toJson();

signals:
    void cityChanged(QString city);

    void streetIdChanged(QString streetId);

    void streetTextChanged(QString streetText);

    void buildingChanged(QString building);

    void entranceChanged(int entrance);

    void floorChanged(int floor);

    void roomChanged(int room);

    void commentChanged(QString comment);



public slots:

    void setCity(QString city);
    void setStreetId(QString streetId);
    void setStreetText(QString streetText);
    void setBuilding(QString building);
    void setEntrance(int entrance);
    void setFloor(int floor);
    void setRoom(int room);
    void setComment(QString comment);


private:

    QString m_city = "";
    QString m_streetId = "";
    QString m_streetText = "";
    QString m_building = "";
    int m_entrance = -1;
    int m_floor = -1;
    int m_room = 1;
    QString m_comment;
};

#endif // ADRESSOBJECTENTITY_H
