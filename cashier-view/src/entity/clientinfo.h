#ifndef CLIENTINFO_H
#define CLIENTINFO_H

#include <QString>

class ClientInfo
{
public:
    ClientInfo();
private:
    int id;
    QString name;
    QString phone;
};

#endif // CLIENTINFO_H
