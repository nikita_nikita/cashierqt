#include "clientobjectentity.h"

ClientObjectEntity::ClientObjectEntity(QObject *parent)
    : QAbstractListModel(parent)
{
}

ClientObjectEntity::ClientObjectEntity(Client client)
{
    this->setName(client.getName());
    this->setPhone(client.getPhone());
    this->setComment(client.getPhone());
    this->setBithDate(client.getBirhDate().toString());
}

int ClientObjectEntity::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
}

QVariant ClientObjectEntity::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}

Client ClientObjectEntity::toClient()
{
    Client client;
    client.setName(this->name());
    client.setPhone(this->phone());
//    client.setBirhDate(QDate::fromString())
    client.setComment(this->comment());
    return client;
}
