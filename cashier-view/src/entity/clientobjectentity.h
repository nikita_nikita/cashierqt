#ifndef CLIENTOBJECTENTITY_H
#define CLIENTOBJECTENTITY_H

#include <QAbstractListModel>

#include"entity/Sales/client.h"

class ClientObjectEntity : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)
    Q_PROPERTY(QString bithDate READ bithDate WRITE setBithDate NOTIFY bithDateChanged)


public:
    explicit ClientObjectEntity(QObject *parent = nullptr);
    ClientObjectEntity(Client client);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QString name() const
    {
        return m_name;
    }

    QString phone() const
    {
        return m_phone;
    }

    QString comment() const
    {
        return m_comment;
    }

    QString bithDate() const
    {
        return m_bithDate;
    }

public slots:
    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(m_name);
    }

    void setPhone(QString phone)
    {
        if (m_phone == phone)
            return;

        m_phone = phone;
        emit phoneChanged(m_phone);
    }

    void setComment(QString comment)
    {
        if (m_comment == comment)
            return;

        m_comment = comment;
        emit commentChanged(m_comment);
    }

    void setBithDate(QString bithDate)
    {
        if (m_bithDate == bithDate)
            return;

        m_bithDate = bithDate;
        emit bithDateChanged(m_bithDate);
    }
    Client toClient();
signals:
    void nameChanged(QString name);

    void phoneChanged(QString phone);

    void commentChanged(QString comment);

    void bithDateChanged(QString bithDate);

private:
    QString m_name;
    QString m_phone;
    QString m_comment;
    QString m_bithDate;
};

#endif // CLIENTOBJECTENTITY_H
