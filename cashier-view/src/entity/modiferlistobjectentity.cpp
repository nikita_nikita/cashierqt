#include "modiferlistobjectentity.h"

ModiferListObjectEntity::ModiferListObjectEntity()
{

}

ModiferListObjectEntity::ModiferListObjectEntity(ModiferList *modifers)
{
    for(int i= 0; i <modifers->count();i++){
        ModiferObjectEntity modifer(modifers->at(i).getModifer(),modifers->at(i).getQuantity());
        this->append(&modifer);
    }

}

ModiferList ModiferListObjectEntity::toMidiferList()
{
    ModiferList result;
    for(int i = 0; i < this->count(); i++){
        auto mod = this->at(i)->toMidifer();
        result.append(mod);
    }
    return result;
}
