#ifndef MODIFERLISTOBJECTENITY_H
#define MODIFERLISTOBJECTENITY_H

#include "modiferobjectentity.h"
#include <QList>
#include <QJsonArray>
class ModiferListObjectEntity : public QList<ModiferObjectEntity *>
{
public:
    ModiferListObjectEntity();
    ModiferListObjectEntity(ModiferList *modifers);
    ModiferList toMidiferList();
};

#endif // MODIFERLISTOBJECTENITY_H
