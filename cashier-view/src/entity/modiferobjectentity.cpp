#include "modiferobjectentity.h"

ModiferObjectEntity::ModiferObjectEntity(QObject *parent) : QObject(parent)
{

}

ModiferObjectEntity::ModiferObjectEntity()
{

}

ModiferObjectEntity::ModiferObjectEntity(Modifer modifer)
{
    this->setProduct(modifer.getModifer());
    this->setQuantity(modifer.getQuantity());

}


ModiferObjectEntity::ModiferObjectEntity(QString modifer, int quantiti)
{
this->setProduct(modifer);
this->setQuantity(quantiti);
}


bool ModiferObjectEntity::operator==(const ModiferObjectEntity &right)
{
 if(this->product() != right.product() || this->quantity() != right.quantity()){
     return  false;
 }
 return true;
}

ModiferObjectEntity &ModiferObjectEntity::operator=(const ModiferObjectEntity &right)
{
    this->setProduct(right.product());
    this->setQuantity(right.quantity());

    return *this;


}



Modifer ModiferObjectEntity::toMidifer()
{
    Modifer modifer;
    modifer.setModifer(this->product());
    modifer.setQuantity(this->quantity());
    return modifer;
}


QString ModiferObjectEntity::product() const
{
    return m_product;
}

int ModiferObjectEntity::quantity() const
{
    return m_quantity;
}

void ModiferObjectEntity::setProduct(QString product)
{
    if (m_product == product)
        return;

    m_product = product;
    emit productChanged(m_product);
}

void ModiferObjectEntity::setQuantity(int quantity)
{
    if (m_quantity == quantity)
        return;

    m_quantity = quantity;
    emit quantityChanged(m_quantity);
}
