#ifndef MODIFEROBJECTENTITY_H
#define MODIFEROBJECTENTITY_H

#include <QObject>
#include <QJsonObject>
#include "entity/Sales/modiferlist.h"
class ModiferObjectEntity : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString product READ product WRITE setProduct NOTIFY productChanged)
    Q_PROPERTY(int quantity READ quantity WRITE setQuantity NOTIFY quantityChanged)
public:
    explicit ModiferObjectEntity(QObject *parent = nullptr);
    ModiferObjectEntity();
    ModiferObjectEntity(Modifer modifer);

    ModiferObjectEntity(QString modifer,int quantiti);

    QString product() const;
    int quantity() const;

    bool operator==(const ModiferObjectEntity & right);
    ModiferObjectEntity &operator=(const ModiferObjectEntity & right);

    Modifer toMidifer();



signals:

    void productChanged(QString product);

    void quantityChanged(int quantity);

public slots:
    void setProduct(QString product);
    void setQuantity(int quantity);
private:
    QString m_product;
    int m_quantity;
};

#endif // MODIFEROBJECTENTITY_H
