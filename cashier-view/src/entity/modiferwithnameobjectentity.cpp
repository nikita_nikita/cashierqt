#include "modiferwithnameobjectentity.h"

ModiferWithNameObjectEntity::ModiferWithNameObjectEntity()
{

}

QString ModiferWithNameObjectEntity::name() const
{
    return product_name;
}

void ModiferWithNameObjectEntity::setName(const QString &value)
{
    product_name = value;
}

QString ModiferWithNameObjectEntity::getId() const
{
    return id;
}

void ModiferWithNameObjectEntity::setId(const QString &value)
{
    id = value;
}

int ModiferWithNameObjectEntity::getQuantity() const
{
    return quantity;
}

void ModiferWithNameObjectEntity::setQuantity(int value)
{
    quantity = value;
}

bool ModiferWithNameObjectEntity::getSingleChoice() const
{
    return singleChoice;
}

void ModiferWithNameObjectEntity::setSingleChoice(bool value)
{
    singleChoice = value;
}

int ModiferWithNameObjectEntity::getPrice() const
{
    return price;
}

void ModiferWithNameObjectEntity::setPrice(int value)
{
    price = value;
}


