#ifndef MODIFERWITHNAMEOBJECTENTITY_H
#define MODIFERWITHNAMEOBJECTENTITY_H

#include <QString>
class ModiferWithNameObjectEntity
{

public:
    ModiferWithNameObjectEntity();

    QString name() const;
    void setName(const QString &value);


    QString getId() const;
    void setId(const QString &value);

    int getQuantity() const;
    void setQuantity(int value);
    bool getSingleChoice() const;
    void setSingleChoice(bool value);

    int getPrice() const;
    void setPrice(int value);

private:
    QString product_name;
    QString id;
    int quantity;
    int price;
    bool singleChoice;

};

#endif // MODIFERWITHNAMEOBJECTENTITY_H
