#include "notification.h"

Notification::Notification()
{

}

Notification::Notification(int type, QString messaga)
{
 this->type = type;
 this->message = messaga;
 this->read = false;
}

bool Notification::getReadStatus() const
{
    return read;
}

void Notification::setReadStatus(bool value)
{
    read = value;
}

QString Notification::getMessage() const
{
    return message;
}

void Notification::setMessage(const QString &value)
{
    message = value;
}

int Notification::getType() const
{
    return type;
}

void Notification::setType(int value)
{
    type = value;
}

QDateTime Notification::getDatetime() const
{
    return datetime;
}

void Notification::setDatetime(const QDateTime &value)
{
    datetime = value;
}
