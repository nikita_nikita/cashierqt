#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QString>
#include <type_traits>
#include <QDateTime>
class Notification
{

public:
    Notification();
    Notification(int type, QString messaga);
    bool getReadStatus() const;
    void setReadStatus(bool value);

    QString getMessage() const;
    void setMessage(const QString &value);

    int getType() const;
    void setType(int value);

    QDateTime getDatetime() const;
    void setDatetime(const QDateTime &value);

private:

    int type;
    QString message;
    bool read;
    QDateTime datetime;

};

#endif // NOTIFICATION_H
