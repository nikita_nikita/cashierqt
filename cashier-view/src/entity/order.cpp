#include "order.h"

Order::Order()
{

}

QString Order::getId() const
{
    return id;
}

void Order::setId(const QString &value)
{
    id = value;
}

QString Order::getPoint() const
{
    return point;
}

void Order::setPoint(const QString &value)
{
    point = value;
}

QString Order::getClient() const
{
    return client;
}

void Order::setClient(const QString &value)
{
    client = value;
}

QString Order::getParent() const
{
    return parent;
}

void Order::setParent(const QString &value)
{
    parent = value;
}

QString Order::getStatus() const
{
    return status;
}

void Order::setStatus(const QString &value)
{
    status = value;
}

int Order::getType() const
{
    return type;
}

void Order::setType(int value)
{
    type = value;
}

int Order::getDelivery() const
{
    return delivery;
}

void Order::setDelivery(int value)
{
    delivery = value;
}

int Order::getPerson() const
{
    return person;
}

void Order::setPerson(int value)
{
    person = value;
}

QDate Order::getCookInDate() const
{
    return cookInDate;
}

void Order::setCookInDate(const QDate &value)
{
    cookInDate = value;
}

QTime Order::getCookInTime() const
{
    return cookInTime;
}

void Order::setCookInTime(const QTime &value)
{
    cookInTime = value;
}

bool Order::getPaid() const
{
    return paid;
}

void Order::setPaid(bool value)
{
    paid = value;
}

QString Order::getLocalNumber() const
{
    return localNumber;
}

void Order::setLocalNumber(const QString &value)
{
    localNumber = value;
}

QString Order::getComment() const
{
    return comment;
}

void Order::setComment(const QString &value)
{
    comment = value;
}

int Order::getPreAmount() const
{
    return preAmount;
}

void Order::setPreAmount(int value)
{
    preAmount = value;
}

int Order::getAmount() const
{
    return amount;
}

void Order::setAmount(int value)
{
    amount = value;
}

QString Order::getPersonInCharge() const
{
    return personInCharge;
}

void Order::setPersonInCharge(const QString &value)
{
    personInCharge = value;
}

//QVariant Order::getOffers() const
//{
//    return offers;
//}

//void Order::setOffers(const QVariant &value)
//{
//    offers = value;
//}

Address Order::getAddress() const
{
    return address;
}

void Order::setAddress(const Address &value)
{
    address = value;
}



PaymentList Order::getPayments() const
{
    return payments;
}

void Order::setPayments(const PaymentList &value)
{
    payments = value;
}

ProductInOrderList Order::getProducts() const
{
    return products;
}

void Order::setProducts(const ProductInOrderList &value)
{
    products = value;
}

QList<QString> Order::getMarkers() const
{
    return markers;
}

void Order::setMarkers(const QList<QString> &value)
{
    markers = value;
}
