#ifndef ORDER_H
#define ORDER_H
#include <QString>
#include <QDateTime>
#include <QVariant>
#include "entity/Sales/paymentlist.h"
#include "entity/Sales/productinorderlist.h"
#include "entity/Sales/address.h"
class Order
{
public:
    Order();
    QString getId() const;
    void setId(const QString &value);

    QString getPoint() const;
    void setPoint(const QString &value);

    QString getClient() const;
    void setClient(const QString &value);

    QString getParent() const;
    void setParent(const QString &value);

    QString getStatus() const;
    void setStatus(const QString &value);

    int getType() const;
    void setType(int value);

    int getDelivery() const;
    void setDelivery(int value);

    int getPerson() const;
    void setPerson(int value);

    QDate getCookInDate() const;
    void setCookInDate(const QDate &value);

    QTime getCookInTime() const;
    void setCookInTime(const QTime &value);

    bool getPaid() const;
    void setPaid(bool value);

    QString getLocalNumber() const;
    void setLocalNumber(const QString &value);

    QString getComment() const;
    void setComment(const QString &value);

    int getPreAmount() const;
    void setPreAmount(int value);

    int getAmount() const;
    void setAmount(int value);

    QString getPersonInCharge() const;
    void setPersonInCharge(const QString &value);

//    QVariant getOffers() const;
//    void setOffers(const QVariant &value);

    Address getAddress() const;
    void setAddress(const Address &value);



    PaymentList getPayments() const;
    void setPayments(const PaymentList &value);

    ProductInOrderList getProducts() const;
    void setProducts(const ProductInOrderList &value);



    QList<QString> getMarkers() const;
    void setMarkers(const QList<QString> &value);

private:

    QString id;
    QString point;
    QString client;
    QString parent;
    QString status;
    int type;
    int delivery;
    int person;
    QDate cookInDate;
    QTime cookInTime;
    bool paid;
    QString localNumber;
    QString comment;
    int preAmount;
    int amount;
    QString personInCharge;
//    QVariant offers;
    Address address;
    QList<QString> markers;
    PaymentList payments;
    ProductInOrderList products;
};

#endif // ORDER_H
