#include "orderentityobject.h"
#include "orderentityobject.h"
#include <QDebug>
#include <QUuid>

OrderEntityObject::OrderEntityObject(QObject *parent) : QObject(parent)
{

    m_payments = new PaymentListObjectEntity();
    m_products  = new ProductListObjectEntity();
    m_address = new AddressObjectEntity(this);
    qDebug()<<&m_address<< "m_address";



}

OrderEntityObject::OrderEntityObject()
{

}

OrderEntityObject::OrderEntityObject(Order order)
{
    this->setId(order.getId());
    this->setPoint(order.getPoint());
    this->setClient(order.getClient());
    this->setParent(order.getParent());
    this->setStatus(order.getStatus());
    this->setType(order.getType());
    this->setDelivery(order.getDelivery());
    this->setPerson(order.getPerson());
    this->setCookInDate(order.getCookInDate().toString("yyyy-MM-dd"));
    this->setCookInTime(order.getCookInTime().toString("hh-mm"));
    this->setPaid(order.getPaid());
    this->setLocalNumber(order.getLocalNumber());
    this->setComment(order.getComment());
    this->setPreAmount(order.getPreAmount());
    this->setAmount(order.getAmount());
    this->setPersonInCharge(order.getPersonInCharge());
    this->m_address->setData(order.getAddress());





    //    ProductListObjectEntity orderProducts(order.getProducts());
    ProductListObjectEntity *products = new ProductListObjectEntity();
    for(int i = 0; i < order.getProducts().count(); i++){
        auto product = order.getProducts().at(i);

        ProductObjectEntity productObject(&product);

        products->append(&productObject);



    }


}

//Order OrderEntityObject::toOrder()
//{
//    Order order;
//    if(this->id().count() != 0){
//    order.setId(this->id());
//    }else
//    {
//order.setId(generateUUID());
//    }
//    order.setType(this->type());
//    order.setPoint(this->point());
//    order.setAmount(this->amount());
//    order.setClient(this->client());
//    order.setParent(this->parent());
//    order.setStatus(this->status());
//    order.setAddress(this->address()->toOrderAddress());
//    order.setComment(this->comment());
//  order.setMarkers();
//    order.setDelivery(this->delivery());
//    order.setPayments(this->payments()->toOrderPaymentList());
//    order.setProducts(this->products()->toOrderProduct());
//    order.setPreAmount(this->preAmount());
//    order.setCookInDate(QDate::fromString(this->cookInDate()));
//    order.setCookInTime(QTime::fromString(this->cookInTime()));
//    order.setLocalNumber(this->localNumber());
//    order.setPersonInCharge(this->personInCharge());
//    return order;
//}

void OrderEntityObject::setOrderToProgress(OrderWithCart order)
{
    this->setId(order.getId());
    this->setPoint(order.getPoint());

    this->setClient(order.getClient());

    this->setParent(order.getParent());

    this->setStatus(order.getStatus());

    this->setType(order.getType());

    this->setDelivery(order.getDelivery());

    this->setPerson(order.getPerson());


    this->setCookInDate(order.getCookInDate().toString("yyyy-MM-dd"));
    this->setCookInTime(order.getCookInTime().toString("hh-mm"));


    this->setPaid(order.getPaid());
    this->setLocalNumber(order.getLocalNumber());
    this->setComment(order.getComment());
    this->setPreAmount(order.getPreAmount());
    this->setAmount(order.getAmount());
    this->setPersonInCharge(order.getPersonInCharge());
    this->m_payments->setData(order.getPayments());
    this->m_address->setData(order.getAddress());
    auto productInOrder = order.getCart().getProducts();
    productInOrder.append(order.getProducts().getBonusProduct());
    this->m_products->setData(productInOrder);


}


QString OrderEntityObject::id() const
{
    return m_id;
}

QString OrderEntityObject::point() const
{
    return m_name;
}

QString OrderEntityObject::client() const
{
    return m_client;
}

QString OrderEntityObject::parent() const
{
    return m_parent;
}

QString OrderEntityObject::status() const
{
     auto index = statusList->indexOfCode(m_status);
    return statusList->at(index).getName();
}

int OrderEntityObject::type() const
{
    return m_type;
}

int OrderEntityObject::delivery() const
{
    return m_delivery;
}

QString OrderEntityObject::cookInTime() const
{
    return m_cookInTime;
}

bool OrderEntityObject::paid() const
{
    return m_paid;
}

QString OrderEntityObject::localNumber() const
{

    //     qDebug()<<&(m_address)<< " address()";
    return m_localNumber;
}

QString OrderEntityObject::comment() const
{
    return m_comment;
}

QString OrderEntityObject::orderComment() const
{
    if(m_comment.size() > 0){
        QString comment = "Коментарий к заказу<br>" + m_comment;
        return comment;
    }

    return "";

}

int OrderEntityObject::preAmount() const
{
    return m_preAmount;
}

int OrderEntityObject::amount() const
{
    return m_amount;
}

QString OrderEntityObject::personInCharge() const
{
    return m_personInCharge;
}

AddressObjectEntity *OrderEntityObject::address() const
{
    return m_address;
}

PaymentListObjectEntity *OrderEntityObject::payments() const
{
    return m_payments;
}



QString OrderEntityObject::cookInDate() const
{
    return m_cookInDate;
}

int OrderEntityObject::person() const
{
    return m_person;
}

int OrderEntityObject::indefer() const
{
    return m_indefer;
}

bool OrderEntityObject::isPayd()
{
    //    for(int i = 0; i < m_payments->count();i++){
    //        if(m_payments->at(i)->get)
    //    }
    return false;
}

ProductListObjectEntity *OrderEntityObject::products() const
{
    return m_products;
}



void OrderEntityObject::setId(QString id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit idChanged(m_id);
}

void OrderEntityObject::setPoint(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit pointChanged(m_name);
}

void OrderEntityObject::setClient(QString client)
{
    if (m_client == client)
        return;

    m_client = client;
    emit clientChanged(m_client);
}

void OrderEntityObject::setParent(QString parent)
{
    if (m_parent == parent)
        return;

    m_parent = parent;
    emit parentChanged(m_parent);
}

void OrderEntityObject::setStatus(QString status)
{
    qDebug()<<"set Status " << status;
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(m_status);
}

void OrderEntityObject::setType(int type)
{
    if (m_type == type)
        return;

    m_type = type;
    emit typeChanged(m_type);
}

void OrderEntityObject::setDelivery(int delivery)
{
    if (m_delivery == delivery)
        return;

    m_delivery = delivery;
    emit deliveryChanged(m_delivery);
}

void OrderEntityObject::setCookInTime(QString cookInTime)
{
    if (m_cookInTime == cookInTime)
        return;

    m_cookInTime = cookInTime;
    emit cookInTimeChanged(m_cookInTime);
}

void OrderEntityObject::setPaid(bool paid)
{
    if (m_paid == paid)
        return;

    m_paid = paid;
    emit paidChanged(m_paid);
}

void OrderEntityObject::setLocalNumber(QString localNumber)
{
    if (m_localNumber == localNumber)
        return;

    m_localNumber = localNumber;
    emit localNumberChanged(m_localNumber);
}

void OrderEntityObject::setComment(QString comment)
{
    if (m_comment == comment)
        return;

    m_comment = comment;
    emit commentChanged(m_comment);
}

void OrderEntityObject::setPreAmount(int preAmount)
{
    if (m_preAmount == preAmount)
        return;

    m_preAmount = preAmount;
    emit preAmountChanged(m_preAmount);
}

void OrderEntityObject::setAmount(int amount)
{
    if (m_amount == amount)
        return;

    m_amount = amount;
    emit amountChanged(m_amount);
}

void OrderEntityObject::setPersonInCharge(QString personInCharge)
{
    if (m_personInCharge == personInCharge)
        return;

    m_personInCharge = personInCharge;
    emit personInChargeChanged(m_personInCharge);
}

void OrderEntityObject::setAddress(AddressObjectEntity *address)
{
    qDebug()<<address<<" adress";
    if (m_address == address)
        return;

    m_address = address;
    emit addressChanged(m_address);
}

void OrderEntityObject::setPayments(PaymentListObjectEntity *payments)
{
    if (m_payments == payments)
        return;

    m_payments = payments;
    emit paymentsChanged(m_payments);
}


void OrderEntityObject::setPerson(int person)
{
    if (m_person == person)
        return;

    m_person = person;
    emit personChanged(m_person);
}

void OrderEntityObject::setIndefer(int indefer)
{
    if (m_indefer == indefer)
        return;

    m_indefer = indefer;
    emit indeferChanged(m_indefer);
}

void OrderEntityObject::setProducts(ProductListObjectEntity *products)
{
    if (m_products == products)
        return;

    m_products = products;
    emit productsChanged(m_products);
}

QString OrderEntityObject::generateUUID() const
{


    auto id =  QUuid::createUuid().toString();
    id.remove("{");
    id.remove("}");

    return id;
}

void OrderEntityObject::setData(Order order)
{
    this->setId(order.getId());
    this->setPoint(order.getPoint());
    this->setClient(order.getClient());
    this->setParent(order.getParent());
    this->setStatus(order.getStatus());
    this->setType(order.getType());
    this->setDelivery(order.getDelivery());
    this->setPerson(order.getPerson());
    this->setCookInDate(order.getCookInDate().toString("yyyy-MM-dd"));
    this->setCookInTime(order.getCookInTime().toString("hh-mm"));
    this->setPaid(order.getPaid());
    this->setLocalNumber(order.getLocalNumber());
    this->setComment(order.getComment());
    this->setPreAmount(order.getPreAmount());
    this->setAmount(order.getAmount());
    this->setPersonInCharge(order.getPersonInCharge());
    this->m_address->setData(order.getAddress());
}



void OrderEntityObject::setStatusList(StatusList *value)
{
    statusList = value;
}



void OrderEntityObject::setCookInDate(QString cookInDate)
{
    if (m_cookInDate == cookInDate)
        return;

    m_cookInDate = cookInDate;
    emit cookInDateChanged(m_cookInDate);
}


