﻿#ifndef ORDERENTITYOBJECT_H
#define ORDERENTITYOBJECT_H

#include <QObject>
#include "order.h"
#include <QDateTime>
#include "../model/newproductmodel.h"
#include "../entity/adressobjectentity.h"
#include "../entity/paymentlist.h"
#include "poductlistobjectentity.h"
#include "entity/Settings/statuslist.h"
#include "entity/Sales/orderwithcart.h"

class OrderEntityObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString point READ point WRITE setPoint NOTIFY pointChanged)
    Q_PROPERTY(QString client READ client WRITE setClient NOTIFY clientChanged)
    Q_PROPERTY(QString parent READ parent WRITE setParent NOTIFY parentChanged)
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int delivery READ delivery WRITE setDelivery NOTIFY deliveryChanged)
    Q_PROPERTY(int person READ person WRITE setPerson NOTIFY personChanged)
    Q_PROPERTY(QString point READ point WRITE setPoint NOTIFY pointChanged)
    Q_PROPERTY(QString cookInTime READ cookInTime WRITE setCookInTime NOTIFY cookInTimeChanged)
    Q_PROPERTY(QString cookInDate READ cookInDate WRITE setCookInDate NOTIFY cookInDateChanged)
    Q_PROPERTY(bool paid READ paid WRITE setPaid NOTIFY paidChanged)
    Q_PROPERTY(QString localNumber READ localNumber WRITE setLocalNumber NOTIFY localNumberChanged)
    Q_PROPERTY(QString comment READ orderComment WRITE setComment NOTIFY commentChanged)
    Q_PROPERTY(int preAmount READ preAmount WRITE setPreAmount NOTIFY preAmountChanged)
    Q_PROPERTY(int amount READ amount WRITE setAmount NOTIFY amountChanged)
    Q_PROPERTY(QString personInCharge READ personInCharge WRITE setPersonInCharge NOTIFY personInChargeChanged)

    Q_PROPERTY(AddressObjectEntity * address READ address WRITE setAddress NOTIFY addressChanged)

    Q_PROPERTY(PaymentListObjectEntity * payments READ payments WRITE setPayments NOTIFY paymentsChanged)

    Q_PROPERTY(ProductListObjectEntity *  products READ products WRITE setProducts NOTIFY productsChanged)

    Q_PROPERTY(int indefer READ indefer WRITE setIndefer NOTIFY indeferChanged)







public:
    explicit OrderEntityObject(QObject *parent = nullptr);
    OrderEntityObject();
    OrderEntityObject(Order order);
    void setOrderToProgress(OrderWithCart order);

    QString id() const;

    QString point() const;

    QString client() const;

    QString parent() const;

    QString status() const;

    int type() const;

    int delivery() const;

    QString cookInTime() const;

    bool paid() const;

    QString localNumber() const;

    QString comment() const;
    QString orderComment() const;

    int preAmount() const;

    int amount() const;

    QString personInCharge() const;


    AddressObjectEntity * address() const;

    PaymentListObjectEntity * payments() const;


    QString cookInDate() const;

    int person() const;

    int indefer() const;


    bool isPayd();



    ProductListObjectEntity * products() const;

    void setStatusList(StatusList *value);

signals:


    void idChanged(QString id);

    void pointChanged(QString point);

    void clientChanged(QString client);

    void parentChanged(QString parent);

    void statusChanged(QString status);

    void typeChanged(int type);

    void deliveryChanged(int delivery);

    void cookInTimeChanged(QString cookInTime);

    void paidChanged(bool paid);

    void localNumberChanged(QString localNumber);

    void commentChanged(QString comment);

    void preAmountChanged(int preAmount);

    void amountChanged(int amount);

    void personInChargeChanged(QString personInCharge);

    void addressChanged(AddressObjectEntity * address);

    void paymentsChanged(PaymentListObjectEntity * payments);


    void cookInDateChanged(QString cookInDate);

    void personChanged(int person);

    void indeferChanged(int indefer);

    void productsChanged(ProductListObjectEntity * products);

public slots:

void setId(QString id);

void setPoint(QString point);

void setClient(QString client);

void setParent(QString parent);

void setStatus(QString status);

void setType(int type);

void setDelivery(int delivery);

void setCookInTime(QString cookInTime);

void setPaid(bool paid);

void setLocalNumber(QString localNumber);

void setComment(QString comment);

void setPreAmount(int preAmount);

void setAmount(int amount);

void setPersonInCharge(QString personInCharge);


void setAddress(AddressObjectEntity * address);

void setPayments(PaymentListObjectEntity * payments);

void setCookInDate(QString cookInDate);
void setPerson(int person);

void setIndefer(int indefer);

void setProducts(ProductListObjectEntity * products);

QString generateUUID() const;
void setData(Order order);
private:

QString m_id;
QString m_name;
QString m_client;
QString m_parent;
QString m_status;
int m_type;
int m_delivery;
QString m_cookInTime;
QString m_cookInDate;
bool m_paid;
QString m_localNumber;
QString m_comment;
int m_preAmount;
int m_amount;
QString m_personInCharge;

PaymentListObjectEntity* m_payments;
int m_person;
void setData(QJsonObject jObj);

int m_indefer;

AddressObjectEntity * m_address;
StatusList *statusList;
ProductListObjectEntity * m_products;


};

#endif // ORDERENTITYOBJECT_H
