#include "paymentlist.h"

PaymentListObjectEntity::PaymentListObjectEntity()
{

}

void PaymentListObjectEntity::setData(PaymentList payments)
{
    this->clear();
    for(int i = 0; i < payments.count();i++){
        auto payment = payments.at(i);
        PaymentObjectEntity paymentObject(payment);
        this->append(&paymentObject);
    }
}



PaymentList PaymentListObjectEntity::toOrderPaymentList()
{
    PaymentList result;
for(int i = 0; i < this->count(); i++){
    result.append(this->at(i)->toPayment());
}
return  result;
}

