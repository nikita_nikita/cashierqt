#ifndef PAYMENTLISTOBJECTENITY_H
#define PAYMENTLISTOBJECTENITY_H

#include "paymentobjectentity.h"
#include <QList>
#include <QJsonArray>

class PaymentListObjectEntity: public QList<PaymentObjectEntity *>
{
public:
    PaymentListObjectEntity();    
    void setData(PaymentList payments);
    PaymentList toOrderPaymentList();

};

#endif // PAYMENTLISTOBJECTENITY_H
