#include "paymentobjectentity.h"


PaymentObjectEntity::PaymentObjectEntity(QObject *parent) : QObject(parent)
{

}

PaymentObjectEntity::PaymentObjectEntity(Payment payment)
{
    this->setType(payment.getType());
    this->setAmount(payment.getAmount());
    this->setId(payment.getId());
}

int PaymentObjectEntity::Type() const
{
    return m_Type;
}

double PaymentObjectEntity::Amount() const
{
    return m_Amount;
}

void PaymentObjectEntity::setType(int Type)
{
    if (m_Type == Type)
        return;

    m_Type = Type;
    emit TypeChanged(m_Type);
}

void PaymentObjectEntity::setAmount(double Amount)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_Amount, Amount))
        return;

    m_Amount = Amount;
    emit AmountChanged(m_Amount);
}

double PaymentObjectEntity::getAmount() const
{
    return m_Amount;
}

QString PaymentObjectEntity::getId() const
{
    return id;
}

void PaymentObjectEntity::setId(const QString &value)
{
    id = value;
}

Payment PaymentObjectEntity::toPayment()
{
    Payment payment;
    payment.setId(this->id);
    payment.setType(this->m_Type);
    payment.setAmount(this->m_Amount);
    return payment;
}
