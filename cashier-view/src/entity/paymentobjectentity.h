#ifndef PAYMENTOBJECTENTITY_H
#define PAYMENTOBJECTENTITY_H

#include <QObject>
#include <QJsonObject>
#include "entity/Sales/paymentlist.h"
class PaymentObjectEntity : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int Type READ Type WRITE setType NOTIFY TypeChanged)
    Q_PROPERTY(double Amount READ Amount WRITE setAmount NOTIFY AmountChanged)
    //Q_PROPERTY(bool isDone READ isDone WRITE setIsDone NOTIFY isDoneChanged)




public:
    explicit PaymentObjectEntity(QObject *parent = nullptr);
    PaymentObjectEntity(Payment payment);

    int Type() const;
    double Amount() const;

    QString getId() const;
    void setId(const QString &value);

    Payment toPayment();

    double getAmount() const;

//    bool isDone() const
//    {
//        return m_isDone;
//    }

signals:

    void TypeChanged(int Type);

void AmountChanged(double Amount);

void isDoneChanged(bool isDone);

public slots:
void setType(int Type);
void setAmount(double Amount);
//void setIsDone(bool isDone)
//{
//    if (m_isDone == isDone)
//        return;

//    m_isDone = isDone;
//    emit isDoneChanged(m_isDone);
//}

private:

int m_Type;
double m_Amount;
QString id;

//bool m_isDone;
};

#endif // PAYMENTOBJECTENTITY_H
