#include "poductlistobjectentity.h"

ProductListObjectEntity::ProductListObjectEntity()
{

}

ProductListObjectEntity::ProductListObjectEntity(ProductInOrder product)
{
    ProductObjectEntity paymentObject(&product);
    this->append(&paymentObject);

}

void ProductListObjectEntity::setData(ProductInOrderList products)
{
    this->clear();
    for(int i = 0; i < products.count();i++){
        auto product = products.at(i);
        ProductObjectEntity paymentObject(&product);
        this->append(&paymentObject);
    }
}



ProductInOrderList ProductListObjectEntity::toOrderProduct()
{
    ProductInOrderList products;
    for(int i =0; i < this->count(); i++){
        products.append(this->at(i)->toProductInOrder());
    }
    return products;
}

