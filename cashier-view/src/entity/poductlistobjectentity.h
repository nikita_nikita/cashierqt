#ifndef PRODUCTLISTOBJECTENTITY_H
#define PRODUCTLISTOBJECTENTITY_H
#include "productobjectentity.h"
#include "QList"
#include "entity/Sales/productinorderlist.h"
class ProductListObjectEntity :public QList<ProductObjectEntity *>
{
public:
    ProductListObjectEntity();
    ProductListObjectEntity(ProductInOrder product);


    void setData(ProductInOrderList products);
    void add(ProductObjectEntity productObject);
    ProductInOrderList toOrderProduct();
};

#endif // PRODUCTLISTOBJECTENTITY_H
