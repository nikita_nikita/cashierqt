#include "product.h"

Product::Product()
{

}
Product::Product(QJsonObject jObj)
{
    this->setData(jObj);

}

Product::Product(Dish dish)
{
    this->setId(dish.getId());
    this->setName(dish.getName());
    this->setDescriprion(dish.getDescriprion());
    this->setUnit(dish.getUnit());
    this->setCategory(dish.getCategory());
    this->setImage(dish.getImage());
    this->setColor(dish.getColor());
    this->setBarcode(dish.getBarcode());
    this->setWeight_flag(dish.getWeight_flag());
    this->setSort(dish.getSort());
    this->setTimeForPreparing(dish.getTimeForPreparing());
    this->setModifications(dish.getModificationgroup());
    this->setType(DISH);

}

Dish Product::toDish()
{
    Dish dish;
    dish.setId(this->getId());
    dish.setName(this->getName());
    dish.setDescriprion(this->getDescriprion());
    dish.setUnit(this->getUnit());
    dish.setCategory(this->getCategory());
    dish.setImage(this->getImage());
    dish.setColor(this->getColor());
    dish.setBarcode(this->getBarcode());
    dish.setWeight_flag(this->getWeight_flag());
    dish.setSort(this->getSort());
    dish.setTimeForPreparing(this->getTimeForPreparing());
    return dish;

}


Product::Product(Good good)
{
    this->setId(good.getId());
    this->setName(good.getName());
    this->setUnit(good.getUnit());
    this->setCategory(good.getCategory());
    this->setImage(good.getImage());
    this->setColor(good.getColor());
    this->setBarcode(good.getBarcode());
    this->setSort(good.getSort());
    this->setTimeForPreparing(QTime::fromMSecsSinceStartOfDay(0));
    this->setType(GOOD);
}

Good Product::toGood()
{
Good good;
good.setId(this->getId());
good.setName(this->getName());
good.setUnit(this->getUnit());
good.setCategory(this->getCategory());
good.setImage(this->getImage());
good.setColor(this->getColor());
good.setBarcode(this->getBarcode());
good.setSort(this->getSort());
return good;
}

Product::Product(Prepack prepack)
{
    this->setId(prepack.getId());
    this->setName(prepack.getName());
    this->setCategory(prepack.getCategory());
    this->setImage(prepack.getImage());
    this->setSort(prepack.getSort());
    this->setTimeForPreparing(QTime::fromMSecsSinceStartOfDay(0));
    this->setType(PREPACK);
}

Prepack Product::toPrepack()
{
    Prepack prepack;
    prepack.setId(this->getId());
    prepack.setName(this->getName());
    prepack.setCategory(this->getCategory());
    prepack.setImage(this->getImage());
    prepack.setSort(this->getSort());
    return prepack;

}

QString Product::getId() const
{
    return id;
}

void Product::setId(const QString &value)
{
    id = value;
}

QString Product::getName() const
{
    return name;
}

void Product::setName(const QString &value)
{
    name = value;
}

QString Product::getDescriprion() const
{
    return descriprion;
}

void Product::setDescriprion(const QString &value)
{
    descriprion = value;
}

QString Product::getUnit() const
{
    return unit;
}

void Product::setUnit(const QString &value)
{
    unit = value;
}

QString Product::getCategory() const
{
    return category;
}

void Product::setCategory(const QString &value)
{
    category = value;
}

QString Product::getImage() const
{
    return image;
}

void Product::setImage(const QString &value)
{
    image = value;
}

QString Product::getColor() const
{
    return color;
}

void Product::setColor(const QString &value)
{
    color = value;
}

QString Product::getBarcode() const
{
    return barcode;
}

void Product::setBarcode(const QString &value)
{
    barcode = value;
}

bool Product::getWeight_flag() const
{
    return weight_flag;
}

void Product::setWeight_flag(bool value)
{
    weight_flag = value;
}

int Product::getSort() const
{
    return sort;
}

void Product::setSort(int value)
{
    sort = value;
}

QTime Product::getTimeForPreparing() const
{
    return timeForPreparing;
}

void Product::setTimeForPreparing(const QTime &value)
{
    timeForPreparing = value;
}

QList<QString> Product::getModifications() const
{
    return modifications;
}

void Product::setModifications(const QList<QString> &value)
{
    modifications = value;
}

void Product::setData(QJsonObject json)
{
    this->setId(json.value("id").toString());
    this->setName(json.value("name").toString());
    this->setDescriprion(json.value("description").toString());
    this->setUnit(json.value("unit").toString());
    this->setCategory(json.value("category").toString());
    this->setImage(json.value("image").toString());
    this->setColor(json.value("color").toString());
    this->setBarcode(json.value("barcode").toString());
    this->setWeight_flag(json.value("weight_flag").toBool());
    this->setSort(json.value("sort").toInt());
    this->setTimeForPreparing(QTime::fromString(json.value("time_for_preparing").toString()));
//    this->setModifications(json.value("modificationgroups").toArray());
    auto modifergoupArray = json.value("modificationgroup").toArray();
    QList<QString> modiferGrouplist;
    for(int i = 0; i < modifergoupArray.count();i++){
        modiferGrouplist.append(modifergoupArray.at(i).toString());
    }
    this->setModifications(modiferGrouplist);
}

int Product::getPrice() const
{
    return price;
}

void Product::setPrice(int value)
{
    price = value;
}

ProductInOrder Product::toProductInOrder()
{
    ProductInOrder product;
    product.setProduct(this->getId());
    product.setQuantity(1);
    return product;
}


int Product::getType() const
{
    return type;
}

void Product::setType(int value)
{
    type = value;
}

