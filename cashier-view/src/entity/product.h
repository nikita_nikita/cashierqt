#ifndef PRODUCT_H
#define PRODUCT_H

#include <QJsonObject>
#include <QTime>

#include "entity/Menu/modificationgrouplist.h"
#include "entity/Menu/dish.h"
#include "entity/Menu/goods.h"
#include "entity/Menu/prepack.h"
#include "entity/Sales/productinorder.h"

class Product
{
public:

    enum TYPE{
        GOOD = 0,
        DISH = 1,
        PREPACK =2
    };
    Product();

    Product(QJsonObject jObj);
    QJsonObject toJson();

    Product(Dish dish);
    Dish toDish();

    Product(Good good);
    Good toGood();

    Product(Prepack prepack);
    Prepack toPrepack();


    QString getId() const;
    void setId(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    QString getDescriprion() const;
    void setDescriprion(const QString &value);

    QString getUnit() const;
    void setUnit(const QString &value);

    QString getCategory() const;
    void setCategory(const QString &value);

    QString getImage() const;
    void setImage(const QString &value);

    QString getColor() const;
    void setColor(const QString &value);

    QString getBarcode() const;
    void setBarcode(const QString &value);

    bool getWeight_flag() const;
    void setWeight_flag(bool value);

    int getSort() const;
    void setSort(int value);

    QTime getTimeForPreparing() const;
    void setTimeForPreparing(const QTime &value);

    QList<QString> getModifications() const;
    void setModifications(const QList<QString> &value);


    int getType() const;
    void setType(int value);

    int getPrice() const;
    void setPrice(int value);

    ProductInOrder toProductInOrder();




private:
    void setData(QJsonObject jObj);
    QString id;
    QString name;
    QString descriprion;
    QString unit;
    QString category;
    QString image;
    QString color;
    QString barcode;
    bool weight_flag;
    int sort;
    int type;
    int price;
    QTime timeForPreparing;
    QList<QString> modifications;
};

#endif // PRODUCT_H
