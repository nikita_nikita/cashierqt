#include "producthash.h"

ProductHash::ProductHash()
{

}

ProductHash::ProductHash(DishList dishes)
{
    for(int i = 0; i < dishes.count();i++ ){
        this->insert(dishes.at(i).getId(),dishes.at(i));
    }
}

ProductHash::ProductHash(GoodsList goods)
{
    for(int i = 0; i < goods.count();i++ ){
        this->insert(goods.at(i).getId(),goods.at(i));
    }

}

ProductHash::ProductHash(PrepackList prepacks)
{
    for(int i = 0; i < prepacks.count();i++ ){
        this->insert(prepacks.at(i).getId(),prepacks.at(i));
    }
}
