#ifndef PRODUCTHASH_H
#define PRODUCTHASH_H

#include <QHash>
#include "product.h"

#include "entity/Menu/dishlist.h"
#include "entity/Menu/goodslist.h"
#include "entity/Menu/prepacklist.h"
class ProductHash: public QHash<QString,Product>
{
public:
    ProductHash();
    ProductHash(DishList dishes);
    ProductHash(GoodsList goods);
    ProductHash(PrepackList prepacks);

};

#endif // PRODUCTHASH_H
