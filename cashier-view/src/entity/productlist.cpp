#include "productlist.h"

ProductList::ProductList()
{

}

ProductList::ProductList(DishList dishes)
{
    for(int i = 0; i < dishes.count();i++ ){
        this->append(dishes.at(i));
    }
}

ProductList::ProductList(GoodsList goods)
{
    for(int i = 0; i < goods.count();i++ ){
        this->append(goods.at(i));
    }
}

ProductList::ProductList(PrepackList prepacks)
{
    for(int i = 0; i < prepacks.count();i++ ){
        this->append(prepacks.at(i));
    }
}


Product ProductList::getById(QString productId) const
{
    for(int i = 0; i < this->count(); i++){
        if(productId == this->at(i).getId())
            return this->at(i);
    }
    return Product();
}

QString ProductList::getNameByID(QString productId) const
{
    for(int i = 0; i < this->count(); i++){
        if(productId == this->at(i).getId())
            return this->at(i).getName();
    }
    return QString::number(this->count());
}

QList<QString> ProductList::getModificationGroupbyId(QString productId)
{
    for(int i = 0; i < this->count(); i++){
        if(productId == this->at(i).getId())
            return this->at(i).getModifications();
    }
    return QList<QString>();
}

