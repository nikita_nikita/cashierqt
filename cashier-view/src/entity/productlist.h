#ifndef PRODUCTLIST_H
#define PRODUCTLIST_H

#include <QList>
#include "product.h"
#include "entity/Menu/modificationgrouplist.h"
#include "entity/Menu/dishlist.h"
#include "entity/Menu/goodslist.h"
#include "entity/Menu/prepacklist.h"
class ProductList :public QList<Product>
{
public:
    ProductList();
    ProductList(DishList dishes);
    ProductList(GoodsList goods);
    ProductList(PrepackList prepacks);
    Product getById(QString productId) const;
    QString getNameByID(QString productId) const;
    QList<QString> getModificationGroupbyId(QString id);

};

#endif // PRODUCTLIST_H
