#include "productobjectentity.h"

ProductObjectEntity::ProductObjectEntity(QObject *parent) : QObject(parent)
{

}

ProductObjectEntity::ProductObjectEntity(ProductInOrder *orderProduct)
{
    this->setComment(orderProduct->getComment());
    this->setProduct(orderProduct->getProduct());
    auto modifers = orderProduct->getModifers();    
    this->setModifers(new ModiferListObjectEntity(&modifers));
}


QString ProductObjectEntity::product() const
{
    return m_product;
}

int ProductObjectEntity::quantity() const
{
    return m_quantity;
}

QString ProductObjectEntity::comment() const
{
    return m_comment;
}


ProductInOrder ProductObjectEntity::toProductInOrder()
{
    ProductInOrder product;
    product.setComment(this->comment());
    product.setProduct(this->product());
    product.setModifers(this->modifers()->toMidiferList());
    product.setQuantity(this->quantity());

    return product;
}

ModiferListObjectEntity *ProductObjectEntity::modifers() const
{
    return m_modifers;
}



void ProductObjectEntity::setProduct(QString product)
{
    if (m_product == product)
        return;

    m_product = product;
    emit productChanged(m_product);
}

void ProductObjectEntity::setQuantity(int quantity)
{
    if (m_quantity == quantity)
        return;

    m_quantity = quantity;
    emit quantityChanged(m_quantity);
}

void ProductObjectEntity::setComment(QString comment)
{
    if (m_comment == comment)
        return;

    m_comment = comment;
    emit commentChanged(m_comment);
}

void ProductObjectEntity::setModifers(ModiferListObjectEntity *modifers)
{
    if (m_modifers == modifers)
        return;

    m_modifers = modifers;
    emit modifersChanged(m_modifers);
}

//void ProductObjectEntity::setModifications(const ModificationsList &value)
//{
//    modifications = value;
//}


