#ifndef PRODUCTOBJECTENTITY_H
#define PRODUCTOBJECTENTITY_H

#include <QObject>
#include "modiferlistobjectentity.h"
#include "entity/Sales/productinorderlist.h"
class ProductObjectEntity : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString product READ product WRITE setProduct NOTIFY productChanged)
    Q_PROPERTY(int  quantity READ quantity WRITE setQuantity NOTIFY quantityChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)
    Q_PROPERTY(ModiferListObjectEntity * modifers READ modifers WRITE setModifers NOTIFY modifersChanged)
    Q_PROPERTY(int saleType READ saleType WRITE setSaleType NOTIFY saleTypeChanged)
    Q_PROPERTY(int saleValue READ saleValue WRITE setSaleValue NOTIFY saleValueChanged)
    Q_PROPERTY(int offerType READ offerType WRITE setOfferType NOTIFY offerTypeChanged)

public:
    explicit ProductObjectEntity(QObject *parent = nullptr);
    ProductObjectEntity(ProductInOrder *orderProduct);
    QString product() const;
    int quantity() const;
    QString comment() const;
    ProductInOrder toProductInOrder();
    ModiferListObjectEntity * modifers() const;
    void setModifications(const ModificationsList &value);



    int saleType() const
    {
        return m_saleType;
    }

    int saleValue() const
    {
        return m_saleValue;
    }

    int offerType() const
    {
        return m_offerType;
    }

public slots:
    void setProduct(QString product);
    void setQuantity(int quantity);
    void setComment(QString comment);

    void setModifers(ModiferListObjectEntity * modifers);



    void setSaleType(int saleType)
    {
        if (m_saleType == saleType)
            return;

        m_saleType = saleType;
        emit saleTypeChanged(m_saleType);
    }

    void setSaleValue(int saleValue)
    {
        if (m_saleValue == saleValue)
            return;

        m_saleValue = saleValue;
        emit saleValueChanged(m_saleValue);
    }

    void setOfferType(int offerType)
    {
        if (m_offerType == offerType)
            return;

        m_offerType = offerType;
        emit offerTypeChanged(m_offerType);
    }

signals:
    void productChanged(QString product);
    void quantityChanged(int quantity);
    void commentChanged(QString comment);
    void modifersChanged(ModiferListObjectEntity * modifers);

    void saleTypeChanged(int saleType);

    void saleValueChanged(int saleValue);

    void offerTypeChanged(int offerType);

private:
    QString m_product;
    int m_quantity;
    QString m_comment;
    ModiferListObjectEntity * m_modifers;
    int m_saleType;
    int m_saleValue;
    int m_offerType;
};

#endif // PRODUCTOBJECTENTITY_H
