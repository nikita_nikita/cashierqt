#include "productofferdata.h"

ProductOfferData::ProductOfferData()
{

}

ProductOfferData::ProductOfferData(ActionData data)
{
 this->setData(data);
}

void ProductOfferData::setData(ActionData data)
{
    this->setSaleType(data.getSaleType());
    this->setOfferType(data.getType());
    this->setSaleValue(data.getValue());

}

int ProductOfferData::getOfferType() const
{
    return offerType;
}

void ProductOfferData::setOfferType(int value)
{
    offerType = value;
}

int ProductOfferData::getSaleValue() const
{
    return saleValue;
}

void ProductOfferData::setSaleValue(int value)
{
    saleValue = value;
}

int ProductOfferData::getSaleType() const
{
    return saleType;
}

void ProductOfferData::setSaleType(int value)
{
    saleType = value;
}
