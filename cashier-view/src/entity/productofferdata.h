#ifndef PRODUCTOFFERDATA_H
#define PRODUCTOFFERDATA_H

#include "entity/Marketing/actiondata.h"
class ProductOfferData
{
public:
    ProductOfferData();
    ProductOfferData(ActionData data);

    int getSaleType() const;
    void setSaleType(int value);

    int getSaleValue() const;
    void setSaleValue(int value);

    int getOfferType() const;
    void setOfferType(int value);

private:
    void setData(ActionData data);
    int saleType;
    int saleValue;
    int offerType;
};

#endif // PRODUCTOFFERDATA_H
