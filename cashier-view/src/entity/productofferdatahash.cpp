#include "productofferdatahash.h"

ProductOfferDataHash::ProductOfferDataHash()
{

}

ProductOfferDataHash::ProductOfferDataHash(ActionData actionData)
{
    this->setData(actionData);
}



void ProductOfferDataHash::setData(ActionData actionData)
{
    auto productIdList = actionData.getTarget();
    for(int i = 0; i < productIdList.count(); i++){
        this->insert(productIdList.at(i),actionData);
    }
}
