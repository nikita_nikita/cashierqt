#ifndef PRODUCTOFFERDATAHASH_H
#define PRODUCTOFFERDATAHASH_H

#include <QHash>
#include "productofferdata.h"
#include "entity/Marketing/actiondata.h"
class ProductOfferDataHash: public QHash<QString,ProductOfferData>
{
public:
    ProductOfferDataHash();
    ProductOfferDataHash(ActionData actionData);
private:
    void setData(ActionData actionData);

};

#endif // PRODUCTOFFERDATAHASH_H
