#include "categorylistmodel.h"

CategorylistModel::CategorylistModel(QObject *parent)
    : QAbstractListModel(parent)
{
    
}
int CategorylistModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return  m_list.count();

}


QHash<int, QByteArray> CategorylistModel::roleNames() const
{
    QHash<int, QByteArray> categories = QAbstractListModel::roleNames();
    categories[IdRole] = "id";
    categories[NameRole] = "name";
    categories[TypeRole] = "type";
    categories[ParentRole] = "parent";
    categories[SortRole] = "sort";
    return categories;
    
}


QVariant CategorylistModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto categories = m_list.at(index.row());

    switch (role) {
    case IdRole:
        return QVariant(categories.getId());
    case NameRole:
        return QVariant(categories.getName());
    case TypeRole:
        return QVariant(categories.getType());
    case ParentRole:
        return QVariant(categories.getParent());
    case SortRole:
        return QVariant(categories.getSort());

    }
    return QVariant();
}

void CategorylistModel::append(Category category)
{
    beginInsertRows(QModelIndex(), m_list.size(), m_list.size());
    this->m_list.append(category);
    endInsertRows();

}

void CategorylistModel::append(CategoryList categories)
{
    beginInsertRows(QModelIndex(), m_list.size(), m_list.size());
    this->m_list.append(categories);
    endInsertRows();
}
