#ifndef CATEGORYLISTMODEL_H
#define CATEGORYLISTMODEL_H

#include <QAbstractListModel>
#include "entity/Menu/categorylist.h"
class CategorylistModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum{
        IdRole = Qt::UserRole +1,
        NameRole,
        TypeRole,
        ParentRole,
        SortRole
    };
    explicit CategorylistModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    
    void append(Category category);
    void append(CategoryList categories);
private:
    QList<Category> m_list;
};

#endif // CATEGORYLISTMODEL_H
