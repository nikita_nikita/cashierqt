#include "clientlistmodel.h"

ClientListModel::ClientListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> ClientListModel::roleNames() const
{
    QHash<int, QByteArray> clients = QAbstractListModel::roleNames();
    clients[PhoneRole] = "client_phone";
    clients[NameRole] = "client_name";
    clients[CommentRole] = "client_comment";
    clients[BirthDateRole] = "client_birth_date";
    return clients;
}

int ClientListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid()){
        return 0;
    }
    return m_list.count();
}

QVariant ClientListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()){
        return QVariant();
    }
    auto client = m_list.at(index.row());
    switch (role) {
    case PhoneRole:
        return  client.getPhone();
    case NameRole:
        return  client.getName();
    case CommentRole:
        return client.getComment();
    case BirthDateRole:
        return  client.getBirhDate();
    default:
        return QVariant("что то пошло не так в  modificationListModel");
    }
}


void ClientListModel::setClientList(ClientList clientList)
{

    beginResetModel();
    m_list = clientList;
    endResetModel();
    qDebug()<<"matherFuccker";
}

Client ClientListModel::getClientByIndex(int index)
{
    return m_list.at(index);
}

void ClientListModel::clientChoice(int index)
{
    emit clientChoice(m_list.at(index).getName());
}

