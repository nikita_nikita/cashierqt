#ifndef CLIENTLISTMODEL_H
#define CLIENTLISTMODEL_H
#include <QDebug>
#include <QAbstractListModel>
#include "entity/Sales/clientlist.h"
#include "../entity/clientobjectentity.h"
class ClientListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum{
        PhoneRole = Qt::UserRole,
        NameRole,
        CommentRole,
        BirthDateRole
    };
    explicit ClientListModel(QObject *parent = nullptr);
    virtual QHash<int, QByteArray> roleNames() const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setClientList(ClientList clientList);
    Client getClientByIndex(int index);
public slots:
    void clientChoice(int index);
private:
    ClientList m_list;
signals:
    void clientChoice(QString phone);
};

#endif // CLIENTLISTMODEL_H
