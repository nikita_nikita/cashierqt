#include "modificationlistmodel.h"
#include <QDebug>
ModificationListModel::ModificationListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> ModificationListModel::roleNames() const
{
    QHash<int, QByteArray> modification = QAbstractListModel::roleNames();
    modification[IdRole] = "mod_id";
    modification[NameRole] = "mod_name";
    modification[QuantityRole] = "mod_quantity";
    modification[SingleChoiceRole] = "mod_singleChoice";
    return modification;
}

int ModificationListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()){
        return 0;
    }
    return m_list.count();
}


QVariant ModificationListModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid()){
        return QVariant();
    }
    auto modification = m_list.at(index.row());
    switch (role) {
    case IdRole:
        return  modification.getId();
    case QuantityRole:
        return modification.getQuantity();
    case NameRole:
        return  modification.name();
    case SingleChoiceRole:
        return  modification.getSingleChoice();
    default:
        return QVariant("что то пошло не так в  modificationListModel");
    }
}

bool ModificationListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        qDebug()<<"index is Not Valid in ModificationListModel";
        return false;
    }
    auto modification = m_list.at(index.row());
    switch (role) {
    case IdRole:
        return  false;
    case NameRole:
        return false;
    case QuantityRole:
        modification.setQuantity(value.toInt());
        if(modification.getSingleChoice()){
            choiseSinglemodification(modification.getId());
        }

    }
    m_list.replace(index.row(), modification);
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

void ModificationListModel::setModificationList(ModificationsList *value)
{
    modificationList = value;
}

void ModificationListModel::setModiferList(ModiferList *value)
{
    beginResetModel();
    modiferList = value;
    for(int i = 0; i < modificationList->count(); i++ ){
        auto index = modiferList->indexOfId(modificationList->at(i).getId());
        auto modifer = modiferList->at(index);
        //придумать название
        ModiferWithNameObjectEntity modicaca;
        modicaca.setId(modifer.getModifer());
        modicaca.setQuantity(modifer.getQuantity());
        auto modification = modificationList->at(i);
        modicaca.setName(modification.getName());
        modicaca.setSingleChoice(modification.getSingleChoice());
        m_list.append(modicaca);
        endResetModel();
    }
}

ModiferList ModificationListModel::getModiferList() const
{
    ModiferList result;
    for(int i = 0; i < m_list.count(); i++){
        auto modicaca =  m_list.at(i);
        Modifer modifer;
        modifer.setModifer(modicaca.getId());
        modifer.setQuantity(modicaca.getQuantity());
        result.append(modifer);
    }
    return result;
}

void ModificationListModel::choiseSinglemodification(QString id)
{

        beginResetModel();
        for(int i = 0; i < m_list.count(); i++){
            auto modifer = m_list.at(i);
            if(modifer.getId() == id){
                modifer.setQuantity(1);
                m_list.replace(i,modifer);
            }else{
                modifer.setQuantity(0);
            }
            m_list.replace(i,modifer);
        }
        endResetModel();

}






