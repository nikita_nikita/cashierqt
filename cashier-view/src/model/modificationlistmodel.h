#ifndef MODIFICATIONLISTMODEL_H
#define MODIFICATIONLISTMODEL_H

#include <QAbstractItemModel>
#include "entity/Menu/modificationgroup.h"
#include "../entity/modiferwithnameobjectentity.h"
#include "entity/Sales/modiferlist.h"
class ModificationListModel : public QAbstractListModel
{
    Q_OBJECT


public:
    enum{
        IdRole = Qt::UserRole,
        NameRole,
        QuantityRole,
        SingleChoiceRole
    };
    explicit ModificationListModel(QObject *parent = nullptr);
    virtual QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    void setModificationList(ModificationsList *value);
    void setModiferList(ModiferList *value);
    ModiferList getModiferList() const;
    void choiseSinglemodification(QString id);

public slots:

private:

    ModificationsList *modificationList;
    ModiferList  *modiferList;
    QList<ModiferWithNameObjectEntity> m_list;

};

#endif // MODIFICATIONLISTMODEL_H
