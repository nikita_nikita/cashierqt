#include "mordificationgrouplistmodel.h"
#include <QDebug>
ModificationGrouplistModel::ModificationGrouplistModel(QObject *parent)
    : QAbstractListModel(parent)
{

}



QHash<int, QByteArray> ModificationGrouplistModel::roleNames() const
{
    QHash<int, QByteArray> products = QAbstractListModel::roleNames();
    products[IdRole] = "modificationGroupId";
    products[NameRole] = "modificationGroupName";
    products[TypeRole] = "modificationGroupType";
    products[ModificationsRole] = "modificationList";
    return products;
}



int ModificationGrouplistModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return  m_list.count();
}



QVariant ModificationGrouplistModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    auto modificationGroups = m_list.at(index.row());
    switch (role){
    case IdRole:
        return QVariant(modificationGroups.getId());
    case TypeRole:
        return QVariant(modificationGroups.getType());
    case NameRole:
        return QVariant(modificationGroups.getName());
    case ModificationsRole:
        auto modificationsList = this->m_modList.at(index.row());
        return QVariant::fromValue(modificationsList);
    }
    // FIXME: Implement me!
    return QVariant();

}

bool ModificationGrouplistModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }
    auto modificationGroups = m_list.at(index.row());
    switch (role) {
    case IdRole:
        return  false;
    case TypeRole:
        return false;
    case NameRole:
        modificationGroups.setName(value.toString());
    }
    m_list.replace(index.row(), modificationGroups);
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

Qt::ItemFlags ModificationGrouplistModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}



void ModificationGrouplistModel::setList(const ModificationGroupList &list)
{
    this->isUpdate = false;
    m_list = list;
    m_parametrs = defaultParametrs();
    beginResetModel();

    creatModificationListModel();
    endResetModel();
}

ModificationListModel *ModificationGrouplistModel::modifications() const
{
    return m_modifications;
}

void ModificationGrouplistModel::updateList(ModiferList modiferList, ModificationGroupList modGroupList)
{

    this->isUpdate = true;
    beginResetModel();
    this->m_list = modGroupList;
    setParametrs(modiferList);
    if(modiferList.count() == 0){
        m_parametrs = defaultParametrs();
    }else{
    m_parametrs = processModificationList();
    }
    creatModificationListModel();

    endResetModel();
}

void ModificationGrouplistModel::setModifications(ModificationListModel *modifications)
{
    if (m_modifications == modifications)
        return;

    m_modifications = modifications;
    emit modificationsChanged(m_modifications);
}
void ModificationGrouplistModel::confrim()
{
    ModiferList resultlist;
    for(int i = 0; i < m_modList.count(); i++){
        auto modGroup = m_modList.at(i);
        resultlist.append(modGroup->getModiferList());
    }
    if(isUpdate)
    {
        emit updateModifications(resultlist);
    }else{
        emit replaceModifications(resultlist);
    }
}

void ModificationGrouplistModel::choice(QString id)
{
    auto modifications = m_list.getModifficationGroupnByModificationId(id).getModifications();
    for(int i = 0; i <modifications.count();i++ ){
        auto modification = modifications.at(i);
        auto atIndexId = modification.getId();
        auto index = m_parametrs.indexOfId(atIndexId);


        if(atIndexId == id){
            if(index != -1){
                auto mod = m_parametrs.at(index);
                if(m_parametrs.at(index).getModifer()==id){
                    mod.setQuantity(1);
                }else{
                    mod.setQuantity(0);
                }
                m_parametrs.replace(index,mod);
                qDebug()<<"i am hear";
                this->setParametrs(m_parametrs);
            }else{
                return;
            }

        }
    }

}



ModiferList ModificationGrouplistModel::defaultParametrs()
{
    ModiferList result;
    ModificationsList modifications =  m_list.getAllModifications();
    for(int i = 0; i < modifications.count(); i++){
        auto modification = modifications.at(i);
        result.append(Modifer(modification.getId(),modification.getDefaults()));
    }

    return  result;

}

ModiferList ModificationGrouplistModel::processModificationList()
{
    //взял список модификаций  у продукта
    auto modifications = m_list.getAllModifications();

    // создал список модификаций для заполнения
    ModiferList result;


    for(int i = 0; i <  modifications.count() ;i++){

        Modifer modifer;

        auto id = modifications.at(i).getId();
        qDebug()<<m_parametrs.indexOfId(id)<< "index of id  count";

        if(m_parametrs.indexOfId(id) == -1){
            modifer.setModifer(id);
            modifer.setQuantity(0);
        }else{
            modifer = m_parametrs.at(i);
        }

//        auto number =  m_parametrs.getQuantityById(id);
//        modifer.setModifer(id);
//        modifer.setQuantity(number);
//        }else{
//         modifer = m_parametrs.at(i);
//        }
        result.append(modifer);
        qDebug()<< result.count()<<"Элементов добавлено ! " ;
    }
     qDebug()<<"i am hear";
     return result;
}



void ModificationGrouplistModel::creatModificationListModel()
{
    clearModificationListModel();
    this->clearModificationListModel();
    for (int i =0; i<m_list.count(); i++){


        auto mod = new  ModificationListModel();

        auto modifications = m_list.at(i).getModifications();
        mod->setModificationList(&modifications);
        mod->setModiferList(&m_parametrs);
        this->m_modList.append(mod);
    }

}

void ModificationGrouplistModel::clearModificationListModel()
{
    for (int i=0; i<m_modList.count(); i++) {
        delete (m_modList.at(i));
    }
    m_modList.clear();
}

ModiferList ModificationGrouplistModel::parametrs() const
{
    return m_parametrs;
}

void ModificationGrouplistModel::setParametrs(const ModiferList &parametrs)
{
    m_parametrs = parametrs;
}

