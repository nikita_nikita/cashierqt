#ifndef MORDIFICATIONGROUPLISTMODEL_H
#define MORDIFICATIONGROUPLISTMODEL_H

#include <QAbstractItemModel>
#include "entity/Menu/modificationgrouplist.h"
#include "modificationlistmodel.h"
#include "entity/Sales/modiferlist.h"
#include "../entity/modiferwithnameobjectentity.h"
class ModificationGrouplistModel : public QAbstractListModel
{
    Q_OBJECT
    //    Q_PROPERTY(ModificationListModel * modifications READ modifications WRITE setModifications NOTIFY modificationsChanged)

public:
    enum{
        IdRole = Qt::UserRole,
        NameRole,
        TypeRole,
        QuantityRole,
        ModificationsRole

    };
    explicit ModificationGrouplistModel(QObject *parent = nullptr);

    // Header:
    virtual QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    void setList(const ModificationGroupList &list);

    ModificationListModel * modifications() const;
    void updateList(ModiferList modiferList, ModificationGroupList modGroupList);
    ModiferList parametrs() const;
    void setParametrs(const ModiferList &parametrs);

public slots:
    void setModifications(ModificationListModel * modifications);
    void choice(QString id);

    void confrim();
signals:    
    void modificationsChanged(ModificationListModel * modifications);
    void firstTypeModificationChanged(QString id, int quantity);
    void zeroTypeModificationChanged(QString id);
    void replaceModifications(ModiferList);
    void updateModifications(ModiferList);




private:
    ModiferList m_parametrs;
    ModificationGroupList m_list;

    ModificationListModel * m_modifications;
    QList<ModificationListModel *> m_modList;

    bool isUpdate = false;
    void creatModificationListModel();
    void clearModificationListModel();
    ModiferList defaultParametrs();
    ModiferList processModificationList();


};

#endif // MORDIFICATIONGROUPLISTMODEL_H
