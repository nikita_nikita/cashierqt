#ifndef NEWPRODUCTMODEL_H
#define NEWPRODUCTMODEL_H

#include <QAbstractItemModel>

class NewProductModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum{
        IdRole = Qt::UserRole+1,
        PointRole,
        ClientRole,
        ParentRole,
        StatusRole,

        TypeRole,
        DeliveryRole,
        PersonRole,
        CookInDateRole,
        CookInTimeRole,

        PaidRole,
        LocalNumberRole,
        CommentRole,
        PreAmountRole,
        AmountRole,
        PersonInChargeRole,
        AddressRole,
        PayMentsRole,
        ProductsRole,
        IndefierRole

    };
    explicit NewProductModel(QObject *parent = nullptr);

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
};

#endif // NEWPRODUCTMODEL_H
