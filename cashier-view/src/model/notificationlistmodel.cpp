#include "notificationlistmodel.h"
#include <QDebug>
NotificationListModel::NotificationListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int NotificationListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;


    return m_list.count();
}



QVariant NotificationListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto notifications = m_list.at(index.row());

    switch (role) {
    case ReadRole:
        return QVariant(notifications.getReadStatus());
    case TypeRole:
        return QVariant(notifications.getType());
    case MessageRole:
        return QVariant(notifications.getMessage());
    case TimeRole:
        return QVariant(notifications.getDatetime());
    case IndefRole:
        return QVariant(index.row());
    }

    // FIXME: Implement me!
    return QVariant();
}

QHash<int, QByteArray> NotificationListModel::roleNames() const
{
    QHash<int, QByteArray> notifications = QAbstractListModel::roleNames();
    notifications[ReadRole] = "read";
    notifications[TypeRole] = "type";
    notifications[MessageRole] = "message";
    notifications[TimeRole] = "time";
    notifications[IndefRole] = "indexer";
    return notifications;
}

void NotificationListModel::addMessage(Notification notification)
{

    beginInsertRows(QModelIndex(), m_list.size(), m_list.size());
    this->m_list.append(notification);
    endInsertRows();


}

void NotificationListModel::messageRead(int i)
{

    auto n  = this->m_list.at(i);

    n.setReadStatus(true);
    this->m_list.replace(i, n);
    QModelIndex index = createIndex(i, 0);
    emit dataChanged(index,index);

}

Notification NotificationListModel::itemAt(int index)
{
    return  this->m_list.takeAt(index);
}


