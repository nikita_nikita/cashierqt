#ifndef NOTIFICATIONLISTMODEL_H
#define NOTIFICATIONLISTMODEL_H

#include <QAbstractListModel>
#include "../entity/notification.h"
class NotificationListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum{
        ReadRole= Qt::UserRole,
        TypeRole,
        MessageRole,
        TimeRole,
        IndefRole
    };

    explicit NotificationListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void addMessage(Notification notification);
    void messageRead(int index);

    Notification itemAt(int index);
    
signals:

private:
    QList<Notification> m_list;
};

#endif // NOTIFICATIONLISTMODEL_H
