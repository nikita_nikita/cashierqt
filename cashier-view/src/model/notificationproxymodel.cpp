#include "notificationproxymodel.h"
#include <QDebug>

NotificationProxyModel::NotificationProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

void NotificationProxyModel::setReadFilter(bool read)
{
    this->m_readable = read;
    invalidateFilter();
}


bool NotificationProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, filterKeyColumn(), source_parent);
    bool result =sourceModel()->data(index, filterRole()).toBool() == this->m_readable;

    return result;
}
