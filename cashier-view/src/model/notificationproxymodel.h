#ifndef NOTIFICATIONPROXYMODEL_H
#define NOTIFICATIONPROXYMODEL_H
#include <QSortFilterProxyModel>

class NotificationProxyModel: public QSortFilterProxyModel

{
    Q_OBJECT
public:
    explicit NotificationProxyModel(QObject *parent = nullptr);
    void setReadFilter(bool read);
    private:
    bool m_readable;
    protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

};


#endif // NOTIFICATIONPROXYMODEL_H
