#ifndef ORDERADEDMODEL_H
#define ORDERADEDMODEL_H

#include <QAbstractItemModel>
#include <QList>
#include "../entity/orderentityobject.h"

#include "../entity/producthash.h"
#include "entity/Menu/produccosthash.h"
class OrderAdedModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit OrderAdedModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;


    void add(Order order);

    void setProducts(const ProductHash &value);
    void setProductCost(const ProductCostHash &value);

private:
    ProductHash products;
    ProductCostHash productCost;
    QString getAddressString(Address adress) const;
    QString getProductString(ProductInOrderList productlist) const;
    QList<OrderEntityObject *> m_list;
};

#endif // ORDERADEDMODEL_H
