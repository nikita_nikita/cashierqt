#include "orderfiltervisiblemodel.h"

OrderFilterVisibleModel::OrderFilterVisibleModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

bool OrderFilterVisibleModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, filterKeyColumn(), source_parent);
    if(m_deliveryType == -1){
        return true;
    }
    bool result = sourceModel()->data(index, filterRole()).toInt() == m_deliveryType;
    return result;

}


void OrderFilterVisibleModel::setDeliveryType(int deliveryType)
{

    if (m_deliveryType == deliveryType)
        return;
    m_deliveryType = deliveryType;
    emit deliveryTypeChanged(m_deliveryType);
    invalidateFilter();
}




