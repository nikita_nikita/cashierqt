#ifndef ORDERFILTERVISIBLEMODEL_H
#define ORDERFILTERVISIBLEMODEL_H
#include <QSortFilterProxyModel>
//#include "orderlistmodel.h"
class OrderFilterVisibleModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit OrderFilterVisibleModel(QObject *parent = nullptr);
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    void setDeliveryType(int deliveryType);



signals:
    void deliveryTypeChanged(int deliveryType);


private:
    int m_deliveryType = -1;


};



#endif // ORDERFILTERVISIBLEMODEL_H
