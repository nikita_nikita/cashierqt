#include "orderlistmodel.h"
#include <QDebug>
OrderListModel::OrderListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int OrderListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;
    return m_list.count();
    // FIXME: Implement me!
}

QHash<int, QByteArray> OrderListModel::roleNames() const
{
    QHash<int, QByteArray> orders = QAbstractListModel::roleNames();
    orders[IdRole] = "id";
    orders[PointRole] = "type";
    orders[ClientNameRole] = "name";
    orders[ClientRole] = "client";
    orders[ParentRole] = "parent";
    orders[StatusRole] = "status";

    orders[TypeRole] = "type";
    orders[DeliveryRole] = "delivery";
    orders[PersonRole] = "person";
    orders[CookInDateRole] = "cook_in_date";
    orders[CookInTimeRole] = "cook_in_time";

    orders[PaidRole] = "paid";
    orders[LocalNumberRole] = "local_number";
    orders[CommentRole] = "comment";
    orders[PreAmountRole] = "pre_amount";
    orders[AmountRole] = "amount";
    orders[PersonInChargeRole] = "person_in_charge";
    orders[AddressRole] = "address";
    orders[PayMentsRole] = "payments";
    orders[ProductsRole] = "products";
    orders[IndefierRole] = "indefier";

    return orders;


}

QVariant OrderListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();


    auto orders = m_list.at(index.row());

    switch (role) {
    case IdRole:
        return QVariant(orders.getId());
    case PointRole:
        return QVariant(orders.getPoint());
    case  ClientNameRole:
        qDebug()<<orders.getClient();
        return QVariant(clientList.getByPhone(orders.getClient()).getName());
    case ClientRole:
        return QVariant(orders.getClient());
    case ParentRole:
        return QVariant(orders.getParent());
    case StatusRole:
        return QVariant(orders.getStatus());
    case TypeRole:
        return QVariant(orders.getType());
    case DeliveryRole:
        return QVariant(orders.getDelivery());
    case PersonRole:
        return QVariant(orders.getPerson());
    case CookInDateRole:
        return QVariant(orders.getCookInDate());
    case CookInTimeRole:
        return QVariant(orders.getCookInTime().toString("h:m"));
    case PaidRole:
        return QVariant(orders.getPaid());
    case LocalNumberRole:
        return QVariant(orders.getLocalNumber());
    case CommentRole:
        return QVariant(orders.getComment());
    case PreAmountRole:
        return QVariant(orders.getPreAmount());
    case AmountRole:
        return QVariant(orders.getAmount());
    case PersonInChargeRole:
        return QVariant(orders.getPersonInCharge());
    case AddressRole:
        if(orders.getDelivery() == 1){
            if(orders.getAddress().getEntrance() == -1){
                return "Доставка, адрес не задан";
            }
        return QVariant(getAddressString(orders.getAddress()));}
        if(orders.getDelivery() == 0){
            return "Самовывоз";
        }
        if(orders.getDelivery() == 2){
            return "В зале";
        }
    case PayMentsRole:
        return QVariant(getLostPaid(orders.getAmount(),orders.getPayments()));
    case ProductsRole:
        return QVariant(getProductString(orders.getProducts()));
    case IndefierRole:
        return index.row();

    }
    // FIXME: Implement me!
    return QVariant();
}

Order OrderListModel::getOrderByID(QString id)
{
    return m_list.getOrderById(id);
}

void OrderListModel::updateStatus(QString id, QString statuss)
{
    auto indexOfId =  m_list.indexOfId(id);
    auto order = m_list.at(indexOfId);
    order.setStatus(statuss);
    m_list.replace(indexOfId,order);
    emit dataChanged(index(indexOfId,0),index(indexOfId,0), QVector<int>(StatusRole));
}



void OrderListModel::set(OrderList orders)
{
    if(m_list == orders){
        return;
    }

    auto rightIds =  compeirIDsToGetIdsOfNewOrders(orders.getAllId());
        beginInsertRows(index(m_list.count(),0),m_list.count(),orders.count()-1);
    for(int i =0; i< rightIds.count();i++){
        auto rightIndex = orders.indexOfId(rightIds.at(i));
        this->m_list.append(orders.at(rightIndex));
    }
        endInsertRows();

}

void OrderListModel::update(int index, Order order)
{
    this->m_list.replace(index,order);
}

void OrderListModel::saveOrder(Order order)
{
    auto indexOf = m_list.indexOfId(order.getId());
    if( indexOf != -1){
        m_list.replace(indexOf,order);
        emit dataChanged(index(indexOf,0),index(indexOf,0));
    }
    else {
        this->set(order);
    }
}

void OrderListModel::set(Order order)
{
    auto lastIndex = m_list.count();
    beginInsertRows(index(lastIndex,0),lastIndex,lastIndex);
    this->m_list.append(order);
    endInsertRows();
}

void OrderListModel::setProducts(const ProductList &value)
{
    products = value;
}

void OrderListModel::setProductCost(const ProductCostHash &value)
{
    productCost = value;
}

int OrderListModel::indexOfOrderID(QString id)
{
    for(int i = 0; i < m_list.count();i++){
        if(id == m_list.at(i).getId()){
            return i;
        }
    }
    return -1;
}

Order OrderListModel::getOrderByIndex(int index)
{
    return m_list.at(index);
}


QList<QString> OrderListModel::compeirIDsToGetIdsOfNewOrders(QList<QString> rightIds)
{
    auto leftIds = this->m_list.getAllId();
    for(int i = 0; i < leftIds.count(); i++){
        auto rightIndex =  rightIds.indexOf(leftIds.at(i));
        if(rightIndex != -1){
            beginRemoveRows(index(0,0),i,i);
            rightIds.removeAt(rightIndex);
            leftIds.removeAt(i);
            endRemoveRows();
            i--;
        }
    }

    return rightIds;
}

void OrderListModel::setClientList(ClientList clients)
{

    beginResetModel();
    clientList.append(clients);
    endResetModel();
    //    emit dataChanged(index(0,0), index(m_list.count()-1,0),QVector<int>() << ClientNameRole);
}

double OrderListModel::getLostPaid(int orderCost, PaymentList payments) const
{
    double result = 0;
    for(int i = 0; i < payments.count();i++){
        result += payments.at(i).getAmount();
    }
    return orderCost-result;
}


QString OrderListModel::getAddressString(Address adress) const
{
    QString addressString = adress.getStreetText() + " " + adress.getBuilding() + " " + QString::number(adress.getEntrance());
    return  addressString;
}

QString OrderListModel::getProductString(ProductInOrderList productlist) const
{
    QString productString;
    for (int i = 0; i < productlist.count(); i++){
        auto product = productlist.at(i);
        productString += products.getById(product.getProduct()).getName() + " " +QString::number(product.getQuantity()) + " шт. ";

    }
    return  productString;

}


