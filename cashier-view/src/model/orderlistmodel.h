#ifndef ORDERLISTMODEL_H
#define ORDERLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "entity/Sales/orderlist.h"
#include "entity/Sales/address.h"
#include "entity/Sales/productinorderlist.h"
#include "../entity/productlist.h"
#include "entity/Menu/produccosthash.h"
#include "entity/Sales/clientlist.h"


#include <QModelIndex>

class OrderListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum{
        IdRole = Qt::UserRole+1,
        PointRole,
        ClientNameRole,
        ClientRole,
        ParentRole,
        StatusRole,
        TypeRole,
        DeliveryRole,
        PersonRole,
        CookInDateRole,
        CookInTimeRole,
        PaidRole,
        LocalNumberRole,
        CommentRole,
        PreAmountRole,
        AmountRole,
        PersonInChargeRole,
        AddressRole,
        PayMentsRole,
        ProductsRole,
        IndefierRole,
    };

    explicit OrderListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;



    Order getOrderByID(QString id);

    void updateStatus(QString id, QString statuss);
    void set(Order order);
    void set(OrderList orders);

    void update(int index, Order order);
    void saveOrder(Order order);
    void setProducts(const ProductList &value);
    void setProductCost(const ProductCostHash &value);
    int indexOfOrderID(QString id);
    Order getOrderByIndex(int index);
    //Кидаем в функцию список id
    //сравниваем с текущими
    //если какой то заказ нам больше не виден(например, перенаправлен на другую точку), он будет автоматически удалён из m_list
    //в ответ получаем список id новых заказов
    QList<QString> compeirIDsToGetIdsOfNewOrders(QList<QString> emitOrderIds);
signals:
    void clientChoice(QString phone);
public slots:
    void setClientList(ClientList clients);
private:
    double getLostPaid(int orderCost, PaymentList payments) const;
    ProductList products;

    ProductCostHash productCost;
    QString getAddressString(Address adress) const;
    QString getProductString(ProductInOrderList productlist) const;
    OrderList m_list;
    ClientList clientList;
};

#endif // ORDERLISTMODEL_H
