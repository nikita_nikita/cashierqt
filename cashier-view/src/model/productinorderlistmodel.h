#ifndef PRODUCTINORDERLISTMODEL_H
#define PRODUCTINORDERLISTMODEL_H

#include <QAbstractListModel>

class ProductInOrderListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ProductInOrderListModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
};

#endif // PRODUCTINORDERLISTMODEL_H
