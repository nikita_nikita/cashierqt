#include "productinordermodel.h"
#include <QDebug>
ProductInOrderListModel::ProductInOrderListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int ProductInOrderListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;
    return  m_list.count();
    // FIXME: Implement me!
}

QHash<int, QByteArray> ProductInOrderListModel::roleNames() const
{
    QHash<int, QByteArray> products = QAbstractListModel::roleNames();
    products[IdRole] = "id";
    products[NameRole] = "name";
    products[PriceRole] = "price";
    products[QuantityRole] = "quantity";
    products[ModificationGroupRole] = "modificationlist";
    products[ProductWithModificationsRole] = "productWithModifications";
    products[ProductEditorRole] = "toEdit";
    return products;
}

QVariant ProductInOrderListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto products = m_list.at(index.row());
    switch (role) {
    case IdRole:
        return QVariant(products.getProduct());
    case NameRole:
        return QVariant(getName(products.getProduct()));
    case PriceRole:
        if(products.IsBonusProduct()){
            return 0;
        }else{
        return QVariant(products.getAmount());
        }
    case ModificationGroupRole:
        return QVariant(getModificationList(products.getModifers()));
    case QuantityRole:
        return QVariant(products.getQuantity());
    case ProductEditorRole:
        qDebug()<<productInUse;
        return  index.row() == productInUse;
    case ProductWithModificationsRole:
        auto modif = products.getModifers();
        if(modif.count() > 0 ){
            return true;
        }
        else{
            return false;
        }

    }

    // FIXME: Implement me!
    return QVariant();
}

void ProductInOrderListModel::add(ProductInOrder *product)
{
    auto id = product->getProduct();

    auto index = m_list.indexOfProductInOrder(product);
    qDebug()<<"Индекс " << index;
    if(index  == -1)
    {
        m_list.append(*product);
        emit productListChanged(m_list);
    }
    else{
        qDebug()<<"Плюсанулся";

        auto prod =   m_list.at(index);
        prod.setQuantity(prod.getQuantity()+1);
        m_list.replace(index,prod);
        emit productListChanged(m_list);

    }
}

void ProductInOrderListModel::add()
{

auto product = m_list.at(productInUse);
product.setQuantity(product.getQuantity()+1);


m_list.replace(productInUse,product);
emit productListChanged(m_list);
//emit dataChanged(index(productInUse,0),index(productInUse,0));
}

void ProductInOrderListModel::remove()
{
//    beginResetModel();
    m_list.removeAt(productInUse);
    setProductInUse(-1);
    setChoiceProductHaveModification(false);
    emit productListChanged(m_list);
//    endResetModel();
}

QString ProductInOrderListModel::getName(QString productId) const
{
    return productList.getNameByID(productId);
}

int ProductInOrderListModel::getPrice(QString productId) const
{

    return costHash.value(productId);
}

QJsonArray ProductInOrderListModel::getModificationList(ModiferList modifers) const
{
    QJsonArray result;

    for(int i = 0;i < modifers.count(); i++){
        auto modifer = modifers.at(i);
        auto quantity = modifer.getQuantity();


        auto modificationGroup = modifications.getModifficationGroupnByModificationId(modifer.getModifer());
        if(quantity >0 ){

            QJsonObject modificationItem;

            auto modification =  modifications.getModificationById(modifer.getModifer());

//            auto product = productList.getById(modification.getProduct());
            if(modificationGroup.getType() == 0){
                modificationItem["name"] = QString(modificationGroup.getName() + " ( " + modification.getName() + " ) ");
                auto price  = quantity * costHash.value(modification.getProduct());
                if(price == 0){
                    modificationItem["cost"] = QString("");

                }else{
                    modificationItem["cost"] = QString("+ " + QString::number( price) + " p." );

                }
                result.append(modificationItem);
            }else{
                modificationItem["name"] = QString(modification.getName());
                auto price  = quantity * costHash.value(modification.getProduct());
                modificationItem["cost"] = QString(QString::number(quantity)  +QLatin1String(" x ") +  QString::number(price)+ QLatin1String(" p."));
                result.append(modificationItem);

            }
        }
    }

    return result;
}

void ProductInOrderListModel::setList(const ProductInOrderList &list)
{
    productInUse = -1;
    m_list = list;
    m_orderCost = 0;
    m_choiceProductHaveModification = false;
    emit dataChanged(index(0,0),index(m_list.count()-1,0));
}
void ProductInOrderListModel::updateList(const ProductInOrderList &list)
{
    m_list = list;
    m_orderCost = 0;
    m_choiceProductHaveModification = false;
    emit dataChanged(index(0,0),index(m_list.count()-1,0));
}



int ProductInOrderListModel::getProductInUse() const
{
    return productInUse;
}

void ProductInOrderListModel::setProductInUse(int value)
{
    productInUse = value;
}

bool ProductInOrderListModel::choiceProductHaveModification() const
{
    return m_choiceProductHaveModification;
}




ProductInOrderList ProductInOrderListModel::getList() const
{
    return m_list;
}

int ProductInOrderListModel::getModificationCost(ModiferList modiferList) const
{
    int cost = 0;
    for(int i = 0; i < modiferList.count(); i++){
        auto modifer = modiferList.at(i);
        auto modification = modifications.getModificationById(modifer.getModifer());
        cost +=costHash.value(modification.getProduct())*modifer.getQuantity();
    }

    return cost;
}





void ProductInOrderListModel::setModificationsToProduct(int index,const ModiferList modifications)
{
    auto product = m_list.at(index);
    product.setModifers(modifications);
    m_list.insert(index,product);
    auto deb = m_list.at(index).getModifers();
    for(int i =0; i < deb.count(); i++){
        qDebug()<<deb.at(i).getQuantity();
    }
//    recalculateOrderCost();
    emit productListChanged(m_list);

    //marker
    emit dataChanged(createIndex(index,0),createIndex(index,0));
}

ProductInOrder ProductInOrderListModel::at(int index) const
{
    return m_list.at(index);
}

ProductInOrderList ProductInOrderListModel::getAll()
{
    return m_list;
}

void ProductInOrderListModel::replace(int index, ProductInOrder *product)
{

    m_list.replace(index,*product);
    emit productListChanged(m_list);

}

void ProductInOrderListModel::processingOffers(OfferList offers)
{
for(int i = 0; i< offers.count(); i++){
    auto offer = offers.at(i);
    qDebug()<< "offers count "<< offers.count();
    processActions(offer.getActions());

}

}

int ProductInOrderListModel::orderCost() const
{
    return m_orderCost;
}


void ProductInOrderListModel::reduceQuantity(int index)
{
    auto prod = m_list.at(index);
    int quantity = prod.getQuantity() - 1;

    if(quantity > 0){
        prod.setQuantity(quantity);
        m_list.replace(index,prod);
    }else{
        m_list.removeAt(index);
        setProductInUse(-1);
        setChoiceProductHaveModification(false);
    }
    emit productListChanged(m_list);

}

void ProductInOrderListModel::updateModification(int index)
{
    emit updateModificationsInProduct(index);

}

void ProductInOrderListModel::setOrderCost(int orderCost)
{
    qDebug()<<"orderCostChange";
    if (m_orderCost == orderCost)
        return;
    m_orderCost = orderCost;
    emit orderCostChanged(m_orderCost);
}

void ProductInOrderListModel::toEdit(int index)
{
    setProductInUse(index);
    auto productItem = m_list.at(index);
    auto product =  productList.getById(productItem.getProduct());
    if(product.getModifications().count() != 0){
        setChoiceProductHaveModification(true);
    }else{
        setChoiceProductHaveModification(false);
    }

    resetModel();
}

void ProductInOrderListModel::setChoiceProductHaveModification(bool choiceProductHaveModification)
{
    if (m_choiceProductHaveModification == choiceProductHaveModification)
        return;

    m_choiceProductHaveModification = choiceProductHaveModification;
    emit choiceProductHaveModificationChanged(m_choiceProductHaveModification);
}

void ProductInOrderListModel::processActions(ActionList actions)
{
    qDebug()<<"process Acrions count >> " <<actions.count();
    for(int i =0; i < actions.count(); i++){
        auto action = actions.at(i);
        qDebug()<<action.getData().getProduct().getProduct();
        if(action.getType() == 1){
            qDebug()<<"добавлен бонусный продукт";
            processBonusProducts(action.getData());
        }
    }
}

void ProductInOrderListModel::processBonusProducts(ActionData data)
{
    qDebug()<<"Process Product in order - > "<<data.getProduct().getProduct();
    auto product = productList.getById(data.getId()).toProductInOrder();
    product.setQuantity(1);
    product.setIsBonusProduct(true);
    beginResetModel();
    m_list.append(product);
    endResetModel();
}


void ProductInOrderListModel::processCartOffer()
{

}

void ProductInOrderListModel::processOfferProducts()
{

}



void ProductInOrderListModel::resetModel()
{
    beginResetModel();
    endResetModel();
}


void ProductInOrderListModel::setProductCostHash(const ProductCostHash &value)
{
    costHash = value;
}

void ProductInOrderListModel::setModifications(const ModificationGroupList &value)
{
    modifications = value;
}

void ProductInOrderListModel::setProductList(const ProductList &value)
{
    productList = value;
}
