#ifndef PRODUCTINORDERMODEL_H
#define PRODUCTINORDERMODEL_H
#include <QStringList>
#include <QAbstractListModel>
#include "entity/Sales/productinorderlist.h"
#include "entity/Menu/produccosthash.h"
#include "../entity/productlist.h"
#include "entity/Menu/modificationgrouphash.h"
#include "../entity/modiferwithnameobjectentity.h"
#include "entity/Marketing/offerlist.h"
class ProductInOrderListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int orderCost READ orderCost WRITE setOrderCost NOTIFY orderCostChanged)
    Q_PROPERTY(bool choiceProductHaveModification READ choiceProductHaveModification WRITE setChoiceProductHaveModification NOTIFY choiceProductHaveModificationChanged)
    Q_PROPERTY(int orderCost READ orderCost WRITE setOrderCost NOTIFY orderCostChanged)

public:
    enum {
        IdRole = Qt::UserRole,
        NameRole,
        PriceRole,
        QuantityRole,        
        ModificationGroupRole,
        ProductWithModificationsRole,
        ProductEditorRole
    };
    explicit ProductInOrderListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void add(ProductInOrder *product);
    void add();
    void remove();
    void setProductList(const ProductList &value);
    void setProductCostHash(const ProductCostHash &value);
    void setModifications(const ModificationGroupList &value);
    void setModificationsToProduct(int index, const ModiferList modifications);
    void setModificationsToLast(ModiferList modifications);
    void setModificationCost(int value);
    ProductInOrder at(int index) const;
    ProductInOrderList getAll();
    void replace(int index, ProductInOrder *product);

    void processingOffers(OfferList offers);


    ProductInOrderList getList() const;
    int getProductInUse() const;
    void setProductInUse(int value);
    bool choiceProductHaveModification() const;
    void updateList(const ProductInOrderList &list);
    void setList(const ProductInOrderList &list);

public slots:

    int orderCost() const;
    void reduceQuantity(int index);
    void updateModification(int index);
    void setOrderCost(int orderCost);
    void toEdit(int index);
    void setChoiceProductHaveModification(bool choiceProductHaveModification);

signals:
    //Подумать на д названием
    void updateModificationsInProduct(int index);
    void orderCostChanged(int orderCost);
    void productListChanged(ProductInOrderList value);
    void choiceProductHaveModificationChanged(bool choiceProductHaveModification);

private:

    void processActions(ActionList actions);
    //Блюдо в подарок
    void processBonusProducts(ActionData data);
    //Скидка/Надбавка на заказ
    void processCartOffer();
    //Предложите гостю
    void processOfferProducts();


    void resetModel();
    int getModificationCost(ModiferList modiferList) const;
    QString getName(QString productId) const;
    int getPrice(QString productId) const;
    QJsonArray getModificationList(ModiferList modifers) const;


    ProductInOrderList m_list;
    ModificationGroupList modifications;
    ProductCostHash costHash;
    ProductList productList;
//    QList<int> modifCostList;
    int productInUse = -1;
    int m_orderCost = 0;
    bool m_choiceProductHaveModification = false;
};

#endif // PRODUCTINORDERMODEL_H
