#include "productlistmodel.h"
#include <QtDebug>
ProductListModel::ProductListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_modificationGroups = new ModificationGrouplistModel();
}

int ProductListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return  m_list.count();

}

QHash<int, QByteArray> ProductListModel::roleNames() const
{
    QHash<int, QByteArray> products = QAbstractListModel::roleNames();
    products[IdRole] = "id";
    products[NameRole] = "name";
    products[CategoryRole] = "category";
    products[PriceRole] = "price";
    products[ModifcationGroupRole] = "modifications";
    return products;

}

QVariant ProductListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto products = m_list.at(index.row());

    switch (role) {
    case IdRole:
        return QVariant(products.getId());
    case NameRole:
        return QVariant(products.getName());
    case CategoryRole:
        return QVariant(products.getCategory());
    case PriceRole:
        return QVariant(products.getPrice());
    case ModifcationGroupRole:
        return QVariant(products.getModifications());
    }

    // FIXME: Implement me!
    return QVariant();
}


void ProductListModel::append(ProductList products)
{

beginResetModel();

        m_list.append(products);

    endResetModel();




}

void ProductListModel::setPrices(const ProductCostHash &value)
{
    prices = value;
}

void ProductListModel::showModifications(QList<QString> modificationIdList)
{
    ModificationGroupList modifers;
    for(int i =0; modificationIdList.count(); i++){
        modifers.append(modifications.getModifcicationGroupById(modificationIdList.at(i)));
    }
    m_modificationGroups->setList(modifers);

}

void ProductListModel::setModificationPanelStatus(bool modificationPanelStatus)
{
    if (m_modificationPanelStatus == modificationPanelStatus)
        return;
    m_modificationPanelStatus = modificationPanelStatus;
    emit modificationPanelStatusChanged(m_modificationPanelStatus);
}

void ProductListModel::addToCart(QString id)
{

    emit onClickedToProduct(id);
}



void ProductListModel::setModifications(const ModificationGroupList &value)
{
    modifications = value;
}

ModificationGrouplistModel *ProductListModel::modificationGroups() const
{
    return m_modificationGroups;
}

bool ProductListModel::modificationPanelStatus() const
{
    return m_modificationPanelStatus;
}

void ProductListModel::setModificationGroups(ModificationGrouplistModel *modificationGroups)
{

    if (m_modificationGroups == modificationGroups)
        return;

    m_modificationGroups = modificationGroups;



    emit modificationGroupsChanged(m_modificationGroups);
}


