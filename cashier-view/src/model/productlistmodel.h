#ifndef PRODUCTLISTMODEL_H
#define PRODUCTLISTMODEL_H

#include <QAbstractListModel>
#include "../entity/productlist.h"
#include "entity/Menu/produccosthash.h"
#include "mordificationgrouplistmodel.h"
class ProductListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(ModificationGrouplistModel * modificationGroups READ modificationGroups WRITE setModificationGroups NOTIFY modificationGroupsChanged)
    Q_PROPERTY(bool modificationPanelStatus READ modificationPanelStatus WRITE setModificationPanelStatus NOTIFY modificationPanelStatusChanged)

    public:
    enum {
        IdRole = Qt::UserRole,
        CategoryRole,
        NameRole,
        PriceRole,
        ModifcationGroupRole};
    explicit ProductListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void append(ProductList products);

    void setPrices(const ProductCostHash &value);



    void setModifications(const ModificationGroupList &value);

    ModificationGrouplistModel * modificationGroups() const;

    bool modificationPanelStatus() const;

public slots:
    void setModificationGroups(ModificationGrouplistModel * modificationGroups);
    void showModifications(QList<QString> modificationIdList);
    void setModificationPanelStatus(bool modificationPanelStatus);
    void addToCart(QString id);
signals:
    void modificationGroupsChanged(ModificationGrouplistModel * modificationGroups);

    void modificationPanelStatusChanged(bool modificationPanelStatus);
    void onClickedToProduct(QString id);
private:
    ProductList m_list;

    ProductCostHash prices;
    ModificationGroupList modifications;



    ModificationGrouplistModel * m_modificationGroups;
    bool m_modificationPanelStatus = false;
};

#endif // PRODUCTLISTMODEL_H
