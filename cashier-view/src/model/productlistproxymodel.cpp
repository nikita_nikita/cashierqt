#include "productlistproxymodel.h"

ProductListProxyModel::ProductListProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

void ProductListProxyModel::setFindName(QString findName)
{

    if (m_findName == findName)
        return;
    if( findName.count() == 0  ){
        setVisible(false);
    }else{
        setVisible(true);
    }
    qDebug()<<"Фильрация по: " << findName;
    m_findName = findName;
    emit findNameChanged(m_findName);
    invalidateFilter();
}

void ProductListProxyModel::setVisible(bool visible)
{
    if (m_visible == visible)
        return;

    m_visible = visible;
    emit visibleChanged(m_visible);

}


bool ProductListProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, filterKeyColumn(), source_parent);
    bool result = sourceModel()->data(index, filterRole()).toString().contains(m_findName,Qt::CaseInsensitive);
    return result;
}

QString ProductListProxyModel::findName() const
{
    return m_findName;
}

bool ProductListProxyModel::visible() const
{
    return m_visible;
}
