#ifndef PRODUCTLISTPROXYMODEL_H
#define PRODUCTLISTPROXYMODEL_H
#include <QSortFilterProxyModel>
#include "productlistmodel.h"
#include <QDebug>

class ProductListProxyModel: public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QString findName READ findName WRITE setFindName NOTIFY findNameChanged)
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)


public:
    explicit ProductListProxyModel(QObject *parent = nullptr);

    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;    


    QString findName() const;

    bool visible() const;

public slots:
    void setFindName(QString findName);
    void setVisible(bool visible);

signals:
    void findNameChanged(QString findName);
    void visibleChanged(bool visible);

private:
    QString m_findName ;
    bool m_visible = false;
};

#endif // PRODUCTLISTPROXYMODEL_H
