#include "saveorderlistpoxymodel.h"

SaveOrderListPoxyModel::SaveOrderListPoxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{

}

bool SaveOrderListPoxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, filterKeyColumn(), source_parent);
    bool result = sourceModel()->data(index, filterRole()).toString() == "status";
    return result;
}

void SaveOrderListPoxyModel::setStatus(const QString &value)
{
    status = value;
    invalidateFilter();
}
