#ifndef SAVEORDERLISTPOXYMODEL_H
#define SAVEORDERLISTPOXYMODEL_H

#include <QSortFilterProxyModel>
#include <QObject>

class SaveOrderListPoxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit SaveOrderListPoxyModel(QObject *parent = nullptr);
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    void setStatus(const QString &value);

private:
    QString status;


};

#endif // SAVEORDERLISTPOXYMODEL_H
