#include "statuslistmodel.h"
#include <QDebug>
StatusListModel::StatusListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int StatusListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_list->count();
}

QHash<int, QByteArray> StatusListModel::roleNames() const
{
    QHash<int, QByteArray> status = QAbstractListModel::roleNames();
    status[NameRole] = "status_name";
    status[CodeRole] = "status_code";
    status[ColorRole] = "status_color";
    return status;
}

QVariant StatusListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()){
        return QVariant();
    }
    auto status = m_list->at(index.row());
    switch (role) {
    case NameRole:
        return  status.getName();
    case CodeRole:
        return  status.getCode();
    case ColorRole:
        return status.getColor();
    default:
        return QVariant("что то пошло не так в  modificationListModel");
    }
}

void StatusListModel::setList(StatusList *list)
{
    qDebug()<<"Количество загруженных статусов - "<< list->count();
    beginResetModel();
    m_list = list;
    endResetModel();

}

int StatusListModel::indexOfStatusCode(QString code)
{
    return  m_list->indexOfCode(code);
}

QString StatusListModel::getStatusValueByName(QString name)
{
    for(int i = 0; i < m_list->count();i++){
        auto status = m_list->at(i);
        if(status.getName() == name){
            return status.getCode();
        }
    }
    return "";
}

StatusList *StatusListModel::getList() const
{
    return m_list;
}
