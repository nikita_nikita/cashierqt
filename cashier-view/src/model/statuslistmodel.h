#ifndef STATUSLISTMODEL_H
#define STATUSLISTMODEL_H

#include <QAbstractListModel>
#include "entity/Settings/statuslist.h"
class StatusListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum{
        NameRole = Qt::UserRole,
        CodeRole,
        ColorRole
    };
    explicit StatusListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setList(StatusList *list);

    int indexOfStatusCode(QString code);
    QString getStatusValueByName(QString name);
    StatusList *getList() const;

private:
    StatusList *m_list;
};

#endif // STATUSLISTMODEL_H
