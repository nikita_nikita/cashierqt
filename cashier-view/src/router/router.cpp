#include "router.h"

Router::Router(QObject *parent) : QObject(parent)
{

}

QString Router::layer() const
{
    return m_layer;
}


void Router::setLayer(QString layer)
{
    if (m_layer == layer)
        return;

    m_layer = layer;
    emit layerChanged(m_layer);
}

void Router::toLoginPage()
{
    emit stackClear();
    setLayer(LOGIN_LAYER);
}

void Router::toMainPage()
{
    emit stackClear();
       //onPage = "PAGE_MAIN";
    setLayer(DEFAULT_LAYER);
}

void Router::toProfile()
{
    emit stackPush(PAGE_PROFILE);
    //onPage = "PAGE_PROFILE";
    setLayer(DEFAULT_LAYER);
}

void Router::toMesages()
{    
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_MESSAGES";
    emit stackPush(PAGE_MESSAGES);
}

void Router::toReports()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_REPORTS";
    emit stackPush(PAGE_REPORTS);
}

void Router::toReturns()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_RETURNS";
    emit stackPush(PAGE_RETURNS);
}

void Router::toGuestList()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_GUESTLIST";
    emit stackPush(PAGE_GUESTLIST);
}

void Router::toDeliveryTipe()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_DELIVERYTIPE";
    emit stackPush(PAGE_DELIVERYTIPE);
}

void Router::toOrderTime()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_ORDERTIME";
    emit stackPush(PAGE_ORDERTIME);
}

void Router::toOrderFindClient()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_ORDERFINDCLIENT";
    emit stackPush(PAGE_ORDERFINDCLIENT);
}

void Router::toOrderFindClientResult()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_ORDERFINDCLIENTRESULT";
    emit stackPush(PAGE_ORDERFINDCLIENTRESULT);
}

void Router::toOrderDeliveryAddress()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_DELIVERYADDRESS";
    emit stackPush(PAGE_DELIVERYADDRESS);
}

void Router::toProductChoice()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_PRODUCTCHOICE";
    emit stackPush(PAGE_PRODUCTCHOICE);
}

void Router::toOrderUpdatePage()
{
    setLayer(DEFAULT_LAYER);
    //onPage = "PAGE_ORDERUPDATE";
    emit stackPush(PAGE_ORDERUPDATE);
}


