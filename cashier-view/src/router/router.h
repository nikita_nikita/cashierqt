#ifndef ROUTER_H
#define ROUTER_H

#include <QObject>

class Router : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString layer READ layer WRITE setLayer NOTIFY layerChanged)
public:
    const QString LOGIN_LAYER = "login";
    const QString DEFAULT_LAYER = "default";
    explicit Router(QObject *parent = nullptr);
    QString layer() const;    



signals:
    void layerChanged(QString layer);
    void stackClear();
    void stackPop();
    void stackPush(QString url);

public slots:
    void setLayer(QString layer);
    void toLoginPage();
    void toMainPage();
    // в профиль
    void toProfile();
    // к сообщениям
    void toMesages();
    // к отчетам
    void toReports();
    //Возврат товара
    void toReturns();
    // список гостей
    void toGuestList();
    //выбор типа заказа (бар, доставка, самовывоз)
    void toDeliveryTipe();    
    //выбор времени заказа (Если время не выбрано, устанавливается как "К ближайшему времени")
    void toOrderTime();
    //Поиск клиента по телефону
    void toOrderFindClient();
    //Результат поиска клиента
    //Маркер - в одну или 2 страницы ? сейчас в одну с прописанным поведением.
    void toOrderFindClientResult();
    //Ввод данных адреса заказа
    void toOrderDeliveryAddress();
    //Страница выбора товаров,блюд, скидок,купоно ...
    void toProductChoice();
    //Страница редактирования заказа
    void toOrderUpdatePage();


private:
    QString m_layer="login";
    const QString PAGE_PROFILE="qrc:/view/pages/Profile.qml";
    const QString PAGE_GUESTLIST="qrc:/view/pages/GuestList.qml";
    const QString PAGE_REPORTS="qrc:/view/pages/Reports.qml";
    const QString PAGE_RETURNS="qrc:/view/pages/Returns.qml";
    const QString PAGE_MESSAGES="qrc:/view/pages/Messages.qml";
    const QString PAGE_DELIVERYTIPE = "qrc:/view/pages/OrderType.qml";
    const QString PAGE_ORDERTIME = "qrc:/view/pages/OrderTime.qml";
    const QString PAGE_DELIVERYADDRESS = "qrc:/view/pages/OrderAddress.qml";
    const QString PAGE_PRODUCTCHOICE = "qrc:/view/pages/OrderProductChoise.qml";
    const QString PAGE_ORDERUPDATE = "qrc:/view/pages/OrderUpdate.qml";
    const QString PAGE_ORDERFINDCLIENT = "qrc:/view/pages/OrderFindClient.qml";
    const QString PAGE_ORDERFINDCLIENTRESULT = "qrc:/view/pages/OrderFindClientResult.qml";


};


#endif // ROUTER_H
