#include "categoryservice.h"

CategoryService::CategoryService(QObject *parent):  ObjectService(parent)
{
    categoryManager = new CategoryManager(parent);
    connect(categoryManager, &CategoryManager::loaded,this,&CategoryService::onCategoryListLoadSuccess);
}

void CategoryService::getCategoryList()
{
    categoryManager->list();
}

void CategoryService::setToken(QString token)
{
    ObjectService::setToken(token);
}

void CategoryService::onCategoryListLoadSuccess(CategoryList list)
{
    emit loadSuccess(list);
}

void CategoryService::onError(QString message)
{
ObjectService::error(message);
}


