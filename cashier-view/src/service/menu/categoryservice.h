#ifndef CATEGORYSERVICE_H
#define CATEGORYSERVICE_H
#include "../objectservice.h"
#include "menu/categorymanager.h"

class CategoryService: public ObjectService
{
    Q_OBJECT
public:
    CategoryService(QObject *parent = nullptr);
    void getCategoryList();
    void setToken(QString token);

signals:
    void loadSuccess(CategoryList list);
public slots:
    void onCategoryListLoadSuccess(CategoryList list);
    void onError(QString message);
private:
    CategoryManager *categoryManager;
};

#endif // CATEGORYSERVICE_H
