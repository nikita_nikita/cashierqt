#include "objectservice.h"
#include "QDebug"
ObjectService::ObjectService(QObject *parent) : QObject(parent)
{

}

void ObjectService::setConfig(Config config)
{
    this->config = config;
}

void ObjectService::setToken(QString &token)
{
    this->token = token;
}

QString ObjectService::getToken()
{
    return this->token;
}

QString ObjectService::getPoint()
{
    return this->config.getPointID();
}


