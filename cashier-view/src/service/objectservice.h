#ifndef OBJECTSERVICE_H
#define OBJECTSERVICE_H

#include <QObject>
#include "../config/config.h"
class ObjectService : public QObject
{
    Q_OBJECT
public:
    explicit ObjectService(QObject *parent = nullptr);
    

    virtual void setToken(QString &token);
    virtual QString getToken();
    QString getPoint();

    
signals:
    void error(QString message);
    void info(QString message);
public slots:
void setConfig(Config config);
private:
    Config config;
    QString token;
};

#endif // OBJECTSERVICE_H
