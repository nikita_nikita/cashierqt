#include "servicelocator.h"
#include <QDebug>
ServiceLocator::ServiceLocator(QObject *parent) : ObjectService(parent)
{
    shiftService = new ShiftService(this);
    storageService = new StorageService(this);
    this->registerService(shiftService);
    this->registerService(storageService);


}

ShiftService *ServiceLocator::getShiftService() const
{
    return  shiftService;
}

StorageService *ServiceLocator::getStorageService() const
{
    return  storageService;
}



void ServiceLocator::setConfig(Config config)
{

    for (int i = 0;i<services.length();i++)
    {
        qDebug()<<"serviceLocator setConfig";
        services.at(i)->setConfig(config);
    }

}



void ServiceLocator::updateToken(QString &token)
{
    ObjectService::setToken(token);
}





void ServiceLocator::registerService(ObjectService *service)
{
    connect(shiftService,&ShiftService::tokenChanged,service,&ObjectService::setToken);

    this->services.append(service);

}

