#ifndef SERVICELOCATOR_H
#define SERVICELOCATOR_H
#include "clientlibqt.h"
#include "objectservice.h"

#include "menu/categoryservice.h"
#include "staff/shiftservice.h"
#include "staff/userservice.h"
#include "storageservice.h"

#include "src/config/config.h"
//#include "sales/orderservice.h"
class ServiceLocator:public ObjectService
{
    Q_OBJECT
public:
    explicit ServiceLocator(QObject *parent = nullptr);
    ShiftService *getShiftService() const;
    StorageService *getStorageService() const;




signals:
    void error(QString message);
    void shiftIsOpenChange(bool shiftIsOpen);
public slots:
    void setConfig(Config config);
    void updateToken(QString &token);
private:    
    ShiftService *shiftService;
    StorageService *storageService;
    QList<ObjectService *> services;
    void registerService(ObjectService *service);

};

#endif // SERVICELOCATOR_H
