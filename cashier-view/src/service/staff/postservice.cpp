#include "postservice.h"

PostService::PostService(QObject *parent):  ObjectService(parent)
{
    postmanager = new PostManager();
    connect(postmanager,&PostManager::getList,this,&PostService::onLoadSuccess);
}

void PostService::setToken(QString token)
{
    ObjectService::setToken(token);
}

void PostService::load()
{

}

void PostService::onLoadSuccess(PostList posts)
{
Q_UNUSED(posts)
}
