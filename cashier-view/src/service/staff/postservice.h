#ifndef POSTSERVICE_H
#define POSTSERVICE_H


#include "../objectservice.h"
#include "staff/postmanager.h"
class PostService: public ObjectService
{
    Q_OBJECT
public:
    PostService(QObject *parent = nullptr);
     void setToken(QString token);
     void load();
signals:
     void loaded(PostList posts);

private:
    PostManager *postmanager;
private slots:
    void onLoadSuccess(PostList posts);
};

#endif // POSTSERVICE_H
