﻿#include "shiftservice.h"

ShiftService::ShiftService(QObject *parent):  ObjectService(parent)
{
    shiftManager = new ShiftManager(parent);

    connect(shiftManager,&ShiftManager::opened,this,&ShiftService::updateToken);
    connect(shiftManager,&ShiftManager::openError,this,&ShiftService::onError);
    connect(shiftManager,&ShiftManager::closeError,this,&ShiftService::onError);
}

void ShiftService::Open(QString login, QString passwd)
{

    shiftManager->open(login,passwd);
}

void ShiftService::Open(QString pin)
{

    shiftManager->openByPin(pin);
}

void ShiftService::Close(QString token)
{
    shiftManager->close(token);
}

void ShiftService::updateToken(QString token)
{
    qDebug()<<"ShiftService::updateToken";
    emit tokenChanged(token);

}

void ShiftService::onError(int code,QString message)
{
emit error( QString(code) + "  - "+ message);
}


QDateTime ShiftService::getOpenTime() const
{
    return openTime;
}

void ShiftService::setOpenTime(const QDateTime &value)
{
    openTime = value;
}


