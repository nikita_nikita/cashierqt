#ifndef SHIFTSERVICE_H
#define SHIFTSERVICE_H
#include "../objectservice.h"
#include "staff/shiftmanager.h"
#include <QTimer>
class ShiftService:public ObjectService
{
    Q_OBJECT
public:
    ShiftService(QObject *parent = nullptr);
    void Open(QString login, QString passwd);
    void Open(QString pin);
    void Close(QString token);



    QDateTime getOpenTime() const;
    void setOpenTime(const QDateTime &value);


signals:
    void tokenChanged(QString &token);
    void shiftIsClosed();
    void warning(QString message);
    void error(QString message);
    void info(QString message);
public slots:
    void updateToken(QString token);
    void onError(int code, QString message);

private:
    QTimer *shiftTimer;
    QDateTime openTime;
    bool shiftStatus;
    ShiftManager *shiftManager;

};

#endif // SHIFTSERVICE_H
