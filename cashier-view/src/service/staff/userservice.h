#ifndef USERSERVICE_H
#define USERSERVICE_H
#include "staff/authmanager.h"
#include "../objectservice.h"
class UserService:public ObjectService
{
    Q_OBJECT
public:
    UserService(QObject *parent = nullptr);
    void getUserData(QString token);
    void setToken(QString token);
private:

    AuthManager *authmanager;
};

#endif // USERSERVICE_H
