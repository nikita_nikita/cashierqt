#include "storageservice.h"
#include <QList>
StorageService::StorageService(QObject *parent):  ObjectService(parent)
{
    categoryManager = new CategoryManager(this);
    goodManager = new GoodManager(this);
    modifireManager = new ModificationManager(this);
    dishManager = new DishManager(this);
    productCostmanager = new ProductCostManager(this);
    prepackManager = new PrepackManager(this);
    statusManager = new StatusManager(this);



    connect(categoryManager,&CategoryManager::loaded,this, &StorageService::onLoadCategoriesSuccess);
    connect(goodManager,&GoodManager::loaded,this,&StorageService::onLoadGoodSuccess);
    connect(modifireManager,&ModificationManager::loaded,this,&StorageService::onLoadModificationsSuccess);
    connect(dishManager,&DishManager::loaded,this,&StorageService::onLoadDishSuccess);
    connect(productCostmanager,&ProductCostManager::loaded, this, &StorageService::onLoadPriceListSuccess);
    connect(statusManager,&StatusManager::loaded, this, &StorageService::onLoadStatusSucess);
//    connect(prepackManager,&PrepackManager::loaded, this, &StorageService::onLoadPrepackSuccess);
}


CategoryList StorageService::getCategories() const
{
    return categories;
}

void StorageService::setCategories(const CategoryList &value)
{
    categories = value;
}

void StorageService::setConfig(Config config)
{

    ObjectService::setConfig(config);
}

void StorageService::onLoadDishSuccess(DishList dishes)
{
    qDebug()<<"Количество блюд - "<<dishes.count();
    for(int i = 0; i < dishes.count();i++){
        auto dish = dishes.at(i);
        this->products.append(dish);
        this->productPoint.append(ProductPoint(dish.getId(),getPoint()));

    }
    productCostmanager->list(this->productPoint.toJson());
}

void StorageService::onLoadModificationsSuccess(ModificationGroupList modificationGroups)
{
    modificationGroupList = modificationGroups;
    auto idList = modificationGroupList.getAllmodificationsId();
    for(int i = 0; i < idList.count();i++){
        this->productPoint.append(ProductPoint(idList.at(i),getPoint()));
    }
    dishManager->list();
}



void StorageService::onLoadGoodSuccess(GoodsList goods)
{

    for(int i = 0; i < goods.count();i++){
        auto good = goods.at(i);
        this->products.append(good);
        this->productPoint.append(ProductPoint(good.getId(),getPoint()));
    }

qDebug()<<"Количество товаров - "<<goods.count();

    modifireManager->list();

}

void StorageService::onLoadCategoriesSuccess(CategoryList categories)
{
    categories.append(categories);
    goodManager->list();
}

void StorageService::onLoadPriceListSuccess(ProductCostHash productCostHash)
{    
    emit productListChange(products,productCostHash,modificationGroupList);
}

void StorageService::onLoadPrepackSuccess(PrepackList prepack)
{
    for (int i =0;i < prepack.count(); i++){
    this->productPoint.append(ProductPoint(prepack.at(i).getId(),getPoint()));
    }
}

void StorageService::onLoadStatusSucess(StatusList statusList)
{
    emit statusListChange(statusList);
}




void StorageService::setToken(QString &token)
{
    ObjectService::setToken(token);

    categoryManager->setToken(token);
    goodManager->setToken(token);
    modifireManager->setToken(token);
    dishManager->setToken(token);
    productCostmanager->setToken(token);

    categoryManager->list();
    statusManager->setToken(token);
    statusManager->list();

}

