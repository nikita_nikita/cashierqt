#ifndef STORAGE_H
#define STORAGE_H
#include "objectservice.h"
#include "../entity/productlist.h"
#include "menu/dishmanager.h"
#include "menu/goodmanager.h"
#include "menu/categorymanager.h"
#include "menu/modificationmanager.h"
#include "menu/productcostmanager.h"
#include "sales/ordermanager.h"
#include "menu/prepackmanager.h"
#include "settings/statusmanager.h"
#include "settings/settingmanager.h"
#include "entity/Menu/productpintlist.h"
#include "entity/Sales/paymentlist.h"


class StorageService : public ObjectService
{
    Q_OBJECT
public:
    enum TYPE{
    DISH = 0,
    MODIFICATIONS = 1,
    GOOD = 2

};
    StorageService(QObject *parent = nullptr);

    ProductInOrderList getProductsInOrder() const;
    void setProductsInOrder(const ProductInOrderList &value);

    CategoryList getCategories() const;
    void setCategories(const CategoryList &value);


    QHash<QString, int> getProductCostList() const;
    void setProductCostList(const QHash<QString, int> &value);


signals:
    void productListChange(ProductList products, ProductCostHash productCost, ModificationGroupList modificationGrouplist);
    void statusListChange(StatusList statuses);
    void pointSettingChanged(Setting setting);
public slots:
    virtual void setConfig(Config config);
    virtual void setToken(QString &token);
    void onLoadDishSuccess(DishList dishes);
    void onLoadModificationsSuccess(ModificationGroupList modificationGroups);
    void onLoadGoodSuccess(GoodsList goods);
    void onLoadCategoriesSuccess(CategoryList categories);
    void onLoadPriceListSuccess(ProductCostHash productCostHash);
    void onLoadPrepackSuccess(PrepackList prepack);
    void onLoadStatusSucess(StatusList statusList);

private:
    PrepackManager *prepackManager;
    GoodManager *goodManager;
    DishManager *dishManager;
    CategoryManager *categoryManager;
    ModificationManager *modifireManager;
    ProductCostManager *productCostmanager;
    StatusManager *statusManager;
    ModificationGroupList modificationGroupList;
    ProductList products;
    CategoryList categories;
    StatusList statuses;
    ProductPointList productPoint;







};

#endif // STORAGE_H
