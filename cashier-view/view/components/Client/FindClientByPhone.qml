import QtQuick 2.0

Rectangle{
    signal loadClientList
    border.color: "black"
    id:root_component
    Column{
        width: root_component.width
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            width: root_component.width
            height: 60
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 22
            text: "Введите мобильный телефон клиента"
        }
        Rectangle{
            anchors.horizontalCenter: parent.horizontalCenter
            color: "red"
            width: root_component.width/2
//            anchors.left: parent.left
//            anchors.right: parent.right
//            anchors.leftMargin: root_component.width/4
//            anchors.rightMargin: root_component.width/4
            height: 50
            border.color: "black"
            TextInput{
                id:input
                anchors.fill: parent
                width: root_component.width
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                onContentSizeChanged:{
                    clientController.number = input.text
                    console.log(clientController.number)
                    loadClientList()

                }
                height: 50
                inputMethodHints: Qt.ImhDigitsOnly
                maximumLength:10
                font.pointSize: 22

                onFocusChanged: {
                    if(focus){
                    inputMask = "8 (###) ### ## ##"
                    }
                }
            }
        }

    }

}
