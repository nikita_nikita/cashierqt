import QtQuick 2.0

Rectangle{
    height: 40
    id:root_component
    signal clientChoice()
    property string  name: ""
    property string phone: ""
    border.color: "silver"

    Row {

        Text {
            width: 200
            height: 40
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: root_component.name
            font.pointSize:20
            color: "red"
        }
        Text {
            width: 200
            height: 40
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: root_component.phone
            font.pointSize:20
            color: "red"
        }
        Rectangle{
            width:  200
            height: 40
            color: "orange"
            border.color: "gray"
            border.width: 4
            Text {
                anchors.centerIn: parent
                font.pointSize:20
                text: "Выбрать"  }
            MouseArea{
                anchors.fill: parent
                onClicked: clientChoice()}
        }


    }
}
