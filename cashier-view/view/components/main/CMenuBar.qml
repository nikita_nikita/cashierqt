import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import QtQuick.Controls.Material 2.12
ToolBar {
    id: root_component
    signal sandwichClicked();

    background: Rectangle {
        color: "white"
    }
    Material.foreground: "black"
    Material.background: "white"
    RowLayout {
        anchors.fill: parent
        ToolButton {
            icon.source: "qrc:/images/view/images/icons/drawer"
            onClicked:  root_component.sandwichClicked()
        }
        TabBar {
            id: bar
            clip: true
            height: parent.height
            spacing: 0
            padding: 0
            Layout.fillHeight: true


            //   width: childrenRect.width
            TabButton {
                id:allButton
                height: parent.height+8
                width: 150
                text: "Все"
                background: Rectangle{
                    color: allButton.focus ? "#dfe384" : "white"
                }
                onClicked: orderController.filterAllButtonClicked()
            }
            TabButton {
                height: parent.height+10
                id:deliveryButton
                width: 150
                text: "Доставка"
                background: Rectangle{
                    color: deliveryButton.focus ? "#dfe384" : "white"
                }
                 onClicked: orderController.filterDeliveryButtonClicked()
            }

            TabButton {
                height: parent.height+10
                id:pickupButton
                width: 150
                text: "Самовывоз"

                background: Rectangle{
                    color: pickupButton.focus ? "#dfe384" : "white"
                }
                 onClicked: orderController.filterPickUpButtonClicked()
            }
        }

        NitificationPanel{}
    }

}
