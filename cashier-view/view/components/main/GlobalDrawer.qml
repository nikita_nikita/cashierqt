import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
Drawer{
    id:root_component
    //interactive: false
    ColumnLayout{
        anchors.fill: parent
        Profile{
            id:profileElement
            Layout.fillWidth: true
            onClicked: {
                 root_component.close()
                router.toProfile()
            }
        }
        Column {
             Layout.fillWidth: true
             Layout.fillHeight: true
             NavItem{
                 text: "Сообщения"
                 onClicked: {
                     root_component.close()
                     router.toMesages()
                 }
             }
             NavItem{
                 text: "Список гостей"
                 onClicked: {
                     root_component.close()
                     router.toGuestList()
                 }
             }
             NavItem{
                 text: "Отчеты"
                 onClicked: {
                     root_component.close()
                     router.toReports()
                 }
             }
             NavItem{
                 text: "Возврат товара"
                 onClicked: {
                     root_component.close()
                     router.toReturns()
                 }
             }
        }

    }

}
