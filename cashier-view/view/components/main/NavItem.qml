import QtQuick 2.0

Rectangle {
    id: root_component
    signal  clicked()
    color: focus?"#5d5d5d":"#3d3d3d"
    height: 40
    width: 400

    property alias text: text_component.text
    Text{
        id: text_component
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 20
        color: "#fff"
    }


    Rectangle{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 1
        color: "#f5f5f5"
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: root_component.clicked()
        onEntered: root_component.focus=true
        onExited: root_component.focus=false
    }
}
