import QtQuick 2.0

Rectangle {
    id:root_component
    color: "#deff82"
    height: 100
    signal clicked()
    Image {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 20
        anchors.leftMargin: 20
        width:  60
        height: 60
        source: "qrc:/images/view/images/sasha.png"
    }
    Text {
        x:120
        y:45
        id: name
        text: "Иванов Иван Иванович"
    }
    MouseArea{
        anchors.fill: parent
       onClicked:root_component.clicked()
    }
}
