import QtQuick 2.0

Item {
    property alias inputText: input.text
    signal searchInputTextChange();

    width: 600
    height: 40
        TextInput{
            text: orderController.nameVisibleProduct.findName
            id:input
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignLeft
            anchors.fill: parent
            font.pointSize: 30
            onTextChanged: searchInputTextChange()


        }

}
