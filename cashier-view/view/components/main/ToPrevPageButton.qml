import QtQuick 2.0
import QtQuick.Controls 2.12

Button{
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    background:
        Rectangle{
        anchors.fill: parent
        color: "#33FF66"
    }
    width: 120
    height: 60
    text: "Назад"
    onClicked: router.stackPop()
}
