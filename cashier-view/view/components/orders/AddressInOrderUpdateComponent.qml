import QtQuick 2.0

Rectangle {
    property string street: ""
    property string corpus: ""
    property string enterance: ""
    property string floor: ""
    property string house: ""
    property string room: ""
    property string comment: ""
    id:root_component
    color: "white"
    Column{
        Row{
            height: 50
            Text {
                width: root_component.width-100
                height: 50
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Информация об адресе"
                font.pointSize: 15
            }
            Rectangle{
                width: 100
                height: 50
                color: "#fe624c"
                Text {
                    color: "white"
                    anchors.centerIn: parent
                    font.pointSize: 15
                    text: "Ред-ть"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: router.toOrderDeliveryAddress()
                }
            }

        }
        Column{
            AddressUpdateComponent{
                leftText: "Улица"
                rightText: root_component.street
            }
            AddressUpdateComponent{
                leftText: "Дом"
                rightText: root_component.house
            }
            AddressUpdateComponent{
                leftText: "Корпус"
                rightText: root_component.corpus
            }
            AddressUpdateComponent{
                leftText: "Подъезд"
                rightText: root_component.enterance
            }
            AddressUpdateComponent{
                leftText: "Этаж"
                rightText: root_component.floor
            }
            AddressUpdateComponent{
                leftText: "Кв-ра"
                rightText: root_component.room
            }
            AddressUpdateComponent{
                leftText: "Курьер"
                rightText: "В разработке"
            }
            AddressUpdateComponent{
                leftText: "Коментарий"
                rightText: root_component.comment
            }
        }


    }
}
