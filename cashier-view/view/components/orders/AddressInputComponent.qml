import QtQuick 2.0

Row {
    property string headertext
    property alias inputData: input.text
    property int inputType: 0
    IntValidator{
    id:digitValid
    bottom: 0
    top:3000
    }
    Component.onCompleted: {
    if(inputType === 1){
    input.validator = digitValid

    }
    }
    height: 35
    Text {
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        height: parent.height
        width: 150
        text: headertext
        font.pointSize: 15
        color: "white"
    }
    Rectangle{
        height: parent.height
        width: 200
        color: "white"
    TextInput{
        inputMethodHints:inputType
        height: parent.height
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 15
        id:input
        anchors.fill: parent

    }
    }

}
