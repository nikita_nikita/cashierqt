import QtQuick 2.0

Column {
    property alias streetText:street.inputData
    property alias buildingText:building.inputData
    property alias entranceText:entrance.inputData
    property alias floorText:floor.inputData
    property alias roomText:room.inputData
    property alias commentText:comment.inputData

    spacing: 20
    AddressInputComponent{
        id:street
        headertext:"Улица"
    }
    AddressInputComponent{
        id:building
        headertext:"Дом"
    }
//    AddressInputComponent{

//        text:"Корпус"
//    }
    AddressInputComponent{
        id:entrance
        inputType: 1
        headertext:"Подъезд"
    }
//    AddressInputComponent{
//        id:room
//        text:"Домофон"
//    }
    AddressInputComponent{
        id:floor
        inputType: 1
        headertext:"Этаж"
    }
    AddressInputComponent{
        id:room
        inputType: 1
        headertext:"Квартира"}
    AddressInputComponent{
        id:comment
        headertext:"Коментарий"
    }

}
