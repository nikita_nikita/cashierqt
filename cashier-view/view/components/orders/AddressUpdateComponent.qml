import QtQuick 2.0

Row {
    id:root_component
    property string leftText: ""
    property string rightText: ""
    Rectangle{
        height: 50
        width: 250
        color: "white"
        Text {
            anchors.centerIn: parent
            text: root_component.leftText
        }
    }
    Rectangle{

        height: 50
        width: 150
        color: "white"
        Text {
            anchors.centerIn: parent

            text: root_component.rightText
        }
    }

}
