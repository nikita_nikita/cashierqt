import QtQuick 2.0

Rectangle{
    property int orderPrice: 0
    property int orderPayd: 0
    color: "red"
    Grid{
        columns: 3
        spacing: 0
        BottomBarComponent{name:"Купон"}
        BottomBarComponent{name:"Скидка"}
        BottomBarComponent{name:"Сохранить и<br>вернуться позже"
            onClick: orderController.payButtonClicked()
        }
        BottomBarComponent{name:"Копон не<br>применён"}
        BottomBarComponent{name:"Подитог : " + orderController.orderObject.amount + " p. <br>Оплачено : " + orderPayd + " p."}
        BottomBarComponent{name:"Оплатить"
            onClick: orderController.showPaymentTypePanel()

        }
    }
}
