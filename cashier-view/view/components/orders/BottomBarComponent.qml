import QtQuick 2.0

Rectangle{
    property string name
    color: "#CCFF66"
    signal click()
    border.color: "black"
width: 200
height: 40
    Text {

        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: "Helvetica"
        font.pointSize: 12
        anchors.fill: parent
        font.weight: Font.DemiBold
        text: name
    }
    MouseArea{
    anchors.fill: parent
    onClicked: clicked()
    }
}
