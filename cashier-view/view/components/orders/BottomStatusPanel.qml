import QtQuick 2.0

Rectangle{
    id:root_component
    color: "purple"
    property alias statusModel: repeater.model
    Grid{
        columns: 5
        anchors.centerIn: parent
        Repeater{
            id:repeater
            Rectangle{
            width: 120
            height: 75
            border.color: "purple"
            border.width: 4
            Text {
                anchors.centerIn: parent
                text: status_name
            }
            }
        }

    }

}
