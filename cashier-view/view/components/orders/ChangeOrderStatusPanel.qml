import QtQuick 2.0

Rectangle {
    id:root_component
    anchors.fill: parent
    color: "silver"
    property alias statusModel: repeater.model
    signal close
    signal changeStatus()
    MouseArea{
        anchors.fill: parent
        onClicked: close()
    }
    Grid{
        columns: 2
        spacing: 30
        anchors.centerIn: parent
        Repeater{
            id: repeater
            ChengeOrderStatusComponent{
                statusColor: status_color
                statusName: status_name
                onSelectStatus:{
                    orderController.orderObject.status = status_code
                    close()
                }
            }
        }
    }

}
