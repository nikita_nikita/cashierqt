import QtQuick 2.0

Rectangle{
    id:root_component
    signal selectStatus
    property string statusName: ""
    property string statusColor: ""
    color:statusColor

    width: 150
    height: 75
    Text {
        anchors.centerIn: parent
        font.pointSize: 10
        color: "white"
        id: name
        text: root_component.statusName
    }
    MouseArea{
        anchors.fill: parent
    onClicked: selectStatus()
    }

}
