import QtQuick 2.0

Rectangle{

    width: 200
    height: 50
    signal choice()
    property int deliveryType: 0
    Component.onCompleted: {
        switch (deliveryType){
        case 0:
            text.text = "Самовывыоз"
            this.color = "#8172b5"
            break ;
        case 1:
            text.text = "Доставка"
            this.color = "#fc9a08"
            break ;
        case 2:
            text.text = "В баре"
            this.color = "#b81cb8"
            break ;
        default:
            break ;
        }
    }
    Text {
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        id: text
        font.family: "Helvetica"
    }
    MouseArea{
    anchors.fill: parent
    onClicked:{
        choice()
        router.toOrderTime()
}
}
}
