import QtQuick 2.12

Rectangle {
    color:"#3f4140"
    height: 60
    Row {
        FilterStatusItem{
            name: "Неподтвержденные"
            amount: 1
            active: true
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Новые"
            amount: 2
            active: true
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Готовится"
            amount: 0
            active: true
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Готовы"
            amount: 0
            active: true
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "В Пути"
            amount: 0

            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Закрытые"
            amount: 0
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Неоплаченные"
            amount: 0
            onClicked: active=!active
        }
        FilterStatusItem{
            name: "Сохранённые"
            amount: 0
            onClicked: active=!active
        }
    }


}
