import QtQuick 2.0

Rectangle {
    id: root_component
    property bool active: false
    property string name: ""
    property int amount: 0
    signal clicked()
    height: 60
    width: text_component.contentWidth+20
    color: active?"#dfe384":"#3f4140"
    Text {
        id: text_component
        anchors.centerIn: parent
        text: root_component.name+":"+root_component.amount
    }
    MouseArea {
        anchors.fill: parent
        onClicked: root_component.clicked()
    }
}
