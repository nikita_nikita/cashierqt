import QtQuick 2.0

Item {
    height: 40
    property alias text: text_component.text
    width: text_component.width+20
    Text {
        id: text_component
        anchors.centerIn: parent
        color: "#fff"
    }
}
