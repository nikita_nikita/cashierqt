import QtQuick 2.0

Rectangle{
    anchors.top: parent.top
    anchors.topMargin: 2
    property string editorText
    property int fontSize
    property bool displeEditMarker: true
    width: parent.width/4-2
    height: parent.height-4
    Text {
        anchors.centerIn: parent
        font.pointSize: fontSize
        text:editorText
        color: displeEditMarker ? "black" : "red"
    }
}
