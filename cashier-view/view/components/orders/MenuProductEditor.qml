import QtQuick 2.13

Rectangle {
border.color: "#333333"
border.width: 2
    Row{
        anchors.fill: parent
        leftPadding: 2
MenuEditorElement{
editorText: "+"
fontSize: 40
MouseArea{
anchors.fill: parent
onClicked:orderController.productAddQuantity()
}
}
MenuEditorElement{
editorText: "-"
fontSize:40
MouseArea{
anchors.fill: parent
onClicked:orderController.productReduceQuantity()
}
}
MenuEditorElement{
editorText: "Уд-ть"
fontSize:15
MouseArea{
anchors.fill: parent
onClicked:orderController.productRemove()

}
}
MenuEditorElement{
editorText: "Ред-ть"
fontSize:15
displeEditMarker: orderController.orderProductListModel.choiceProductHaveModification
MouseArea{
anchors.fill: parent
onClicked:
    if(orderController.orderProductListModel.choiceProductHaveModification){
    orderController.productUpdateModifications()
    }
}
}

    }

}
