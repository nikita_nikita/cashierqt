import QtQuick 2.0

Rectangle {
    id: root_component
    property  int  qantity: 0
    property string name: ""
    property bool isSingle: false
    signal added()
    signal removed()

    /*Component.onCompleted: {
        if(modificationGroupType === 0){
            this.height =  80
            minusElement.visible = false
            quantityElement.visible = false
            this.color = "#CCFFCC"
        }
    }*/
    radius: 20
    width: 140
    height: isSingle ? 80:140
    border.color: root_component.isSingle && root_component.qantity === 1 ? "red" : "blue"
    border.width: 5
    color: isSingle?"#CCFFCC":"white"
    Text {
        id:quantityElement
        anchors.fill: parent
        anchors.bottomMargin:  parent.height/7
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 45
        text: root_component.qantity
        visible: !root_component.isSingle


    }
    Text {
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        verticalAlignment: Text.AlignBottom
        text: root_component.name
        font.pointSize: root_component.isSingle ? 20 :12
    }

    Rectangle{
        property int size: 30
        visible: root_component.qantity && !root_component.isSingle > 0  ? true : false
        id:minusElement
        x:parent.width-size/2
        y:-size/2

        width: size
        height: size
        radius: size/2
        border.color: "red"
        Text {
            font.pointSize: 30
            anchors.centerIn: parent
            text: "-"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {

               root_component.removed()
            }
        }
    }
    MouseArea {
        id:area
        property int modType
        anchors.fill: parent
        onClicked: {
            root_component.added()
        }

    }
}
