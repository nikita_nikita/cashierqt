import QtQuick 2.0

Rectangle{
    property alias header: modHeader.text
    width: parent.width
    height: 50
    color: "#99FFFF"
    Text {
       id:modHeader
       anchors.fill: parent
       horizontalAlignment: Text.AlignHCenter
       verticalAlignment: Text.AlignVCenter
       font.pointSize: 24

    }
}
