import QtQuick 2.0

Rectangle {
    property alias modoficationGridModel: modificationList.model

    width: 600
    height: childrenRect.height
    Grid{
        id:modification_grid
        spacing: 20
        columns: 4
        Repeater{
            id:modificationList
            ModificationComponent{
            name:mod_name
            qantity: mod_quantity
            isSingle:root_component.isSingle

            }

        }


    }
}
