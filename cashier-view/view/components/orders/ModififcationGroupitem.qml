import QtQuick 2.0

Column{

    property alias model: repeater.model
    spacing: 20

    //        Rectangle{
    //            width: parent.width
    //            height: 400
    //            border.color: "#000"
    //            border.width: 2
    Repeater{
        id: repeater

        Column{

            Rectangle{
                width: 600
                height: 50

                Text {
                    anchors.fill: parent
                    text: modificationGroupName
                }
            }
            Rectangle {

                width: 600
                height: childrenRect.height
                Grid{
                    id:modification_grid
                    spacing: 20
                    columns: 4
                    Repeater{

                        model: modificationList
                        ModificationComponent{
                        name:mod_name
                        qantity: mod_quantity
                        isSingle:modificationGroupType==0
                        onAdded: {
                            if (isSingle) {
                                mod_quantity =  1
                            } else {
                                mod_quantity = mod_quantity + 1
                            }


                        }
                        onRemoved: {
                            if (isSingle) {
                                mod_quantity =  0
                            } else {
                                mod_quantity = mod_quantity - 1
                            }


                        }

                        }

                    }


                }
            }

        }
    }
}


