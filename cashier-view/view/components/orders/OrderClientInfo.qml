import QtQuick 2.13

Rectangle {    
    id:root_component
    property string clientPhone:""
    property string clientName: ""
    property string clientBirthDate: ""
    property string clientComment: ""
    signal showStatusPanel
    Column{
        Row{
            height: 50

            Text {
                width: root_component.width-100
                height: 50
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Информация о кленте"
                font.pointSize: 15
            }
            Rectangle{
                width: 100
                height: 50
                color: "#fe624c"
                
                Text {
                    color: "white"
                    anchors.centerIn: parent
                    font.pointSize: 15
                    text: "Ред-ть"
                }
                MouseArea{
                    anchors.fill: parent
                    //            onClicked:

                }
            }

        }

        Column{

            leftPadding: 45
            spacing: 40
            Text {
                font.pointSize: 15
                width:root_component.width
                text: "Имя:   " + root_component.clientName
            }
            Text {
                font.pointSize: 15
                width:root_component.width
                text: "Телефон:   " + root_component.clientPhone
            }
            Text {
                font.pointSize: 15
                width:root_component.width
                text: "День рождения:   " + root_component.clientBirthDate
            }
            Text {
                font.pointSize: 15
                width:root_component.width
                text: "Коментарий к клиенту:   " + root_component.clientComment
            }
        }

        Column{
            spacing: 50
            leftPadding: 50
            Rectangle{
                color: "#99FF99"
                width: root_component.width-100
                height: 100
                Text {
                    font.pointSize: 20
                    anchors.centerIn: parent
                    text: if(orderController.orderObject.delivery === 0){
                          "Тип: Самовывоз"
                          }
                    else if(orderController.orderObject.delivery === 1){
                        "Тип: доставка"
                        }else if(orderController.orderObject.delivery === 2){
                              "Тип: В Баре"
                              }
                }
            }
            Rectangle{

                color: "#6666CC"
                width: root_component.width-100
                height: 100
                Text {
                    font.pointSize: 25
                    anchors.centerIn: parent
                    text: "Статус : " + orderController.orderObject.status
                }
                MouseArea{
                anchors.fill: parent
                onClicked: showStatusPanel()}
            }
        }
    }
}
