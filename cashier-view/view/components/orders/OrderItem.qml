import QtQuick 2.0

Item {
    id: root_compent
    height: 70
    width: toprow_component.width
    signal updateOrder();
    property string order_number: "39798"
    property string order_status:"Готов"
    property string order_time_delivery: "11:48"
    property var order_products: "Минисет 1, Банито"
    property string order_address: "г.Смоленск, ул Киевское шоссе, д 58, кв 83,под.1, эт.8"
    property string order_comment: "подписать факап"
    property string order_summa: "376"
    property string order_client_info: "Аноним"
    property bool order_payd_status: false
    property double order_payments: 0.0
    property string order_client_name: ""
    MouseArea{
    anchors.fill: parent
    onClicked: updateOrder()}
    Column{

        Row {
            height: 50
            id: toprow_component
            Rectangle{
                border.color: "red"
                width: 100
                height: 50

                Rectangle{
                    width: parent.width
                    height: 50
                    color: "#fe624c"
                    Text {
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                        text: root_compent.order_number
                        font.pointSize: 20

                    }}

            }

            Rectangle{
                border.color: "red"
                width: 80
                height: 50
                Text {
                    text: root_compent.order_time_delivery
                }}
            Rectangle{
                border.color: "red"
                width: 200
                height: 50
                Text {
                    text: root_compent.order_address
                }}
            Rectangle{
                border.color: "red"
                width: 180
                height: 50
                Text {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 15
                    text:root_compent.order_client_name + " <br> "+  root_compent.order_client_info
                }}
            Rectangle{
                border.color: "red"
                width: 180
                height: 50
                Text {
                    text: root_compent.order_comment
                }}
            Rectangle{
                border.color: "red"
                width: 280
                height: 50
                Text {
                    text: root_compent.order_products
                }}
            Rectangle{
                border.color: "red"
                width: 100
                height: 50
                Text {
                    text: root_compent.order_summa + " руб"
                }}
        }
        Row{
            Rectangle{
                width: 100
                height: 20
                color: "#6cc5dd"
                Text {
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    height: 20
                    text: root_compent.order_status
                }
            }
            Rectangle{
            width: 820
            height: 20
            color: "#eaddd6"
            Text {
                width: parent.width
                height: 20
                text: root_compent.order_products
            }

            }
            Rectangle{
                width: 820
                height: 20
                color: "#eaddd6"
                Text {
                    width: parent.width
                    height: 20
                    color: root_compent.order_payd_status ?  "green" : "red"
                    text: root_compent.order_payd_status ? "Оплачен": "Не Оплачено " + root_compent.order_payments + " руб."
                }

                }
        }
    }
}
