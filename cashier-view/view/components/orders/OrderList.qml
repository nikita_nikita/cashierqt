import QtQuick 2.12
import QtQuick.Layouts 1.12
Item {

    property alias model: listview_component.model

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        OrderListHeader{
            Layout.fillWidth: true
        }
        ListView{
            id: listview_component
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            delegate: OrderItem{
                onUpdateOrder: orderController.updateOrder(index)
                order_client_info: client
                order_status: status
                order_number: local_number
                order_address: address
                order_comment: comment
                order_products: products
                order_summa: amount
                order_time_delivery: cook_in_time
                order_payd_status: paid
                order_payments: payments
                order_client_name:name

            }
        }

    }

}

