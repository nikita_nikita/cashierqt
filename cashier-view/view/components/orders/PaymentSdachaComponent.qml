import QtQuick 2.0

Rectangle {
    signal close()
    color: "silver"
    property int paymentType: 0
    MouseArea{
        anchors.fill: parent
        onClicked: close()
    }
    Rectangle{
        width: 400
        height: 300
        border.color: "gray"
        border.width: 4
        anchors.centerIn: parent

    Column{
        anchors.fill: parent

        Text {
            width: parent.width-8
            height: 50
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 15
            text:paymentType === 1 ? "Оплата Наличными" : "Оплата Картой"
        }
        Text {
            width: parent.width-8
            height: 50
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 15
            text: "Остаток оплаты " +(orderController.orderProductListModel.orderCost-orderController.orderPayd) + " p.<br>Внесённая клиентом сумма:"
        }
        Rectangle{
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 30
            anchors.rightMargin: 30
            border.color: "black"
            border.width: 2
            height: 50

        TextInput{
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 5
            font.pointSize: 28
            maximumLength: 6
            horizontalAlignment: Text.AlignHCenter
            inputMethodHints: Qt.ImhFormattedNumbersOnly


        }
    }
    }
    }
}
