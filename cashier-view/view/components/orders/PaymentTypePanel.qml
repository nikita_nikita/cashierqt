import QtQuick 2.0
Rectangle{
    signal close()
    MouseArea{
        anchors.fill: parent
        onClicked: close()
    }
    Rectangle{
        anchors.centerIn: parent
        width: 200
        height: 200
        Column{
            Rectangle{
                width: 200
                height: 100
                border.color: "white"
                border.width: 7
                color:  "orange"
                Text {
                    anchors.centerIn: parent
                    text:  "Наличными"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: orderController.showPaymentPanel(0)

                }
            }
            Rectangle{
                width: 200
                height: 100
                border.color: "white"
                border.width: 7
                color:  "purple"
                Text {
                    anchors.centerIn: parent
                    text:  "Картой"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: orderController.showPaymentPanel(1)
                }
            }
        }
    }
}
