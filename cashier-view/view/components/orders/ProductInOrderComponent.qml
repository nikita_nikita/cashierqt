import QtQuick 2.13
import QtQuick.Layouts 1.12

Column{
    property string product_name
    property int product_price
    property int product_quantity
    property alias product_modifications: modificationlist.model
    property bool product_have_modifications: true
    property bool toEditor: false
    spacing: 4
    Rectangle{
        width: productItem.width
        height: 40
        border.color: "white"
        border.width: 2
        color: toEdit ? "silver" : "#CCCC99"
        Row{

            id:productItem
            height: parent.height
            spacing:20
            leftPadding: 48
            rightPadding: 20


            Text {

                width: 310
                height: parent.height
                font.pointSize: 20
                text: product_name
            }

            Text{
                width:180
                height: parent.height
                font.pointSize: 20
                text: product_quantity + "*" +product_price +" p."
                horizontalAlignment: Text.AlignRight
            }


        }


    }
    ColumnLayout{
        spacing: 0
        width: 592
        Repeater{

            id:modificationlist
            Rectangle{
                height: 40
                width: 590
        color: toEdit ? "silver" : "white"
                Row{
                    id:root_component
                    width: parent.width
                    height: parent.height

                    leftPadding: 70
                    Text {
                        width: 250
                        height: parent.height
                        font.pointSize: 17
                        horizontalAlignment: Text.AlignLeft
                        text: modificationlist.model[index].name
                    }
                    Text {
                        width: 248
                        height: parent.height
                        font.pointSize: 17
                        horizontalAlignment:  Text.AlignRight
                        text: modificationlist.model[index].cost
                    }
                }

            }
        }
    }

}

