import QtQuick 2.13

Rectangle{
    anchors.fill: parent
    border.width: 2
    property alias orderModel: repeater.model
    Column{
        padding: 5
        anchors.fill: parent
        anchors.margins: 2
        Repeater{
            id:repeater
            Rectangle{
                width: 590
                height: product_in_order_component.height
                border.color: "black"
                border.width: toEdit ? 20 : 0
                ProductInOrderComponent{
                    id:product_in_order_component
                    product_name: name
                    product_price: price
                    product_quantity: quantity
                    product_modifications: modificationlist
                    product_have_modifications: productWithModifications
                    toEditor: toEdit
                }
                MouseArea{
                anchors.fill: parent
                onClicked:  orderController.productToEdit(index)
                }
            }

        }
    }
}
