import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle {
    color: "white"
    height: 60
    width: 600
    border.color: "#CC9966"
    border.width: 10
    property string product_name
    property string product_id
//    property int product_price
        property var modifers
    Row{
        anchors.fill: parent
        anchors.leftMargin:  15

    Text {
        id: productName
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width:465
        text: product_name
        font.pointSize: 25
        color: "black"
        horizontalAlignment:  Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
    }
    Button{
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    width: 120
    text: "Добавить"
    background: Rectangle{
    anchors.fill: parent
    anchors.margins: 10
    color: "#FF3300"
    }
    onClicked:   orderController.productListModel.addToCart(product_id)
    }
    }
}
