import QtQuick 2.0
import "../main"
Rectangle{    

    signal productSerachTextChange
    property alias inputText: input.inputText
    Row{

        Text {
            height: 40
            font.pointSize: 30
            text:  "Поиск: "
        }
        SearchInput{
            id:input
            onSearchInputTextChange:{
                console.log("SearchInput signal")
                productSerachTextChange()
        }
    }
}
}
