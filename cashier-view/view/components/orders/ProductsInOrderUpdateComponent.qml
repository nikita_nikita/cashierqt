import QtQuick 2.13

Rectangle {
    property alias productModel: productInOrder.model
    id:root_compnent
    Row{
        height: 50

        Text {
            width: root_compnent.width -100
            height: 50
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Состав заказа"
            font.pointSize: 15
        }
        Rectangle{
            width: 100
            height: 50
            color: "#fe624c"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 15
                text: "Ред-ть"
            }
            MouseArea{
            anchors.fill: parent
            onClicked:
//                if(orderController.orderObject.isPayd())
                router.toProductChoice()
            }
        }

    }

    ListView{
        height: 500
        id:productInOrder
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 150
        delegate:
            Row{
            leftPadding: 30
            Text {
                width: root_compnent/1.5
                height: 50
                font.pointSize: 20
                text:  name
            }
            Text {

                width: root_compnent/3
                height: 50
                font.pointSize: 20
                horizontalAlignment: Text.AlignRight
                text:  quantity + " шт."
            }
        }

    }
    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 90
        width: root_compnent.width
        font.pointSize: 15
        horizontalAlignment: Text.AlignHCenter
        text: "Стоимость заказа<br>" + orderController.orderObject.amount + " руб."
    }
    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        color: "red"
        width: root_compnent.width
        font.pointSize: 15
        horizontalAlignment: Text.AlignHCenter
        text:  orderController.orderObject.comment
    }
}


