import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: "silver"
    property alias saveOrderModal: repeater.model
    opacity: 0.9
    signal close()
    MouseArea{
        anchors.fill: parent
        onClicked: close() }
    Column{
        anchors.centerIn: parent
        Repeater{
            id:repeater
            SaveOrderModalComponent{
                client_info: client
                deliveryType_info: delivery
                address_info: address
                cart_info: products

            }
        }
    }
}
