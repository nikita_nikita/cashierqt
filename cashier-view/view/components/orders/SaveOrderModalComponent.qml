import QtQuick 2.0

Rectangle{
    property string address_info: ""
    property string cart_info: ""
    property int deliveryType_info: -1
    property string client_info: ""
    id: root_component
    width: 400
    height: 60
    Component.onCompleted: {
        if(root_component.delivetyType === 0){
            delivery.text = "Самовывоз"
        }
        if(root_component.delivetyType === 1){
            delivery.text = root_component.address
        }
        if(root_component.delivetyType === 2){
            delivery.text = "В баре"
        }
    }
    Row{
        Text {
            id:delivery
            width: 50
            height: root_component.height
            verticalAlignment: Text.AlignVCenter
        }
        Text {

            id:client
            text: root_component.client
            width: 50
            height: root_component.height
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            id: cart
            width: 300
            height: root_component.height
            text: root_component.cart
            verticalAlignment: Text.AlignVCenter
        }

    }

}
