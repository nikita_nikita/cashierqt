import QtQuick 2.0

Rectangle{
    width: 150
    height: 60
    signal showSaveOrderList()
    anchors.right:parent.right
    color: "#FF6666"
    Text{
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        text: "Только<br>сохранённые"

    }
    Rectangle{
        color: "#FF6666"
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 140
        height: 20
        Rectangle{
            width: 120
            height: 5
            radius: 15
            border.color: "gray"
            color: "green"
            anchors.centerIn: parent
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked:
            showSaveOrderList()
    }
}
