import QtQuick 2.13
import QtQuick.Controls 2.12

Rectangle {
     color: "#000";
      Column {
          anchors.top: parent.top
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.topMargin: 50
          spacing: 20
          Label {
              font.pointSize: 20
              text: "Введите пин код или просканируйте карту"
              anchors.horizontalCenter: parent.horizontalCenter
          }


          TextField {
              anchors.horizontalCenter: parent.horizontalCenter
              inputMethodHints: Qt.ImhDigitsOnly
              onAccepted: loginController.loginByPin(this.text)
          }
          Text {
              anchors.horizontalCenter: parent.horizontalCenter
              color: "red"
              font.pointSize: 25
              text: loginController.errorMessage
              visible: loginController.errorMessageVisible

          }
      }
}
