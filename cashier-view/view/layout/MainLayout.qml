import QtQuick 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../components/main"
import "../pages"
Rectangle {
    color: "#5d5d5d"
    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        CMenuBar{
            Layout.fillWidth: true
            onSandwichClicked: drawer_component.open()
        }
        StackView{
            id: stack_view
            Layout.fillHeight: true
            Layout.fillWidth: true
            initialItem: Orders{}
            Connections {
                target: router
                onStackClear: stack_view.pop(null)
                onStackPop: stack_view.pop()
                onStackPush: stack_view.push(url)
            }
        }


    }
    GlobalDrawer{
        id: drawer_component
        width: 400
         height: window.height
    }
}


