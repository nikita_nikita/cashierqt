import QtQuick 2.9
import QtQuick.Window 2.12

import "layout"

Window {
    id: window
    visible: true
    width:  1200
    height:  600
    title: "Кассир"

    MainLayout {
        anchors.fill: parent       
    }
    LoginLayout {
        anchors.fill: parent
        visible: router.layer=="login"
    }
}


