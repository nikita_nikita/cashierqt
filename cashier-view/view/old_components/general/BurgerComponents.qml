import QtQuick 2.0

ListModel{
    ListElement { title: "Сообщения"; source: "qrc:/view/page/MessagePage.qml" }
    ListElement { title: "Список гостей"; source: "qrc:/layout/page/Guestlist.qml" }
    ListElement { title: "Отчеты"; source: "qrc:/layout/page/Otchet.qml" }
    ListElement { title: "Возврат товара"; source: "qrc:/layout/page/BackProduct.qml" }
    ListElement { title: "Чек коррекции"; source: "qrc:/layout/page/CheckKorekcii.qml" }
    ListElement { title: "Команды фискальному регистратору"; source: "qrc:/layout/page/Fiscal.qml" }
}
