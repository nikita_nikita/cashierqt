import QtQuick 2.0
import QtQuick.Controls 2.12
Item {
    id:component
    signal testing()
    visible: false
    function open(){
        component.visible = true
    }
    function close(){
        component.visible = false
    }
    Rectangle{
        anchors.fill: parent
        opacity: 0.5
        color: "#000"
        MouseArea{
            anchors.fill: parent
            onClicked: component.visible = false}
    }

}
