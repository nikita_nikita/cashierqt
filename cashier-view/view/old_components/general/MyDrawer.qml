import QtQuick 2.0
import QtQuick.Controls 2.12
    Drawer{

    interactive: false

    ProfileItem{id:profileElement}
    ListView {
        id:     listView
        height: 800
        width: 600
        interactive: false
        anchors.top: profileElement.bottom
//        anchors.left: parent.left
//        anchors.right: parent.right
//        anchors.bottom: parent.bottom
        focus: true
        currentIndex: -1
        clip: true
        delegate: ItemDelegate {
            width: parent.width
            text: model.title
            highlighted: ListView.isCurrentItem
            onClicked: {
                stackView.push(model.source)
                drawer.close()
            }
        }
        model: BurgerComponents{}

    }
}

