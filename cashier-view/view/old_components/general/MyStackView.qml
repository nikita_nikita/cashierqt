import QtQuick 2.0
import QtQuick.Controls 2.12
import "../order"
StackView {
    width: parent.width
    spacing: 0
    id: stackView
    anchors.fill: parent
    initialItem: OrderList{}
    function toMainPage(){
        if(stackView.depth > 1){
            stackView.pop(null)
        }
    }
}
