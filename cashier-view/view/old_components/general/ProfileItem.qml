import QtQuick 2.0

Rectangle {
    id:profileElement
    anchors.top: parent.top
    color: "#deff82"
    height: 100
    width:parent.width
    MouseArea{
    anchors.fill: parent
    onClicked:stackView.push("qrc:/view/layout/page/CassierProfilePage.qml")
    }
    Image {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 20
        anchors.leftMargin: 20
        width:  60
        height: 60
       source: "qrc:/images/view/images/sasha.png"
    }
    Text {
        x:120
        y:45
        id: name
        text: qsTr("Иванов Иван Иванович")
    }
}
