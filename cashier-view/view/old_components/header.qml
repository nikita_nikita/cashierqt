import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ApplicationWindow {
    id: window
    visible: true


    // mareker !
    // for Ancroid
//    width: screen.width
//    height: screen.height
    // for Desktop
        width:  1200
        height:  600

    onClosing: {
        close.accepted = false;
        return;
    }
    //    flags: Qt.FramelessWindowHint
    title: "Кассир"
    color: "red"


    header: ToolBar{
        background: Rectangle{
        color: "white"}
        visible:  !loginForm.visible
        Material.foreground: "black"
        Material.background: "white"
        id: toolbar
        property int dateCount: 8
       RowLayout{
           id:topBar
           spacing: 20
           anchors.fill: parent
           ToolButton {
               icon.source: "qrc:/images/view/images/icons/drawer"
               onClicked:  drawer.open()
           }
           TabBar {
               id: bar
               property string name
               property string lastname
               property string phone
               property bool delivery
               property bool haveOrder: false
               property bool change: false
               clip: true
               height: parent.height
               Material.theme: Material.Dark
               Material.accent: "black"
               spacing: 0
               padding: 0
               Layout.fillHeight: true
               width: childrenRect.width
               TabButton {
                   height: parent.height+8
                   id:allButton
                   width: 150
                   text: qsTr("Все")
                   onClicked: stackView.toMainPage()
                   background: Rectangle{
                   color: allButton.focus ? "#dfe384" : "white"}
               }
               TabButton {
                   height: parent.height+10
                   id:deliveryButton
                   width: 150
                   text: qsTr("Доставка")
                   onClicked: stackView.toMainPage()
                   background: Rectangle{
                       color: deliveryButton.focus ? "#dfe384" : "white"}
               }

               TabButton {
                   height: parent.height+10
                   id:pickupButton
                   width: 150
                   text: qsTr("Самовывоз")
                   onClicked: stackView.toMainPage()
                   background: Rectangle{
                       color: pickupButton.focus ? "#dfe384" : "white"}
               }
           }

           Rectangle{
               visible: bar.haveOrder && stackView.depth === 1 ? true: false
               height: 50
               width: 80
               color: "gray"
               MouseArea{
                   anchors.fill: parent
                   onClicked: stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")
               }
               Text {
                   anchors.fill: parent
                   verticalAlignment: Text.AlignVCenter
                   horizontalAlignment: Text.AlignHCenter
                   font.pointSize: 35
                   color: "red"
                   text: "!!!!"}
           }

       }
    }
}
