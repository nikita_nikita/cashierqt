import QtQuick 2.0
import QtQuick.Controls 2.12
Drawer{
    height: parent.height
    width: parent.width/2
    Rectangle{
            id:adres
            color:"gray"
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: 50
            anchors.left: client.right
            height: parent.height-parent.height/5
            anchors.leftMargin: margin*2.5
            MyLabel{
                id:adresLabel
                text: "<b>АДРЕС</b>"}
            Rectangle{
                id:datatimeRec
                border.color: "#d5d5d5"
                anchors.top: adresLabel.bottom
                height: parent.height/7
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/3
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        text: "Время:<br>17:08<br>(сегодня)"}
                }
                Rectangle{
                    id:type
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/3
                    width: parent.width/3
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        text: "Тип заказа<br>Доставка<br>курьером"}
                }
                Rectangle{
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*2/3
                    width: parent.width/3
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Реклама<br>Не задано"}
                }
            }
            Rectangle{
                id:streetRec
                border.color: "#d5d5d5"
                anchors.top: datatimeRec.bottom
                height: parent.height/8.45
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/2
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Улица:<br>Повова"}
                }
                Rectangle{
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/2
                    width: parent.width/2
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Район:<br>не задан"}
                }

            }
            Rectangle{
                id:houseRec
                border.color: "#d5d5d5"
                anchors.top: streetRec.bottom
                height: parent.height/11
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Дом"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/4
                    width: parent.width/4
                    TextInput {id: house
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/2
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Подъезд"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*3/4
                    width: parent.width/4
                    TextInput {id: porch
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }

            }
            Rectangle{
                id:corpusRec
                border.color: "#d5d5d5"
                anchors.top: houseRec.bottom
                height: parent.height/11
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Корпус"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/4
                    width: parent.width/4
                    TextInput {id: corpus
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/2
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Этаж"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*3/4
                    width: parent.width/4
                    TextInput {id: floAt
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }

            }
            Rectangle{
                id:kvRec
                border.color: "#d5d5d5"
                anchors.top: corpusRec.bottom
                height: parent.height/11
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Квартира"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/4
                    width: parent.width/4
                    TextInput {id: kv
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/2
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Домофон"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*3/4
                    width: parent.width/4
                    TextInput {id: domo
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }

            }
            Rectangle{
                id:primRec
                border.color: "#d5d5d5"
                anchors.top: kvRec.bottom
                height: parent.height/11
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Примеч."}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/4
                    width: parent.width*3/4
                    TextInput {id: prim
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }


            }
            MyLabel{
                anchors.top: primRec.bottom
                id:courierLabel
                text: "<b>Курьер</b>"}
            Rectangle{
                id:courierRec
                border.color: "#d5d5d5"
                anchors.top: courierLabel.bottom
                height: parent.height/9
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Курьер:<br>не задан"}
                }
            }
            Rectangle{
                id:comentRec
                border.color: "#d5d5d5"
                anchors.top: courierRec.bottom
                height: parent.height/7
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle{
                    color: "#e7ddd5"
                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    width: parent.width/4
                    Text {anchors.fill: parent
                        horizontalAlignment:  Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Комент.<br>к заказу"}
                }
                Rectangle{

                    border.color: "#d5d5d5"
                    anchors.top: parent.top
                    height: parent.height
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width/4
                    width: parent.width*3/4
                    TextInput {
                        id: coment

                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter}
                }


            }    }

}
