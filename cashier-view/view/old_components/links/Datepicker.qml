import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle {
    property int dateCount: 7
    Image {
        id: leftButton
        source: "qrc:/images/images/leftButton.png"
        sourceSize.width: 33
        sourceSize.height: 33
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(dateCount ==0){
                    dateCount = 30
                    }
                dateCount--
        }


    }
    Label {
        id:dateLabel
        font.pixelSize: 22
        text: Qt.formatDate(dateCount + " Сентрября 2019");


    }
    Image {
        id: rightButton
        source: "qrc:/images/images/rightButton.png"
        sourceSize.width: 33
        sourceSize.height: 33
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(dateCount == 30){
                    dateCount = 0
                    }
                dateCount++
            }
        }

    }
}
}
