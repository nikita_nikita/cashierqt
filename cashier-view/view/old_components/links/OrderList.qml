import QtQuick 2.0

ListModel {
    id: orders
    ListElement{ statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew";status: "Неподтвержденные";number:111;time: "13:47";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:666; productName:"Шавуха за полтос";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusOk";  status: "Отменён";number:121;time: "15:45";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:854; productName:"Шавуха за полтос";delivery:false;pay:false}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusCourier"; status: "В пути";number:131;time: "17:09";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:768; productName:"Шавуха за сотку";delivery:true;pay:false}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusCourier"; status: "Готово";number:141;time: "18:55";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:1109; productName:"Шавуха за сотку";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusCourier"; status: "Новое";number:151;time: "17:48";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:457; productName:"Шавуха за сотку";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "В пути";number:161;time: "16:44";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:299; productName:"Шавуха за сотку";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "Готовится";number:171;time: "15:37";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:125; productName:"Шавуха за 130";delivery:false;pay:false}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "Закрыто";number:181;time: "14:09";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:762; productName:"Шавуха за 130";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "Готово";number:191;time: "13:09";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:567; productName:"Шавуха за 130";delivery:true;pay:true}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "Закрыто";number:201;time: "10:25";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:854; productName:"Шавуха за сотку";delivery:true;pay:false}
    ListElement{statusImage:"qrc:/images/images/TimeStatusIcons/StatusNew"; status: "Закрыто";number:211;time: "12:09";adress:"Кудыкина гора д1";client:"Баба Яга +79101101736";comment:"Запеченых детишек мне :3";coast:669; productName:"Шавуха за сотку";delivery:true;pay:true}
}
