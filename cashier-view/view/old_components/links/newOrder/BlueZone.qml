import QtQuick 2.0
import QtQuick.Layouts 1.12
GridLayout{

    rows:  2
    columns: 2
    columnSpacing: 0
    rowSpacing: 0
    width: 120
    height: 80
    Rectangle{
        id: leftTop
        color: "#70ceea";  width: 70; height: 50
        Image{  source: statusImage;width: 50;height: 50}
    }
    Rectangle{color: "#70ceea"; width: 50; height: 50
        Text {text:"№ "+ number  }
    }
    Rectangle{
        width: 120
        height: 30
        Layout.columnSpan: 2
        color:"#6ac4df"
        Text {text: qsTr(status)
        }
    }
}
