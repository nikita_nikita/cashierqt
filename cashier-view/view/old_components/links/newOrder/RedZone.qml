import QtQuick 2.0
import QtQuick.Layouts 1.12

GridLayout{
    Layout.fillHeight: true
    Layout.topMargin: -30
    rows: 2
    columnSpacing: 0
    rowSpacing: 0
    width: 800
    height: 80
    Rectangle{
        id: secondleftTop
        color: "#fcced0";  width: 1000; height: 50
        Text {text: qsTr(time);id:ordertime;anchors.left: parent.left; width:50 }
        Text {text: qsTr(adress);id:orderadress; anchors.leftMargin: 10;anchors.left: ordertime.right; width: 230}
        Text {text: qsTr(client);id: orderclient; anchors.leftMargin: 10;anchors.left: orderadress.right; width: 230}
        Text {text: qsTr(comment);id: ordercomment; anchors.leftMargin: 10;anchors.left: orderclient.right; width: 230}
        Text {text: qsTr(coast + "руб."); anchors.leftMargin: 10;anchors.left: ordercomment.right; width: 230}
    }


    Rectangle{
        anchors.top:   secondleftTop.bottom
        anchors.left: parent.left
        //                    Layout.fillHeight: true
        //                    Layout.fillWidth: true
        //                    Layout.Top:secondleftTop.bottom
        //                    Layout.topMargin: 40
        width: secondleftTop.width
        height: 30
        color:"#eaddd5"
        Text {text: qsTr(productName)
        }
    }
}
