import QtQuick 2.0

Rectangle{
    id:component
    border.color: "gray"
    property int typeOfColor:  0
    color:  getColor(typeOfColor)
    property alias notificationText: messageInfo.text
    signal close();

    Row{
        Text{
            id:messageInfo;
            verticalAlignment: Text.AlignVCenter;
            font.pointSize: 15;
            height: 40;
            width: 180
        }
        Image {
            width: 30
            height: 30
            source: "qrc:/images/view/images/icons/mail.png"
        }
        Text{
            text: index + 1
            font.pointSize: 13
            anchors.top: parent.top
            anchors.topMargin: 3
            width: 30
        }
        Image {
            //sverticalAlignment: Text.AlignVCenter;
            height: 40;
            width: 40
            source: "qrc:/images/view/images/icons/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: component.close()

            }
        }

    }
    function getColor(typeOfColor){
        switch (typeOfColor) {
        case 0:
            console.log("info")
            return "#87CEEB"
        case 1:
            console.log("warning")
            return "#F0E68C"
        case 2:
            console.log("error")
            return "#90EE90"
        default:
            return "#87CEEB"
        }
    }
}
