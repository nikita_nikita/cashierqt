import QtQuick 2.0

Item {

    Repeater{
        id:repeater
        NotificationItem{
            width: 300
            height: 50
            notificationText: message
            typeOfColor: type
            onClose: notificationController.readMessage(indexer)
        }
        model:notificationController.model
    }

}
