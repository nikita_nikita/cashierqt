import QtQuick 2.0
import QtQuick.Layouts 1.1

ListView {
    clip: true
    contentWidth: 0
    Layout.fillWidth: true
    Component {
        id: my
        GridLayout{
            visible: (toolbar.dateCount % 2
                      || dontaccepted.use && status === "Неподтвержденные"
                      ||indelivery.use && status === "В пути"
                      ||neworder.use && status === "Новое"
                      ||cooking.use && status === "Готовится"
                      ||done.use && status === "Готово"
                      ||close.use && status === "Закрыто"
                      ||cenceled.use && status === "Отменён"
                      ||deliveryButton.focus === true && delivery === false && allButton.focus === false
                      ||deliveryButton.focus !== true && delivery === true && allButton.focus === false) ? false : true
            height:
                (  dontaccepted.use && status === "Неподтвержденные"
                 ||indelivery.use && status === "В пути"
                 ||neworder.use && status === "Новое"
                 ||cooking.use && status === "Готовится"
                 ||done.use && status === "Готово"
                 ||close.use && status === "Закрыто"
                 ||cenceled.use && status === "Отменён"
                 ||deliveryButton.focus === true && delivery === false && allButton.focus === false
                 ||deliveryButton.focus !== true && delivery === true && allButton.focus === false) ? 0 : 80
            columnSpacing: 0
            columns: 2
            GridLayout{
                rows:  2
                columns: 2
                columnSpacing: 0
                rowSpacing: 0
                width: 150
                height: 120
                Rectangle{
                    MouseArea{
                       anchors.fill: parent
                       onClicked:{
                       stackView.push("qrc:/layout/page/OrderInfolight.qml")
                       }}
                id: leftTop
                color: getColor(status); width: 150; height: 50
                Text {
                    anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                    font.pixelSize: 25
                    text:"<b>"+ number+"</b>"}}



            Rectangle{         MouseArea{
                    anchors.fill: parent
                     onClicked:{

                         stackView.push("qrc:/view/layout/page/OrderInfolight.qml")
                     }}

                Layout.fillWidth: true
                width: 150
                height: 30
                Layout.columnSpan: 2
                color:"#6ac4df"
                Text {text: qsTr(status)
                }
            }
        }
        GridLayout{
            Layout.fillHeight: true
            rows: 2
            columnSpacing: 0
            rowSpacing: 0
            x:120
            width: window.width
            height: 80
            Rectangle{
                MouseArea{
                   anchors.fill: parent
                    onClicked:{

                        stackView.push("qrc:/view/layout/page/OrderInfolight.qml")
                    }}
                Layout.fillWidth: true
                id: secondleftTop
                color: "#fcced0";  width: parent.width; height: 50
                Text {text: qsTr(time);id:ordertime;anchors.left: parent.left; width:parent.width*0.05 }
                Text {text: qsTr(adress);id:orderadress; anchors.leftMargin: 10;anchors.left: ordertime.right; width: parent.width*0.23}
                Text {text: qsTr(client);id: orderclient; anchors.leftMargin: 3;anchors.left: orderadress.right; width: parent.width*0.23}
                Text {text: qsTr(comment);id: ordercomment; anchors.leftMargin: 3;anchors.left: orderclient.right; width: parent.width*0.25}
                Text {text: qsTr(coast + "руб."); anchors.leftMargin: 3;anchors.left: ordercomment.right; width: parent.width*0.23}
            }


            Rectangle{
                MouseArea{
                   anchors.fill: parent
                    onClicked:{

                        stackView.push("qrc:/view/layout/page/OrderInfolight.qml")
                    }}
                Layout.fillWidth: true
                Layout.row: 2
                width: secondleftTop.width
                height: 30
                color:"#eaddd5"
                Text {text: productName
                }
                Text{

                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    text: pay ? "Оплачен": "Ожидает оплаты"
                    color: pay? "black": "red"
                }
            }
        }
    }}
//model: OrderList {}
delegate: my
focus: true
function getColor(status){

    if(status === "Неподтвержденные"){
    return  "deepskyblue"
    }
    if(status === "В пути"){
    return "yellowgreen"
    }
    if(status === "Новое"){
    return "tomato"
    }
    if(status === "Готовится"){
     return "skyblue"
    }
    if(status === "Готово"){
        return "sandybrown"
    }

    if(status === "Закрыто"){
      return "silver"
    }

    if(status === "Отменён"){
        return "silver"
    }
}

}



