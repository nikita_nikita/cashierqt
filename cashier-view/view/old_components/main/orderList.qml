import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
Rectangle{
        Layout.fillWidth: true;Layout.fillHeight: true
//    anchors.top: parent.top;anchors.topMargin: -3
    Rectangle{anchors.top: parent.top;width:parent.width;border.width: 0; height: 60;id:lightbackground
        Button{width: parent.width/5;
            anchors.left: parent.left;anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
            id:dontaccepted; property bool use: false
            text: "Неподтвержденные 0";background: Rectangle{color: dontaccepted.use ? "#3f4140":"#dfe384"}
            Material.foreground:  dontaccepted.use? "white" : "black"
            onClicked: {
                console.log(dontaccepted.use)
                dontaccepted.use ? dontaccepted.use = false : dontaccepted.use = true}}

        Button{width: parent.width*0.13;anchors.left: parent.left;anchors.leftMargin: dontaccepted.width;anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
            id:neworder;property bool use: false
            text: "Новые 0";background: Rectangle{color: neworder.use ? "#3f4140":"#dfe384"}
            Material.foreground:  neworder.use? "white" : "black"
            onClicked: {
                console.log(neworder.use)
                neworder.use ? neworder.use = false : neworder.use = true}}

        Button{width: parent.width*0.13;anchors.left: parent.left;anchors.leftMargin: dontaccepted.width+neworder.width;anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
            property bool use: false
            id:cooking;text: "Готовится 0";background: Rectangle{color: cooking.use ? "#3f4140":"#dfe384"}
            Material.foreground:  cooking.use? "white" : "black"
            onClicked: {
                console.log(cooking.use)
                cooking.use ? cooking.use = false : cooking.use = true
            }}


            Button{width: parent.width*0.13; anchors.left: parent.left;anchors.leftMargin: dontaccepted.width+neworder.width+cooking.width;anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
                 property bool use: false
                id:done;text: "Готовы 0";background: Rectangle{color: done.use ? "#3f4140":"#dfe384"}
                Material.foreground:  done.use? "white" : "black"
                onClicked: {
                    console.log(cooking.use)
                    done.use ? done.use = false : done.use = true
                }}
            Button{width: parent.width*0.13; anchors.left: parent.left;anchors.leftMargin: dontaccepted.width+neworder.width+cooking.width+done.width;anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
                 property bool use: false
                id:indelivery;text: "В пути 0";background: Rectangle{color: indelivery.use ? "#3f4140":"#dfe384"}
                Material.foreground:  indelivery.use? "white" : "black"
                onClicked: {
                    console.log(cooking.use)
                    indelivery.use ? indelivery.use = false : indelivery.use = true
                }}
     Button{width: parent.width*0.13;
            anchors.left: parent.left; anchors.leftMargin: dontaccepted.width+neworder.width+cooking.width+done.width+close.width; anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
            id:close; property bool use: false
            text: "Закр-е 0";background: Rectangle{color: close.use ? "#3f4140":"#dfe384"}
            Material.foreground:  close.use? "white" : "black"
            onClicked: {
                console.log(dontaccepted.use)
                close.use ? close.use = false : close.use = true
            }}

        Button{width: parent.width*0.17;
            anchors.left: parent.left;anchors.leftMargin: dontaccepted.width+neworder.width+cooking.width+done.width+close.width+indelivery.width; anchors.top: parent.top;anchors.topMargin: -5;anchors.bottom: parent.bottom;anchors.bottomMargin: -5
            id:cenceled; property bool use: false;text: "Отмена 0"
            background: Rectangle{color: cenceled.use ? "#3f4140":"#dfe384"}
            Material.foreground:  cenceled.use? "white" : "black"
            onClicked: {
            console.log(dontaccepted.use)
            cenceled.use ? cenceled.use = false : cenceled.use = true
            }}
    }
        Rectangle{color: "#79919d"
        anchors.top:lightbackground.bottom
        id: orderId
        height: 30
        width: parent.width

        Text {id:infostatus;  width: 75; text: "Номер"; height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infonumber;  width: 75; text: "Статус"; anchors.left: infostatus.right; height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infotime;    width: parent.width*0.07; text: "Время" ; anchors.left: infonumber.right;height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infoadress;  width: parent.width*0.23; text: "Адрес"; anchors.left: infotime.right;  height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infoclient;  width: parent.width*0.23; text: "Клиент"; anchors.left: infoadress.right;height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infocomment; width: parent.width*0.23; text: "Коментарий";anchors.left: infoclient.right;height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }
        Text {id:infoprice;   width: parent.width*0.07; text: "Цена"; anchors.left: infocomment.right ;height: parent.height; horizontalAlignment: Text.AlignHCenter;verticalAlignment: Text.AlignVCenter }

    }
    OrderModel{
        spacing: 4
        anchors.topMargin: 4
        width: parent.width
        anchors.top:orderId.bottom
        anchors.bottom: parent.bottom
    }

}
