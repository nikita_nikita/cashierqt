import QtQuick 2.0

ListView {
    anchors.fill: parent
    model:notificationController.model
    delegate: Rectangle{
        width: 400
        height: 40
    color: getColor(model.type)
    Row{
    Text{text: message;width: 400;height: 20}
    Rectangle{
        width: 10;
        height: 10
        color: "red";

    }
    }
    }
    function getColor(typeOfColor){
        switch (typeOfColor) {
        case 0:
            console.log("info")
            return "#87CEEB"
        case 1:
            console.log("warning")
            return "#F0E68C"
        case 2:
            console.log("error")
            return "#90EE90"
        default:
            return "#87CEEB"
        }
    }
}
