import QtQuick 2.6
import QtQuick.Controls 2.2

TabButton{
    id: tabButton;
    font.pointSize: 15;
    property var index;
    property var currentIndex;
    contentItem: Text{
        height: tabButton.height;
        color: "#353637"
        horizontalAlignment: Text.AlignHCenter;
        verticalAlignment: Text.AlignVCenter;
        font.pointSize: 15;
        text: tabButton.text;
    }

    background: Rectangle{
    color: allButton.focus ? "#dfe384" : "white"}

}
