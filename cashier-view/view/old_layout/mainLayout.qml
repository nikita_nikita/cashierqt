import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import QtQuick.Controls.Material 2.12
import QtQml 2.0

//import "../components/general"
//import "../page"


ApplicationWindow {
    id: window
    visible: true


    // mareker!
    // for Ancroid
//    width: screen.width
//    height: screen.height
    // for Desktop
        width:  1200
        height:  600

    onClosing: {
        close.accepted = false;
        return;
    }
    title: "Кассир"
    color: "red"


    header: ToolBar{
        background: Rectangle{
        color: "white"}
        visible:  !loginForm.visible
        Material.foreground: "black"
        Material.background: "white"
        id: toolbar
        property int dateCount: 8
        RowLayout {
            id:topBar
            spacing: 20
            anchors.fill: parent
            ToolButton {
                icon.source: "qrc:/images/view/images/icons/drawer"
                onClicked:  drawer.open()
            }
            TabBar {
                id: bar
                property string name
                property string lastname
                property string phone
                property bool delivery
                property bool haveOrder: false
                property bool change: false
                clip: true
                height: parent.height
                Material.theme: Material.Dark
                Material.accent: "black"
                spacing: 0
                padding: 0
                Layout.fillHeight: true
                width: childrenRect.width
                TabButton {
                    height: parent.height+8
                    id:allButton
                    width: 150
                    text: qsTr("Все")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: allButton.focus ? "#dfe384" : "white"}
                }
                TabButton {
                    height: parent.height+10
                    id:deliveryButton
                    width: 150
                    text: qsTr("Доставка")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: deliveryButton.focus ? "#dfe384" : "white"}
                }

                TabButton {
                    height: parent.height+10
                    id:pickupButton
                    width: 150
                    text: qsTr("Самовывоз")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: pickupButton.focus ? "#dfe384" : "white"}
                }
            }

            Rectangle{
            anchors.left: bar.right
            anchors.right: leftButton.left
            }



            Rectangle{
                visible: bar.haveOrder && stackView.depth === 1 ? true: false
                height: 50
                width: 80
                color: "gray"
                MouseArea{
                    anchors.fill: parent
                    onClicked: stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")
                }
                Text {
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 35
                    color: "red"
                    text: "!!!!"}
            }

        }
    }





    StackView{
        width: parent.width
        spacing: 0
        id: stackView
        anchors.fill: parent
//        initialItem: Orders{}
        function toMainPage(){
            if(stackView.depth > 1){
                stackView.pop(null)}}
    }
//    MyDrawer{
//        id:drawer
//        width: Math.min(window.width, window.height) / 3 * 2
//        height: window.height

//    }
    Component {
              id: contactDelegate
              Item {
                  width: 180; height: 40
                  Column {
                      Text { text: '<b>message:</b> '+ message  }
                      Text { text: '<b>Number:</b> ' + visible }
                  }
              }
          }

    ListView {

        model: notificationController.visibleModel
        width: parent.width
        height: parent.height
        delegate: contactDelegate
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }

    }

    /*FAB{
        id:fab
        text:  "Новый заказ"
        onClicked:{
            //            if(!bar.haveOrder){
            stackView.push("qrc:/view/layout/page/NewOrder.qml")}
        //            else{
        //                stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")
        //            }
    }*/
    Button{
        y:parent.height -100
        x:300
        id:error
        text:  "ERROR"
        onClicked:{
            notificationController.errorNotification("Ошибка соединения")
        }
    }
    Button{
        y:parent.height -100
        x:200
        id:warning
        text:  "WARNING"
        onClicked:{
            notificationController.warningNotification("Просроченый заказ")
        }
    }
    Button{
        y:parent.height -100
        x:100
        id:info
        text:  "Info"
        onClicked:{
            notificationController.infoNotification("О-о-о-о")
        }
        }
    Button{
        y:parent.height -400
        x:100

        text:  "true"
        onClicked:{
            notificationController.setVisibleTrue(true)
        }
        }
    Button{
        y:parent.height -500
        x:100

        text:  "flase"
        onClicked:{
            notificationController.setVisibleTrue(false)
        }
        }





    //    InputPanel {
    //        id: inputPanel
    //        z: 99
    //        x: 0
    //        y: window.height
    //        width: window.width

    //        states: State {
    //            name: "visible"
    //            when: inputPanel.active
    //            PropertyChanges {
    //                target: inputPanel
    //                y: window.height - inputPanel.height
    //            }
    //        }
    //        transitions: Transition {
    //            from: ""
    //            to: "visible"
    //            reversible: true
    //            ParallelAnimation {
    //                NumberAnimation {
    //                    properties: "y"
    //                    duration: 250
    //                    easing.type: Easing.InOutQuad
    //                }
    //            }
    //        }
    //    }

//    LoginForm {
//        id:loginForm
//        anchors.fill: parent
//        visible: loginController.shiftIsOpen

//    }

    //    ListView{
    //        width: 200
    //        height: contentItem.childrenRect.width
    //    }

//    Modal{
//        id:ok
//        anchors.fill: parent
//        Rectangle{
//            anchors.centerIn: parent
//            width: 300
//            height: 200
//            Rectangle{
//                color: "palegreen"
//                width: 300
//                height: 150
//                Text {
//                    verticalAlignment: Text.AlignVCenter
//                    horizontalAlignment: Text.AlignHCenter
//                    anchors.fill: parent
//                    font.pointSize: 20
//                    text: "Смена уже открыта"
//                }
//            }
//            Button{
//                width: 300
//                height: 50
//                onClicked: ok.close()
//                anchors.top: parent.top
//                anchors.topMargin: 150
//                background: Rectangle{
//                    anchors.fill:parent
//                    color: "white"
//                }
//                Text{
//                    verticalAlignment: Text.AlignVCenter
//                    horizontalAlignment: Text.AlignHCenter
//                    anchors.fill: parent
//                    font.pointSize: 20
//                    text: "OK"}
//            }
//        }

//    }

}


