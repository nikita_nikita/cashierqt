import QtQuick 2.0
import QtQuick.Controls 2.12

import "../page/newOrder"
Rectangle{id:client
    anchors.fill: parent
    ClientInfo{ id:nameRec;mytext: "Имя";  anchors.top: clientlable.bottom;inputText: bar.name; textSize: 25}
    Rectangle {
        id:datepicker
        anchors.top: nameRec.bottom
        border.color: "#e7ddd5"
        border.width: 10
        height: parent.height/4.8
        anchors.left: parent.left
        anchors.right: parent.right
        Rectangle{
            border.color: "#d5d5d5"
            border.width: 1
            color: "#e7ddd5"
            anchors.top: parent.top
            height: parent.height
            anchors.left: parent.left
            width: parent.width/3.5
            Text {
                font.pointSize: 25
                anchors.fill: parent
                horizontalAlignment:  Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: "День<br>рождения"}
        }
        Rectangle{border.color: "#d5d5d5"
            border.width: 1
            color: "#e9e9ef"
            anchors.top: parent.top
            height: parent.height
            anchors.left: parent.left
            anchors.leftMargin: parent.width/3.5
            anchors.right: parent.right
            Image { source: "qrc:/images/images/icons/datepicker_smol.png"
                anchors.left: parent.left
                anchors.leftMargin: parent.width/3
            }
        }

    }
    Button{
        anchors.top: datepicker.bottom
        width: 80
        height: parent.height/7
        anchors.left: parent.left
        anchors.right: parent.right
        font.pointSize: 44
        background: Rectangle{
            color: "skyblue"
        }
        text: "Сохранить и продолжить"
        onClicked: stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")
    }
}
