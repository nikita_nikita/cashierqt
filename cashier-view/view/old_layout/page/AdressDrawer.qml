import QtQuick 2.0
import QtQuick.Controls 2.12

Drawer{

    interactive: true
    edge:Qt.RightEdge
    Rectangle{
        color: "gray"
        id:adres

        width: parent.width
        anchors.top: parent.top
        anchors.left: client.right
        height: parent.height-parent.height/5
        anchors.leftMargin: margin*2.5
//        MyLabel{
//            id:adresLabel
//            text: "<b>АДРЕС</b>"}

        Rectangle{  color: "gray"
            id:streetRec

            anchors.top: adresLabel.bottom
            height: parent.height/8.45
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/2
                Text {anchors.fill: parent
                    color: "white"
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Улица:<br>Повова"}
            }
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/2
                width: parent.width/2
                Text {
                    color: "white"
                    anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Район:<br>не задан"}


            }

        }
        Rectangle{  color: "gray"
            id:houseRec
            anchors.top: streetRec.bottom
            height: parent.height/11
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: "Дом"}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4
                width: parent.width/4
                TextInput {id: house
                    anchors.fill: parent

                    color: "white"
                    text: "31A"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/2
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    color: "white"

                    text: "Подъезд"}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width*3/4
                width: parent.width/4
                TextInput {id: porch
                    anchors.fill: parent

                    color: "white"
                    text: "11"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }

        }
        Rectangle{  color: "gray"
            id:corpusRec
            anchors.top: houseRec.bottom
            height: parent.height/11
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/4
                Text {anchors.fill: parent

                    color: "white"

                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Корпус"}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4
                width: parent.width/4
                TextInput {id: corpus
                    anchors.fill: parent

                    color: "white"
                    text: "2"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/2
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: "Этаж"}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width*3/4
                width: parent.width/4
                TextInput {id: floAt
                    anchors.fill: parent

                    color: "white"
                    text: "8"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }

        }
        Rectangle{  color: "gray"
            id:kvRec

            anchors.top: corpusRec.bottom
            height: parent.height/11
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    color: "white"

                    text: "Квартира"}
            }
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4
                width: parent.width/4
                TextInput {id: kv

                    color: "white"
                    text: "53"
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/2
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    color: "white"

                    text: "Домофон"}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width*3/4
                width: parent.width/4
                TextInput {id: domo
                    color: "white"
                    text: "Есть"
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }

        }
        Rectangle{  color: "gray"
            id:primRec
            anchors.top: kvRec.bottom
            height: parent.height/11
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/4
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: "Примечан."}
            }
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4
                width: parent.width*3/4
                TextInput {
                    id: prim

                    color: "white"
                    text: "Англоговорящий клиент"
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }


        }
//        MyLabel{
//            anchors.top: primRec.bottom
//            id:courierLabel

//            color: "white"

//            text: "<b>Курьер</b>"}
        Rectangle{  color: "gray"
            id:courierRec

            anchors.top: courierLabel.bottom
            height: parent.height/9
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    color: "white"

                    text: "Курьер:<br>не задан"}
            }
        }
        Rectangle{  color: "gray"
            id:comentRec

            anchors.top: courierRec.bottom
            height: parent.height/7
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{  color: "gray"

                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/4
                Text {anchors.fill: parent
                    color: "white"
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Комент.<br>к заказу"}
            }
            Rectangle{  color: "gray"


                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/4
                width: parent.width*3/4
                TextInput {
                    id: coment
                    color: "white"
                    text: "Англоговорящий клиент"
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter}
            }


        }
   }
    AdressDrawerChange{

        width: parent.width/2
        height: parent.height
        id:adressDrawerChange}
    Rectangle{
        y:0
        x:400
        height: 50
        width: 70
        color: "tomato"
        MouseArea{
        anchors.fill: parent
        onClicked: adressDrawerChange.open()
        }

        Text {
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Ред-ть"}

    }
}

