import QtQuick 2.0

Rectangle{

    anchors.fill: parent
    Text {
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 40
        text: "Окно возврата товара.<br>
               Поиск по номеру телефона, времени или номеру заказа"
    }

}
