import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import "../../layout/page/newOrder"
Drawer{
Rectangle{
    property int margin: 1
    color: "#414241"
    anchors.fill: parent

    Rectangle{id:client
        color: "red"
        width: parent.width
        height: 40
        anchors.top: parent.top
        anchors.left: parent.left
        Label{    horizontalAlignment:  Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 15
            width: parent.width
            height: 30
            color: "white"
            background: Rectangle{
            color: "gray"}
            id:clientlable
            text: "<b>КЛИЕНТ</b>"}
        ClientInfo{ id:nameRec;    mytext: "Имя";  anchors.top: clientlable.bottom; inputText: "Иван"}
        ClientInfo{ id:lastnameRec;mytext: "Телефон";  anchors.top: nameRec.bottom;inputText: "+7 904 348 88 77"}
        ClientInfo{ id:phoneRec;   mytext: "День Рождения";  anchors.top: lastnameRec.bottom;inputText: "18 Октября"}
        Button{
            id:dopRec
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top:  phoneRec.bottom
            height: parent/7
            text: "Дополнительная информация"
            background: Rectangle{
                anchors.fill: parent
            }
            Material.foreground: "#252525"}
        Rectangle{id:withoutDiscount
            border.color: "#d5d5d5"
            anchors.top: dopRec.bottom
            height: 50
            anchors.left: parent.left
            anchors.right: parent.right
            Rectangle{

                border.color: "grey"
                color: "#414241"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                width: parent.width/2
                Text {anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: "Без скидки/<br>ценовой категории"}
            }
            Rectangle{
                color: "#e9e9ef"
                anchors.top: parent.top
                height: parent.height
                anchors.left: parent.left
                anchors.leftMargin: parent.width/2
                width: parent.width/2
                Text {
                    anchors.fill: parent
                    horizontalAlignment:  Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Не в черном списке"}
            }

        }}






    Label{
        anchors.top: parent.top
        anchors.topMargin: 270
        id:courierlable
        horizontalAlignment:  Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 15
        width: parent.width
        height: 30
        color: "white"
        background: Rectangle{
        color: "gray"}
        text: "<b>КОМЕНТАРИЙ К ЗАКАЗУ</b>"}
    Rectangle{

        width: parent.width
        height: 40
        anchors.top: courierlable.bottom
        color: "#d5d5d5"
        TextInput{

            anchors.left: parent.left
            anchors.leftMargin: 10
            font.pointSize: 15
            anchors.fill: parent
            anchors.bottom: parent.bottom
            text: "Без чеснока, майонеза, рыбы "
        }

    }

        }
}
