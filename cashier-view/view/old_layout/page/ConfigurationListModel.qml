import QtQuick 2.0

ListModel {
    id: productsListModel
    ListElement{ name:"Филадельфия классик"; price: 250; topping: false}
    ListElement{ name:"Филадельфия Дабл"; price: 250; topping: false}
    ListElement{ name:"WOK Гречневый"; price: 250; topping: true}
    ListElement{ name:"WOK Яичный"; price: 250; topping: true}
    ListElement{ name:"Pizza Bonna"; price: 250; topping: true}
    ListElement{ name:"Pizza Donna"; price: 250; topping: true}
    ListElement{ name:"Pizza Gonna"; price: 250; topping: true}
    ListElement{ name:"Суп Обычный"; price: 250; topping: false}
    ListElement{ name:"Суп необычный"; price: 250; topping: false}
    }
