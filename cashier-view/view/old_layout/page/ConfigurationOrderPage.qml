import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
//    Component.onCompleted: {
//        bar.haveOrder = true
//        //marker !  При печати чека bar.haveOrder = false
//    }

    MouseArea{width: parent.width/2
        height: parent.height/2
        onClicked:{
            input.focus = false}
        visible:input.focus ? true: false}

    anchors.fill: parent
    color: "red"
    Rectangle{

        border.color: "gray"
        border.width: 3
        width: parent.width/2
        height: parent.height
        Rectangle{

            border.color: "gray"
            id:listRec
            width: parent.width
            height: parent.height-80
            ListView{
                clip: true
                anchors.fill: parent
                anchors.topMargin: 2
                anchors.rightMargin: 2
                model:UserOrderListModel{}
                delegate:UserOrderListElement{}

            }
        }
        Rectangle{
            color: "gray"
            anchors.bottom: parent.bottom
            height: 80
            width: parent.width
            Grid{

                anchors.fill:parent
                columns: 2
                rows: 2
                Rectangle{
                    height: parent.height/2;width: parent.width/3
                    border.color: "gray"
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {kupon.open()}
                    }
                    Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: "Купон"
                    }
                }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/3
                    height: parent.height/2;width: parent.width/3
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {sale.open()}
                    }
                    Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: "Скидка<br>20%"
                    }
                }
                Rectangle{
                    border.color: "gray"
                    y:parent.height/2
                    height: parent.height/2;width: parent.width/3
                    Text {
                        id:kuponItem
                        property string kuponNumber: "Купон не<br> применён"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: kuponNumber
                    }
                }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/3;y:parent.height/2
                    height: parent.height/2;width: parent.width/3
                    Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: "Подитог<br>2500.00 руб."
                    }

                }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/1.5;y:parent.height/2
                    height: parent.height/2;width: parent.width/3

                    MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        bar.haveOrder = false

                        modalChoseOrder.open()
                    }

                    }

                    Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: "Оплатить"
                    }

                }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/1.5
                    height: parent.height/2;width: parent.width/3
                    MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        bar.haveOrder = true
                        stackView.pop(null)
                    }

                    }

                    Text {
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.fill: parent
                        text: "Сохранить и<br> вернуться позже"
                    }

                }
            }
        }
    }
    Rectangle{
        width: parent.width/2
        x:parent.width/2
        anchors.top: parent.top
        anchors.bottom:plitka.top
        Rectangle{
            border.color: "gray"
            border.width: 3
            height: parent.height
            anchors.left: parent.left
            anchors.right: parent.right
            Image {width: parent.height;height: parent.height; source: "qrc:/images/images/icons/lupa.png"}
            TextInput{id:input; anchors.left: parent.left;anchors.leftMargin: parent.height;anchors.right: parent.right;height: parent.height;font.pointSize: 50;clip: true
            }
        }
    }
    Rectangle{
        id:plitka
        property string screen : "categories"
        anchors.right: parent.right
        x:parent.width/2.5
        y:parent.height/7
        width: parent.width/2
        height: parent.height*0.856
        ConfigurationList{visible:input.focus ? true: false}
        CategoryElement{
            anchors.fill: parent
            visible: input.focus === false  && plitka.screen === "categories" ? true : false
        }

        GridView{
            id:myGrid
            anchors.fill: parent
            visible: input.focus === false && plitka.screen === "products" ? true : false
            cellWidth: parent.width/3; cellHeight: parent.height/3
            model:Products{}
            delegate: Column{
                Rectangle{
                    width: myGrid.cellWidth
                    height: myGrid.cellHeight
                    color: name === "Назад" ? "tomato":"silver"
                    border.color:"gray"
                    MouseArea{anchors.fill: parent
                        onClicked:
                            if(name === "Назад"){
                                plitka.screen = "categories"
                            }
                            else{
                                modal.open()
                            }
                        }
                    Text{
                        width: myGrid.cellWidth
                        height: myGrid.cellHeight
                        text: name
                        font.pointSize: 25
                        verticalAlignment: Text.AlignVCenter; horizontalAlignment: Text.AlignHCenter}
                }}
        }


    }
    Modal{id:sale
        anchors.fill: parent
        Rectangle{
        anchors.centerIn: parent

        width: 500
        height: 200

        ListView{
        anchors.fill: parent
        model: ListModel{
            ListElement{name:"Скидка для сотрудников"}
            ListElement{name:"Скидка на парвый заказ"}
            ListElement{name:"Скидка по промокоду  Brazzers Rullez"}
            ListElement{name:"Скидка По промокоду PornoRoll"}


        }
        delegate: Rectangle{
            height: 50
        width: 500
        border.color: "black"
        MouseArea{
        anchors.fill: parent
        onClicked: sale.close()}
        Text {
            font.pointSize: 25
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            text: name  }
        }



        }

        }

    }
    Modal{id:kupon
        anchors.fill: parent
        Rectangle{
            anchors.centerIn: parent
            width: 400
            height: 180
            Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter

                font.pointSize: 20
                width: 400
                height: 35
                text: "Введите номер купона"
            }
            Rectangle{
                y:35
                border.color: black
                width: 400
                height: 80
                TextInput{
                    inputMethodHints:         Qt.ImhDigitsOnly
                    anchors.margins: 1
                    id:inputKupon
                    x:60
                    maximumLength: 8
                    width: 400
                    height: 80
                    font.pointSize: 44
                }}
            Rectangle{
                color:"lightgreen"
                y:115
                height: 100
                width: 400
                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        kuponItem.kuponNumber = "Применен купон<br>№"+inputKupon.text
                        inputKupon.text = ""
                        kupon.close()
                    }

                }
                Text {
                    color: "white"
                    font.pointSize: 44
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: "ПРИМЕНИТЬ"
                }
            }
        }
    }

    MouseArea{width: parent.width/2
        height: parent.height/2
        onClicked:{
            input.focus = false}
        visible:input.focus ? true: false}





    Modal{
        id:modal
        anchors.fill: parent
        Rectangle{
            anchors.centerIn: parent
            width: parent.width/2
            height: parent.height
            ModifiersGrid{
                anchors.fill: parent
                id:modifiers}
        }
    }

    Modal{id:modalChoseOrder;anchors.fill: parent
        Rectangle{
            anchors.centerIn: parent
            width: 260
            height: 200

            Oplata{ x:10;y:10;color: "mediumpurple";textBut: "Наличными"
            }
            Oplata{ x:10;y:100;color: "orange";textBut: "Картой" }

        }
    }
    Modal{id:confrim
        anchors.fill: parent
        Rectangle{
            anchors.centerIn: parent
            width: 400
            height: 250
            Text {
                verticalAlignment:  Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent;anchors.bottomMargin: 50; text: "Заказ успешно сконфигурирован"
            }


            Button{anchors.fill: parent;anchors.topMargin: 200;background: Rectangle{anchors.fill: parent; color: "greenyellow"}
                text: "На главный экран"
                onClicked: stackView.pop(null)}

        }
    }


    Modal{id:zdachia
        anchors.fill: parent

        Rectangle{
            MouseArea{
                anchors.fill: parent

              }
            anchors.centerIn: parent
            width: 400
            height: 250
            Text {width: 400;height: 100
                font.pointSize: 20
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Стоимость заказа 2000"}
            Text{ width: 200;height: 100; y:50;
                font.pointSize: 20
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Сумма от клиента"}
            Rectangle{
                width: 200;height: 50; y:75;x:200;border.color: "silver"

                TextInput{
                    id:clientDal
                    anchors.fill: parent
                    font.pointSize: 20
                    verticalAlignment: Text.AlignVCenter
                    maximumLength: 6
                    inputMethodHints:  Qt.ImhDigitsOnly
                    height: 100;width: 200;x:200;y:100
                    onContentSizeChanged: sdacha(clientDal.text)
                }}



            Text {
                id:sdachaDat
                y:100
                font.pointSize: 20
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                width: 400
                height:100
                text: "Сдача"


            }
            Button{
                width: 400;height:50
                y:200
                background: Rectangle{

                    color:sdachaDat.text !== "Сдача"? "greenyellow":"tomato" ;anchors.fill: parent}
                font.pointSize: 30
                text:sdachaDat.text === "Сдача" ?"Внесите сумму" :"Принять"
                onClicked: {
                    if(sdachaDat.text !== "Сдача"){
                    zdachia.close()
                    skolkoPerson.open()
                    }
                }

            }
        }
    }
    SkolkoPerson{id:skolkoPerson}
    function sdacha(text){

        var sdachaa = Number(clientDal.text) - 2000
        console.log(sdachaa)
        if(sdachaa>-1){
            sdachaDat.text = "Сдача: "+sdachaa
        }
        else
        {
        sdachaDat.text = "Сдача"
        }

    }

}

