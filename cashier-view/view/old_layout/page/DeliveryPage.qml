import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle{
    anchors.fill: parent
    Text {y:50
        x:parent.width/2-100
            width: 200
        font.pointSize: 32
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Выбор адрес достаки"}
    AdressItem{inputName: "Улица";x:parent.width/2-200;y:100}
    AdressItem{inputName: "Дом";x:parent.width/2-200;y:150}
    AdressItem{inputName: "Корпус";x:parent.width/2-200;y:200}
    AdressItem{inputName: "Подъезд";x:parent.width/2-200;y:250}
    AdressItem{inputName: "Домофон";x:parent.width/2-200;y:300}
    AdressItem{inputName: "Этаж";x:parent.width/2-200;y:350}
    AdressItem{inputName: "Квартира";x:parent.width/2-200;y:400}

    Button{
        x:20
        y:parent.height-60
        width: 100
        height: 40
        background: Rectangle{
        anchors.fill: parent
        color: "tomato"
        }
        onClicked: stackView.pop()
        text: "Назад"
    }
    Button{
        x:parent.width-120
        y:parent.height-60
        width: 100
        height: 40
        background: Rectangle{
        anchors.fill: parent
        color: "fuchsia"
        }
        onClicked:{
                stackView.push("qrc:/layout/page/FindByPhoneClient.qml")

        }
        text: "Далее"
    }

}
