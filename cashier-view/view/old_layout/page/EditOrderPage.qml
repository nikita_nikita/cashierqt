import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Rectangle {
    Component.onCompleted: {
        bar.haveOrder = true
        //marker !  При печати чека bar.haveOrder = false
    }

    MouseArea{width: parent.width/2
        height: parent.height/2
        onClicked:{
            console.log("211121")
            input.focus = false}
        visible:input.focus ? true: false}

    anchors.fill: parent
    color: "red"
    Rectangle{

        border.color: "gray"
        border.width: 3
        width: parent.width/2
        height: parent.height
        Rectangle{

            border.color: "gray"
            id:listRec
            width: parent.width
            height: parent.height-80
            ListView{
                clip: true
                anchors.fill: parent
                anchors.topMargin: 2
                anchors.rightMargin: 2
                model:UserOrderListModel{}
                delegate:UserOrderListElement{}
            }
        }
        Rectangle{
            color: "gray"
            anchors.bottom: parent.bottom
            height: 80
            width: parent.width
            Grid{

                anchors.fill:parent
                columns: 2
                rows: 2
            Rectangle{
               height: parent.height/2;width: parent.width/2
               border.color: "gray"
               MouseArea{
               anchors.fill: parent
               onClicked: {kupon.open()}
               }
               Text {
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    text: "Купон"
                }
            }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/2
                     height: parent.height/2;width: parent.width/2
                     MouseArea{
                     anchors.fill: parent
                     onClicked: {sale.open()}
                     }
                Text {
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    text: "Скидка<br>20%"
                }
                }
                Rectangle{
                    border.color: "gray"
                    y:parent.height/2
                    height: parent.height/2;width: parent.width/2
                Text {
                    id:kuponItem
                    property string kuponNumber: "Купон не<br> применён"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    text: kuponNumber
                }
                }
                Rectangle{
                    border.color: "gray"
                    x:parent.width/2;y:parent.height/2
                    height: parent.height/2;width: parent.width/2
                Text {

                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    text: "Подитог<br>2500.00 руб."
                }
                }
}


        }
    }
    Rectangle{
        width: parent.width/2
        x:parent.width/2
        anchors.top: parent.top
        anchors.bottom:plitka.top
        Rectangle{
            border.color: "gray"
            border.width: 3
            height: parent.height
            anchors.left: parent.left
            anchors.right: parent.right
            Image {width: parent.height;height: parent.height; source: "qrc:/images/images/icons/lupa.png"}
            TextInput{id:input; anchors.left: parent.left;anchors.leftMargin: parent.height;anchors.right: parent.right;height: parent.height;font.pointSize: 50;clip: true
            }
        }
    }
    Rectangle{
        id:plitka
        property string screen : "categories"
        anchors.right: parent.right
        x:parent.width/2.5
        y:parent.height/7
        width: parent.width/2
        height: parent.height*0.856
        ConfigurationList{visible:input.focus ? true: false}
        CategoryElement{
            anchors.fill: parent
            visible: input.focus === false  && plitka.screen === "categories" ? true : false
        }

        GridView{
            id:myGrid
            anchors.fill: parent
            visible: input.focus === false && plitka.screen === "products" ? true : false
            cellWidth: parent.width/3; cellHeight: parent.height/3
            model:Products{}
            delegate: Column{
                Rectangle{
                    width: myGrid.cellWidth
                    height: myGrid.cellHeight
                    color: name === "Назад" ? "tomato":"silver"
                    border.color:"gray"
                    MouseArea{anchors.fill: parent
                        onClicked:
                            if(name === "Назад"){
                                plitka.screen = "categories"
                            }
                            else{
                                modal.open()
                            }
                    }

                    Text{
                        width: myGrid.cellWidth
                        height: myGrid.cellHeight
                        text: name
                        font.pointSize: 25
                        verticalAlignment: Text.AlignVCenter; horizontalAlignment: Text.AlignHCenter}
                }}
        }


    }
Modal{id:sale
anchors.fill: parent
    Rectangle{
anchors.centerIn: parent
width: 600
height: 400
Rectangle{
    color: "tomato"
width: parent.width/2
height: parent.height/2
Text {
    font.pointSize: 32
    color: "white"
    anchors.fill: parent
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    text: "Скидка по возрасту"
}
}
Rectangle{

    color: "blue"
    x:parent.width/2
    width: parent.width/2
    height: parent.height/2
    Text {
        font.pointSize: 32
        color: "white"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Скидка для сотрудников"
    }
}
Rectangle{
    color: "red"
    y: parent.height/2
    width: parent.width/2
    height: parent.height/2
    Text {
        font.pointSize: 32
        color: "white"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Скидка в день рождения"
    }
}
Rectangle{
    color: "green"
    x:parent.width/2;y:parent.height/2
    width: parent.width/2
    height: parent.height/2
    Text {
        font.pointSize: 32
        color: "white"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "Скидка по акции"
    }
}
}

}
Modal{id:kupon
    anchors.fill: parent
    Rectangle{
    anchors.centerIn: parent
    width: 400
    height: 180
    Rectangle{
        border.color: black
        width: 400
        height: 80
    TextInput{
        id:inputKupon
        x:60
        maximumLength: 8
        width: 400
    height: 80
    font.pointSize: 44
    }}
    Rectangle{
        color:"lightgreen"
    y:80
    height: 100
    width: 400
    MouseArea{
    anchors.fill: parent
    onClicked:{
        kuponItem.kuponNumber = "Применен купон<br>№"+inputKupon.text
        kupon.close()
    }

    }
    Text {
        color: "white"
        font.pointSize: 44
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "ПРИМЕНИТЬ"
    }
    }
    }
}

MouseArea{width: parent.width/2

        height: parent.height/2
        onClicked:{
            console.log("211121")
            input.focus = false}
        visible:input.focus ? true: false}

    Button{
        width: 60
        height: 40
        background: Rectangle{
            radius: 10
            anchors.fill:parent
        color: "purple"}
        y:parent.height - 50
        x:parent.width -80
        onClicked: {
        bar.haveOrder = false

            stackView.pop()

        }
        Text{
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "white"
            text:  "Принять"
        }
    }
    Modal{

        id:modal
        anchors.fill: parent

        Rectangle{
            anchors.centerIn: parent
        width: parent.width/2
        height: parent.height
        ModifiersGrid{
            anchors.fill: parent
            id:modifiers}
        }
        }


}
