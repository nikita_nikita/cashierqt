import QtQuick 2.0
import  QtQuick.Layouts 1.12
Rectangle {
    property string elemcolor
    property string elemtext
            MouseArea{
                anchors.fill: parent
               onClicked: {
                plitka.screen = "products"
               }
            }
            color: elemcolor
            border.color: "gray"
            border.width: 1.5
            Text {
                Layout.leftMargin: 10
                color: "black"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                height: parent.height
                width: parent.width
                text: elemtext
            }
            width: parent.width/3
            height: parent.height/3

}
