    import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import "../page/newOrder"
import "../../components/links"
Rectangle {
    anchors.fill: parent
    color: "grey"
Label{
    y:20
    font.pointSize: 44
    horizontalAlignment: Text.AlignHCenter
    text: "Введите Мобильный Номер Клиента"
width: parent.width
}
    Rectangle{
        border.color: "#ffd85d"
        border.width: 3
        width: parent.width/2
        height: 80
        y:80
        x:parent.width/2 - parent.width/4
        TextInput{
            font.pointSize: 44
            id: phoneInput
            inputMethodHints: Qt.ImhDigitsOnly
            maximumLength:10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            onFocusChanged: {
                if(focus){
                inputMask = "8 (###) ### ## ##"
                }
            }
        }}
    Button{
    font.pointSize: 44
    y:170
    width: parent.width/3
    x:parent.width/2-parent.width/3-40
    text: "Пропустить"
    background: Rectangle{
        color: "tomato"
    }
    onClicked:{stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")}
    }
    Button{
    font.pointSize: 44
    y:170
    width: parent.width/3
    x:parent.width/2+40
    text: "Далее"
    background: Rectangle{
        color: "yellowgreen"
    }
    onClicked:{stackView.push("qrc:/layout/page/AddGuest.qml")}
    }

}


