import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
Rectangle {
    Rectangle{
        border.color: "#d5d5d5"
        border.width: 1
        color: "white"
        anchors.top: parent.top
        height: parent.height
        anchors.left: parent.left
        width: parent.width/5
        Text {anchors.fill: parent
            horizontalAlignment:  Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: "Имя"}
    }
    Rectangle{border.color: "#d5d5d5"
        border.width: 1
        anchors.top: parent.top
        height: parent.height
        anchors.left: parent.left
        anchors.leftMargin: parent.width/3
        anchors.right: parent.right
        TextInput {id: inputid

            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter}
    }
}
