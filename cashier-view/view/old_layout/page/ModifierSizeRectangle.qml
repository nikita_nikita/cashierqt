import QtQuick 2.0

Rectangle{
    property string name
    width: 120
    height: 100
    radius: 10
    border.color: size.inUse === name? "blue" : "gray"
    border.width: size.inUse === name? 5 : 2.5
    MouseArea{anchors.fill: parent
    onClicked:size.inUse = name
    }
    Text{
        width: parent.width/3
        height: parent.height - 10
        anchors.left: parent.left
        anchors.leftMargin: 5
        text: name
        font.pointSize: 14
        verticalAlignment: Text.AlignBottom; horizontalAlignment: Text.AlignLeft}
}
