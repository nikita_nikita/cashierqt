import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle{
    color:"#fcfcfc"
    radius: 10
    border.color: "silver"
    border.width: 2
    visible: false
    x:parent.width/4
    width: parent.width/2
    height: parent.height*0.9
    Label{color: "black"
        id:modifierLabel
        anchors.top: parent.top
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.leftMargin: 2
        width: parent.width-50
        height: parent.height/10
        text: "Модификаторы"
        font.pointSize: 22
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
    Rectangle{y:12;x:parent.width-30
        width: 26
        height: 26
        MouseArea{
            anchors.fill: parent
            onClicked: modifiers.visible = false }
        Image {anchors.fill: parent
            source: "qrc:/images/images/icons/cross.png"
        }}
    Rectangle{
        id:poloska
        width: parent.width
        height: 1
        anchors.top:parent.top
        anchors.topMargin: 50
        color: "silver"
    }

    Rectangle{
        anchors.top: poloska.bottom
        anchors.bottom: parent.height-80
        color:"#fbfafb"
    }


    ScrollView{
        anchors.fill: parent
    Label{
        anchors.top: parent.bottom
        anchors.topMargin: 55
        anchors.left: parent.left
        anchors.leftMargin: 10
        font.pointSize: 15
        color: "black"
        text: "Размер"}
    GridView{
        snapMode: GridView.NoHighlightRange

        id:size
        clip: true
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top:poloska.bottom
        anchors.topMargin: 85
        property string inUse: "smal"
        anchors.fill: parent
        visible: input.focus === false && plitka.screen === "products" ? true : false
        cellWidth: parent.width/4; cellHeight: parent.height/4

        model:SizeList{}
        delegate: Column{
            Rectangle{
                width: size.cellWidth-15
                height: size.cellHeight-15
                radius: 10
                border.color: size.inUse === name? "blue" : "gray"
                border.width: size.inUse === name? 5 : 2.5
                MouseArea{anchors.fill: parent
                    onClicked:size.inUse = name
                }
                Text{
                    width: size.cellWidth
                    height: size.cellHeight-20
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    text: name
                    font.pointSize: 10
                    verticalAlignment: Text.AlignBottom; horizontalAlignment: Text.AlignLeft}
            }}
    }

    Label{
        id:dopLable
        anchors.top:parent.top
        anchors.topMargin: parent.height/3+30
        anchors.left: parent.left
        anchors.leftMargin: 10
        font.pointSize: 15
        color: "black"
        text: "Дополнительно"}
    GridView{
        clip: true
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top:parent.top
        anchors.topMargin: parent.height/3+60
        id:dop
        anchors.fill: parent
        visible: input.focus === false && plitka.screen === "products" ? true : false
        cellWidth: parent.width/4; cellHeight: parent.height/4
        model:DopolnitelnoList{}
        delegate: Column{
            Rectangle{
                anchors.top: parent.top
                anchors.topMargin: 15
                width: dop.cellWidth-15
                height: dop.cellHeight-15
                radius: 10
                border.color:  "gray"
                border.width: 1
                MouseArea{
                anchors.fill: parent
                onClicked: count++
                }
                Text {
                    visible: count === 0 ? false : true
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: count
                    font.pointSize: 60
                    color: "black"
                }
                Rectangle{
                visible: count === 0 ? false : true
                color: "red"
                width: 30
                height: 30
                radius: 30
                x:parent.width-15
                y:-15
                MouseArea{

                    anchors.fill: parent
                    onClicked: {
                    if(count !==0){count--}

                    }
                }
                Text {
                    color: "white"
                    font.pointSize: 20
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: "-"
                }
                }
                Text{
                    width: dop.cellWidth
                    height: dop.cellHeight-20
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    text: name
                    font.pointSize: 12
                    verticalAlignment: Text.AlignBottom; horizontalAlignment: Text.AlignLeft}
            }}
    }}

    Rectangle{
        id:footer
        width: parent.width
        height: 80
        border.width: 2
        border.color: "silver"
        anchors.bottom: parent.bottom

        Rectangle{
            id: ok
            height: parent.height
            width: parent.width/3
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 2
            anchors.topMargin: 2
            anchors.bottomMargin: 2
            color: "white"

            Text {verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 17
                text: "Итого: 11,10 RUB";color: "black";anchors.fill: parent
            }
        }
        Rectangle{
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.4
            anchors.top: parent.top
            anchors.topMargin: parent.height/5
            height: parent.height/1.5
            width: parent.width/4
            radius: parent.height/10
            color: "#ebe9eb"
            border.color: "gray"
            MouseArea{
                anchors.fill: parent
                onClicked: modifiers.visible = false }
            Text {verticalAlignment: Text.AlignVCenter;horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pointSize: 17
                text: "Отменить"
            }
        }
        Rectangle{
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.68
            anchors.top: parent.top
            anchors.topMargin: parent.height/5
            height: parent.height/1.5
            width: parent.width/3.5
            radius: parent.height/10
            color: "#46b66b"
            border.color: "silver"
            MouseArea{
                anchors.fill: parent
                onClicked: modifiers.visible = false }
            Text {verticalAlignment: Text.AlignVCenter;horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pointSize: 17
                color: "white"
                text: "Добавить в чек"
            }
        }
    }

}
