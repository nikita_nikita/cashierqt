import QtQuick 2.0

Rectangle{
    property string name
    property int count: 0
    width: 120
    height: 100
    radius: 10
    border.color:  "gray"
    border.width: 1
    MouseArea{
    anchors.fill: parent
    onClicked: count++
    }
    Text {
        visible: count === 0 ? false : true
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: count
        font.pointSize: 60
        color: "black"
    }
    Rectangle{
    visible: count === 0 ? false : true
    color: "red"
    width: 30
    height: 30
    radius: 30
    x:parent.width-15
    y:-15
    MouseArea{

        anchors.fill: parent
        onClicked: {
        if(count !==0){count--}

        }
    }
    Text {
        color: "white"
        font.pointSize: 20
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "-"
    }
    }
    Text{
        anchors.fill: parent
        anchors.topMargin: 85
        anchors.left: parent.left
        anchors.leftMargin: 5
        text: name
        font.pointSize: 12
        verticalAlignment: Text.AlignBottom; horizontalAlignment: Text.AlignLeft}
}
