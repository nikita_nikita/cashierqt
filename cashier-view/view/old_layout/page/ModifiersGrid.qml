import QtQuick 2.0
import QtQuick.Controls 2.12


Rectangle{
    id:my
    color:"#fcfcfc"
    radius: 10



    Rectangle{
     width: parent.width
     height: 50
    Label{color: "black"
    id:modifierLabel
    anchors.top: parent.top
    anchors.topMargin: 2
    anchors.left: parent.left
    anchors.leftMargin: 2
    width: parent.width-50
    height: parent.height
    text: "Модификаторы"
    font.pointSize: 22
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
}
Rectangle{y:12;x:parent.width-30
    width: 26
    height: 26
    MouseArea{
        anchors.fill: parent
        onClicked: modal.close() }
    Image {anchors.fill: parent
        source: "qrc:/images/images/icons/cross.png"
    }}}
Rectangle{
    id:poloska
    width: parent.width
    height: 2
    anchors.top:parent.top
    anchors.topMargin: 55
    color: "silver"
}
Flickable{
clip: true
anchors.fill: parent
anchors.topMargin: 55
contentHeight: 1100
contentWidth: parent.width
width: parent.width
Label{
    id:sizeLabel
    width: 100
    height: 30
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.leftMargin: 10
    font.pointSize: 15
    color: "black"
    text: "Размер"}
Grid{
    anchors.left: parent.left
    anchors.leftMargin: 20
    id:size
    columns: 3
    spacing: 20
    width: 600
    height: 100
    property string inUse
    anchors.top: sizeLabel.bottom
    ModifierSizeRectangle{name:"Маленькая"}
    ModifierSizeRectangle{name:"Средняя"}
    ModifierSizeRectangle{name:"Большая"}
    }


Label{
    id:dopLabel
    width: 100
    anchors.top: size.bottom
    anchors.topMargin: 10
    anchors.left: parent.left
    anchors.leftMargin: 10
    font.pointSize: 15
    color: "black"
    text: "Дополнительно"
    }

    Grid{
        id:dop
        anchors.left: parent.left
        anchors.leftMargin: 20
        width: 600
        height: 1100
        spacing: 40
        anchors.top: dopLabel.bottom
        anchors.topMargin: 10
        columns: 3
        rows: 6
        ModifiersDopRectagle{name:"Первый"}
        ModifiersDopRectagle{name:"Второй"}
        ModifiersDopRectagle{name:"Третий"}
        ModifiersDopRectagle{name:"Четвертый"}
        ModifiersDopRectagle{name:"Первый"}
        ModifiersDopRectagle{name:"Второй"}
        ModifiersDopRectagle{name:"Третий"}
        ModifiersDopRectagle{name:"Четвертый"}
        ModifiersDopRectagle{name:"Первый"}
        ModifiersDopRectagle{name:"Второй"}
        ModifiersDopRectagle{name:"Третий"}
        ModifiersDopRectagle{name:"Четвертый"}
        ModifiersDopRectagle{name:"Первый"}
        ModifiersDopRectagle{name:"Второй"}
        ModifiersDopRectagle{name:"Третий"}
        ModifiersDopRectagle{name:"Четвертый"}

    }
}
Rectangle{
    id:footer
    width: parent.width
    height: 80
    border.width: 2
    border.color: "silver"
    anchors.bottom: parent.bottom

    Rectangle{
        id: ok
        height: parent.height
        width: parent.width/3
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: 2
        anchors.topMargin: 2
        anchors.bottomMargin: 2
        color: "white"

        Text {verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 17
            text: "Итого: 11,10 RUB";color: "black";anchors.fill: parent
        }
    }
    Rectangle{
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.4
        anchors.top: parent.top
        anchors.topMargin: parent.height/5
        height: parent.height/1.5
        width: parent.width/4
        radius: parent.height/10
        color: "#ebe9eb"
        border.color: "gray"
        MouseArea{
            anchors.fill: parent
            onClicked: modal.close()}
        Text {verticalAlignment: Text.AlignVCenter;horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pointSize: 17
            text: "Отменить"
        }
    }
    Rectangle{
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.68
        anchors.top: parent.top
        anchors.topMargin: parent.height/5
        height: parent.height/1.5
        width: parent.width/3.5
        radius: parent.height/10
        color: "#46b66b"
        border.color: "silver"
        MouseArea{
            anchors.fill: parent
            onClicked: modal.close() }
        Text {verticalAlignment: Text.AlignVCenter;horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pointSize: 17
            color: "white"
            text: "Добавить в чек"
        }
    }
}

}
