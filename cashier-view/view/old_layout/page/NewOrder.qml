import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
Rectangle {
    id:guest
    color: "#414241"
    Layout.fillHeight: true
    Layout.fillWidth: true
    property bool delivery

    Rectangle {
        id:pickupButton
        color: 'teal'
        x:parent.height/5-100
        y:parent.height/2-40
        width: 150
        height: parent.height/6
        MouseArea{
            anchors.fill: parent
            onClicked: {
                bar.delivery = false
                stackView.push("qrc:/layout/page/TimePage.qml")
            }
            Text {
                color: "white"
                font.pointSize: 25
                anchors.centerIn: parent
                text: "Самовывоз"
            }}}
        Rectangle {
            id:deliveryButton
            x:parent.width/2+parent.height/5
            y:parent.height/2-40
            width: 150
            height: parent.height/6
            color: 'plum'
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    bar.delivery = true
                    stackView.push("qrc:/layout/page/TimePage.qml")
                }
            }
            Text {
                color: "white"
                font.pointSize: 25
                anchors.centerIn: parent
                text: "Доставка"
            }
        }

        Rectangle{
            id:barButton
            color: 'teal'
            x:parent.width/2
            y:parent.height/2-40
            width: 150
            height: parent.height/6
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    bar.delivery = false
                    stackView.push("qrc:/layout/page/TimePage.qml")
                }
                Text {
                    color: "white"
                    font.pointSize: 25
                    anchors.centerIn: parent
                    text: "Самовывоз"
                }}}


    }
