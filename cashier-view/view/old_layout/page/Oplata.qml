import QtQuick 2.0

Rectangle{
    property string textBut
    width:240;height: 80;
   Text {
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: "white"
        font.pointSize: 20
        text: textBut
    }
   MouseArea{
   anchors.fill: parent
   onClicked:
   {
       modalChoseOrder.close()

       if(textBut === "Наличными"){
            zdachia.open()
       }
       else{
           stackView.push("qrc:/layout/page/FindByPhoneClient.qml")

       }}

   }
}
