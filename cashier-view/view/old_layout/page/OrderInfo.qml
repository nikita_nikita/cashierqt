import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../components/links"
import "newOrder"
Rectangle{
    Layout.fillHeight: true
    Layout.fillWidth: true
    color: "#414241"

    NewOrderHeader{id:newOrderFooter}
    Rectangle{
        anchors.top: newOrderFooter.bottom
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 80
        id:first
        width: parent.width/2
        color: "#414241"
        CheckList{}
    }
    Text {
        x:50
        y:100
        font.pointSize: 20
        text: "Лапша <br>
Модиф:Лапша рисовая<br>
      Курица: Удвоить<br>
Лапша <br>
Модиф:Лапша Гречневая<br>
      Курица: Удвоить<br>
Пицца
"
    }
    Rectangle{
        id:second
        anchors.top: newOrderFooter.bottom
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 95
        color: "#414241"
        anchors.left: first.right
        width: parent.width/2
        UserDataInput{}
    }
    Modal{
        id:modal
        anchors.centerIn: parent
        Rectangle{
            border.color: "silver"
            border.width: 3
            width: 400
            height: 200
            Label{
                font.pointSize: 20
                color: "black"
                text: "Подтвердите выбор"
                width: parent.width
                height: parent.height/2
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Button{
                font.pointSize: 20
                anchors.bottom: parent.bottom
                height: parent.height/2
                width: parent.width/2
                text: "Принять"
                background: Rectangle{
                    anchors.fill: parent
                    color: "greenyellow"
                }
                onClicked: stackView.pop(null)
            }
            Button{
                font.pointSize: 20
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                height: parent.height/2
                width: parent.width/2
                text: "Отменить"
                background: Rectangle{
                    anchors.fill: parent
                    color: "tomato"
                }
                onClicked: modal.close()
            }
        }
    }
    ModalChange{
        id:modalChange
        anchors.fill: parent

        Rectangle{
            anchors.centerIn: parent
            border.color: "black"
            width: 400
            height: 100
            TextInput{
                font.pointSize: 15
                id:editText
                width: 400
                height: 50
                text: modalChange.myText
            }
            Button{
                anchors.bottom: parent.bottom
                width: 200
                height: 50
                text: "OK"
                background: Rectangle{anchors.fill: parent
                    color: "green"}
                onClicked: {}
            }

        }


    }


    NewOrderFooter{}
    Modal{
        id:sostavChange
        anchors.fill: parent
        Rectangle{
            width: 400
            height: 400
            TextInput{
                height: 350
                width: 400
            }
        }
    }
}

