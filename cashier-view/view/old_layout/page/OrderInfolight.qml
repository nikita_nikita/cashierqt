import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle {
    
    anchors.fill: parent
    color: "gray"
    Rectangle{color: "gray"
        width: parent.width/3
        height: parent.height
        Text {
            x:50
            y:10
            width: parent.width
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 15
            color: "white"
            text:  "Состав Заказа (Доставка ) "
        }
        
        Text{color: "white"
            x:50
            y:100
            font.pointSize: 20
            text: "Лапша <br>
Модиф:Лапша рисовая<br>
      Курица: Удвоить<br>
Лапша <br>
Модиф:Лапша Гречневая<br>
      Курица: Удвоить<br>
Пицца
"
        }
        
        Text {color: "white"
            x:20
            y:parent.height-100
            width: parent.width
            font.pointSize: 15
            height: 40
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "Стоимость заказа<br>875.00 руб.<b></>"
        }
    }
    Rectangle{
        x:parent.width/3-65
        y:0
        width: 60
        height: 40
        radius: 1
        color: "tomato"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                bar.change = true
                stackView.push("qrc:/layout/page/EditOrderPage.qml")
            }}
        Text {color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            text: "Ред-ть"
        }
    }
    
    Rectangle{
        color: "gray"
        anchors.fill: parent
        anchors.rightMargin: parent.width/3
        anchors.leftMargin: parent.width/3
        Text {color: "white"
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width
            height: 50
            text: "Информация о клеенте"
        }
        Text {color: "white"
            y:50
            x:20
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width
            text: "Имя: Иван"
        }
        Text {color: "white"
            y:80
            x:20
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width

            text: "Телефон: +7 904 346 88 33"
        }
        
        Text {color: "white"
            y:110
            x:20
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width
            text: "День Рождения: 15 Октября"
        }
        
        Text {color: "white"
            y:140
            x:20
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width

            text: "Коментарий к заказу"
        }
        
        Text {color: "white"
            y:170
            x:20
            font.pointSize: 15
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            width: parent.width
            text: "Без чеснока, моркови, имбиря..."
        }


        Rectangle{
            id:typeBut
            property string textBut: "Тип: Доставка"
            x:20
            y:parent.height-200
            width: 250
            height: 60
            color: "lightgreen"
            Text {
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 20
                color:"white"
                anchors.fill: parent
                text: typeBut.textBut
            }
            MouseArea{
            anchors.fill: parent
            onClicked: typeDel.open()
            }
            }
        Rectangle{
        id:statusBut
        property string textBut: "Статус: Принят"
        x:20
        y:parent.height-100
        width: 250
        height: 60
        color: "lightgreen"
        Text {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 20
            color:"white"
            anchors.fill: parent
            text: statusBut.textBut
        }
        MouseArea{
        anchors.fill: parent
        onClicked: status.open()
            }
        }

        Rectangle{
            y:0
            x:200
            width: 60
            height: 40
            color: "tomato"
            MouseArea{
                anchors.fill: parent
                onClicked:clientDrawer.open()
            }
            
            Text {
                color: "white"
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Ред-ть"}
            
        }
        
    }
    Rectangle{
        
        color: "gray"
        id:adres
        anchors.fill: parent
        width: parent.width/3
        anchors.leftMargin: parent.width/1.5
        height: 50
        
        Label{
            font.pointSize: 15; color: "white";
            width: parent.width/2
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            id:adresLabel
            text: "<b>АДРЕС</b>"}
        

        Text {font.pointSize: 15; color: "white";x: 20;y:50;text: "Улица: Попова"}
        Text {font.pointSize: 15; color: "white";x: 20;y:80;text: "Дом: 21"}
        Text {font.pointSize: 15; color: "white";x: 20;y:110;text: "Корпус: 2"}
        Text {font.pointSize: 15; color: "white";x: 20;y:140;text: "Подъезд: 2"}
        Text {font.pointSize: 15; color: "white";x: 20;y:170;text: "Этаж: 4"}
        Text {font.pointSize: 15; color: "white";x: 20;y:200;text: "Кв-ра: 33"}
        Text {font.pointSize: 15; color: "white";x: 20;y:230;text: "Примеч. Какой чудесный день"}
        Text {font.pointSize: 15; color: "white";x: 20;y:260;text: "Курьер: не задан"}
        Text {font.pointSize: 15; color: "white";x: 20;y:290;text: "Комент. к заказу: "}
        Text {font.pointSize: 15; color: "white";x: 20;y:320;text: "Англоговорящий клиент"}


    }
    Rectangle{
        color: "black"
        height: parent.height
        x:parent.width/2
    }
    
    
    Rectangle{
        width: 60
        height: 40
        x:parent.height*2-150
        color: "tomato"
        border.color: black
        Text {
        color: "white"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        text: "Ред-ть"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:adresDrawer.open() }
    }
    
    
    AdressDrawerChange{
        width: parent.width/2
        height: parent.height
        id:adresDrawer
        
    }
    ClientDrawer{
        width: parent.width/2
        height: parent.height
        id:clientDrawer
    }
    
    Button{
        x:parent.width-120
        y:parent.height-60
        width: 100
        height: 40
        background: Rectangle{
        anchors.fill: parent
        color: "fuchsia"
        }
        onClicked: stackView.pop()
        text: "Принять"
    }

    Modal{id:status;anchors.fill: parent
        Rectangle{
        anchors.centerIn: parent
        width: 260
        height: 500

       Statusbutton{ x:10;y:10;color: "mediumpurple";textBut: "Готовится" }
       Statusbutton{ x:10;y:100;color: "orange";textBut: "Готов" }
       Statusbutton{ x:10;y:200;color: "mediumorchid";textBut: "Курьер назначен" }
       Statusbutton{ x:10;y:300;color: "mediumpurple";textBut: "Передан курьеру" }
       Statusbutton{ x:10;y:400;color: "orange";textBut: "Доставлен" }


        }
    }
    Modal{id:typeDel;anchors.fill: parent
        Rectangle{
        anchors.centerIn: parent
        width: 260
        height: 200

       Statusbutton{ x:10;y:10;color: "mediumpurple";textBut: "Самовывз" }
       Statusbutton{ x:10;y:100;color: "orange";textBut: "Доставка" }


        }
    }

}

