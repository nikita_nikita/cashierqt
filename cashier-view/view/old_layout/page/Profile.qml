import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtPositioning 5.13
Grid {
  width: parent.width
  columns: 4
  rows:2
  rowSpacing: 20
  columnSpacing: 20
  spacing: 2
}
