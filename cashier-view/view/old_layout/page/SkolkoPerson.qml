import QtQuick 2.0

Modal{
    anchors.fill: parent
    Rectangle{
        anchors.centerIn: parent
        width: 400
        height: 180
        Text {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

            font.pointSize: 20
            width: 400
            height: 35
            text: "Количество персон"
        }
        Rectangle{
            y:35
            border.color: black
            width: 400
            height: 80
            TextInput{
                inputMethodHints:         Qt.ImhDigitsOnly
                anchors.margins: 1
                id:inputKupon
                x:60
                maximumLength: 8
                width: 400
                height: 80
                font.pointSize: 44
            }}
        Rectangle{
            color:"lightgreen"
            y:115
            height: 100
            width: 400
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    skolkoPerson.close()
                    stackView.push("qrc:/layout/page/FindByPhoneClient.qml")}

                }

            }
            Text {
                color: "white"
                font.pointSize: 44
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "ПРИМЕНИТЬ"
            }
        }
    }
