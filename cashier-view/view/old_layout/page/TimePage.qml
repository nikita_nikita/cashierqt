import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle {
anchors.fill: parent

Text {y:100
    x:parent.width/2-100
        width: 200
    font.pointSize: 32
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    text: bar.delivery ? "К какому времени доставить":"К какому времени приготовить"}
Rectangle{
    y:200
    width: 200
    height: 80
    x:parent.width/2-100
    border.color: "black"

TextInput{
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    font.pointSize: 32
    anchors.fill: parent
    inputMethodHints:         Qt.ImhDigitsOnly
    onFocusChanged: {
    if(focus){
inputMask = "##.##"
    }}

}

}
Button{
    x:parent.width-120
    y:parent.height-60
    width: 100
    height: 40
    background: Rectangle{
    anchors.fill: parent
    color: "fuchsia"
    }
    onClicked:{
        if(bar.delivery)
    stackView.push("qrc:/layout/page/DeliveryPage.qml")
    else{
         stackView.push("qrc:/layout/page/ConfigurationOrderPage.qml")
        }
    }
    text: "Далее"
}


Button{
    x:20
    y:parent.height-60
    width: 100
    height: 40
    background: Rectangle{
    anchors.fill: parent
    color: "tomato"
    }
    onClicked: stackView.pop()
    text: "Назад"
}
}
