import QtQuick 2.0

Component {
   id: guestOrderList
  Rectangle{
      height: topping ? 100 : 40
      anchors.left: parent.left
      anchors.right: parent.right
      Text {y:5;x:20;horizontalAlignment: Text.AlignLeft; verticalAlignment: Text.AlignVCenter; font.pointSize: 25; width: parent.width*0.6; text: name}
      Text {y:5; x:parent.width*0.6;width: parent.width*0.4; text: price + "*"+count+" = "+price*count+" руб."; font.pointSize: 25; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignLeft;}
      Text {y:35;x:50; text:toppings; visible: topping ? true:false}
  }
}
