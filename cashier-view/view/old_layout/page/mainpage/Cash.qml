import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Column {
    id: root_component
    width:  300
   Rectangle {
       color: "#4CAF50"
        width: root_component.width
        height: 50

        Label{
            id: label_texts
            text: "Касса"
            anchors.centerIn: parent
            font.pixelSize: 20
            font.weight: Font.Bold
            height: 30
        }
   }
   Button {
       width: parent.width
       height: 50
       text: "Открыть кассовую смену"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }
   Button {
       width: parent.width
       height: 50
       text: "Заказы"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }
   Button {
       width: parent.width
       height: 50
       text: "Команды фискальному регистратору"
       Material.background: Material.Grey
       Material.theme: Material.Light

   }
   Button {
       width: parent.width
       height: 50
       text: "Чек коррекции"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }

}
