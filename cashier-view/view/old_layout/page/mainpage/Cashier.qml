import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Column {
    id: root_component
    width:  200
   Rectangle {
       color: "#9C27B0"
        width: root_component.width
        height: header_column.height+40

        Column {
            id: header_column
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.centerIn: parent
            spacing: 10
            Label{
                id: label_texts
                text: "Кассир(Юбилейный)"
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 15
                font.weight: Font.Bold
                height: 30
            }
            Label{
                text:  "смена открыта"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label{
                text: "26.6.2019 18:00"
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
   }
   Button {
       width: parent.width
       height: 50
       text: "Закрыть смену"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }

}
