import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Column {
    id: root_component
    width:  300
   Rectangle {
       color: "#2196F3"
        width: root_component.width
        height: 50

        Label{
            id: label_texts
            text: "Склад"
            anchors.centerIn: parent
            font.pixelSize: 20
            font.weight: Font.Bold
            height: 30
        }
   }
   Button {
       width: parent.width
       height: 50
       text: "Списать"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }
   Button {
       width: parent.width
       height: 50
       text: "Перемещение"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }
   Button {
       width: parent.width
       height: 50
       text: "Принять"
       Material.background: Material.Grey
       Material.theme: Material.Light

   }
   Button {
       width: parent.width
       height: 50
       text: "Заявки"
       Material.background: Material.Grey
       Material.theme: Material.Light
   }

}
