import QtQuick 2.0
ListView {
     anchors.fill: parent
     model: ProfileElementInNavigationBar
     delegate: Row {
         Image {
             id: name
             source: "qrc:/images/images/icons/profile1.png"
         }
         Text { text:  FIO}
     }
 }
