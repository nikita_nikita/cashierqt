import QtQuick 2.0
import QtQuick.Layouts 1.12



ListView {
    Component {
        id: my

        GridLayout{
            visible: (toolbar.dateCount % 2
                      || dontaccepted.use && status === "Неподтвержденные"
                      ||indelivery.use && status === "В пути"
                      ||neworder.use && status === "Новое"
                      ||cooking.use && status === "Готовится"
                      ||done.use && status === "Готово"
                      ||close.use && status === "Закрыто"
                      ||cenceled.use && status === "Отменён"
                      ||deliveryButton.focus === true && delivery === false && allButton.focus === false
                      ||deliveryButton.focus !== true && delivery === true && allButton.focus === false) ? false : true
            height:
                (  dontaccepted.use && status === "Неподтвержденные"
                  ||indelivery.use && status === "В пути"
                  ||neworder.use && status === "Новое"
                  ||cooking.use && status === "Готовится"
                  ||done.use && status === "Готово"
                  ||close.use && status === "Закрыто"
                  ||cenceled.use && status === "Отменён"
                 ||deliveryButton.focus === true && delivery === false && allButton.focus === false
                 ||deliveryButton.focus !== true && delivery === true && allButton.focus === false) ? 0 : 80
            columnSpacing: 0
            columns: 2
            GridLayout{
                rows:  2
                columns: 2
                columnSpacing: 0
                rowSpacing: 0
                width: 120
                height: 80
                Rectangle{
                    id: leftTop
                    color: "#70ceea";  width: 70; height: 50
                    Image{  source: statusImage;width: 50;height: 50}
                }
                Rectangle{color: "#70ceea"; width: 50; height: 50
                    Text {text:"№ "+ number  }
                }
                Rectangle{
                    width: 120
                    height: 30
                    Layout.columnSpan: 2
                    color:"#6ac4df"
                    Text {text: qsTr(status)
                    }
                }
            }
            GridLayout{
                anchors.top: parent.top
                rows: 2
                columnSpacing: 0
                rowSpacing: 0
                width: 800
                height: 80
                Rectangle{
                    id: secondleftTop
                    color: "#fcced0";  width: 1000; height: 50
                    Text {text: qsTr(time);id:ordertime;anchors.left: parent.left; width:50 }
                    Text {text: qsTr(adress);id:orderadress; anchors.leftMargin: 10;anchors.left: ordertime.right; width: 230}
                    Text {text: qsTr(client);id: orderclient; anchors.leftMargin: 10;anchors.left: orderadress.right; width: 230}
                    Text {text: qsTr(comment);id: ordercomment; anchors.leftMargin: 10;anchors.left: orderclient.right; width: 230}
                    Text {text: qsTr(coast + "руб."); anchors.leftMargin: 10;anchors.left: ordercomment.right; width: 230}
                }


                Rectangle{
                    anchors.top: secondleftTop.bottom
                    anchors.left: secondleftTop.left
                    width: secondleftTop.width
                    height: 30
                    color:"#eaddd5"
                    Text {text: qsTr( productName)
                    }
                }
            }
        }}


    model: OrderList {}
    delegate: my
    focus: true
}






