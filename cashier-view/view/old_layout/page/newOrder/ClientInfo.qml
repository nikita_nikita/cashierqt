import QtQuick 2.0
import QtQuick.Controls 2.12
Rectangle {
    property string mytext
    property alias inputText: inputid.text
    property int textSize
    id:nameRec
    border.color: "#e7ddd5"
    border.width: 10
    height: 50
    anchors.left: parent.left
    anchors.right: parent.right
    Rectangle{
        border.color: "#d5d5d5"
        border.width: 1
        color: "#e7ddd5"
        anchors.top: parent.top
        height: 50
        anchors.left: parent.left
        width: parent.width/3.5
        Text {
            font.pointSize: textSize
            anchors.fill: parent
            horizontalAlignment:  Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: mytext}
    }
    Rectangle{border.color: "#d5d5d5"
        border.width: 1
        color: "#e9e9ef"
        anchors.top: parent.top
        height: 50
        anchors.left: parent.left
        anchors.leftMargin: parent.width/3.5
        anchors.right: parent.right

        MouseArea{
        anchors.fill: parent
        onClicked: {

        }
        }
        TextInput{
            id: inputid
            color: "black"
            font.pointSize: textSize
            text: inputText
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter}

}
}
