import QtQuick 2.0
import QtQuick.Controls 2.12

Button{
    property int number
    property string mytext
    width: 60
    height: 70
    anchors.top: parent.top
    anchors.topMargin: 10
    onClicked:{
        modal.open()
}
    background: Rectangle{
    color:"#414241" }
    Rectangle{
        id:buttonNumber
        width: 30
        height: 30
        radius: 30
        anchors.left: parent.left
        anchors.leftMargin: 15
        color: "#414241"
        border.color: "#808080"
        anchors.top: parent.top
        Text {color: "#808080"
            anchors.fill: parent
            horizontalAlignment:  Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: number
        }
    }
    Rectangle{
    color:"#414241"
    anchors.top: buttonNumber.bottom
    height: 30
    width: parent.width
    Text {color: "#808080"
        anchors.fill: parent
        horizontalAlignment:  Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: mytext
    }}

     }
