import QtQuick 2.0
import "../models"
Rectangle {
    anchors.left: parent.left
    anchors.right: parent.right
    height: parent.height
    width: parent.width/3
    color: "red"
    Component {
         id: contactDelegate
         Item {
             width: parent.width/2; height: 40
             Column {
                 Text { text: '<b>Name:</b> ' + name }
                 Text { text: '<b>Number:</b> ' + number }
             }
         }
     }

    ListView {
        anchors.fill: parent
        model: NotificationModel {}
        delegate: contactDelegate
        highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        focus: true
    }}
//}
