import QtQuick 2.0
import QtQuick.Controls 2.12

Pane {

  Column {
      anchors.top: parent.top
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.topMargin: 50
      spacing: 20
      Label {
          font.pointSize: 20
          text: "Введите пин код или просканируйте карту"
          anchors.horizontalCenter: parent.horizontalCenter
      }


      TextField {
          anchors.horizontalCenter: parent.horizontalCenter
          inputMethodHints: Qt.ImhDigitsOnly
          onAccepted: loginController.loginByPin("123456")
      }
  }



}
