import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import QtQuick.Controls.Material 2.12
import QtQml 2.0


import "../components"
import "../old_page"
import "../old_components/general"
import "../old_components/main"



ApplicationWindow {
    id: window
    visible: true
    // mareker!
    // for Ancroid
    //    width: screen.width
    //    height: screen.height
    // for Desktop
    width:  1200
    height:  600
    onClosing: {
        close.accepted = false;
        return;
    }
    title: "Кассир"
    color: "red"
    header: ToolBar{
        background: Rectangle{
            color: "white"
        }
        visible:  !loginForm.visible
        Material.foreground: "black"
        Material.background: "white"
        id: toolbar
        property int dateCount: 8
        RowLayout {
            id:topBar
            spacing: 20
            anchors.fill: parent
            ToolButton {
                icon.source: "qrc:/images/view/images/icons/drawer"
                onClicked:  drawer.open()
            }
            TabBar {
                id: bar
                property string name
                property string lastname
                property string phone
                property bool delivery
                property bool haveOrder: false
                property bool change: false
                clip: true
                height: parent.height
                Material.theme: Material.Dark
                Material.accent: "black"
                spacing: 0
                padding: 0
                Layout.fillHeight: true
                width: childrenRect.width
                TabButton {
                    height: parent.height+8
                    id:allButton
                    width: 150
                    text: qsTr("Все")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: allButton.focus ? "#dfe384" : "white"}
                }
                TabButton {
                    height: parent.height+10
                    id:deliveryButton
                    width: 150
                    text: qsTr("Доставка")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: deliveryButton.focus ? "#dfe384" : "white"}
                }

                TabButton {
                    height: parent.height+10
                    id:pickupButton
                    width: 150
                    text: qsTr("Самовывоз")
                    onClicked: stackView.toMainPage()
                    background: Rectangle{
                        color: pickupButton.focus ? "#dfe384" : "white"}
                }
            }
        }
        NotificationPanel{
        anchors.left: parent.left
        anchors.leftMargin: 500
        }



    }


    MyDrawer{
        id:drawer
        width: Math.min(window.width, window.height) / 3 * 2
        height: window.height
    }

    OrderFilter{
        id:filter
        height: 60
        width: parent.width
    }
    MyStackView{
        id:stackView
        anchors.top:filter.bottom
        anchors.topMargin: 60}
    MyDrawer{}

   /* LogInPage {
        id:loginForm
        anchors.fill: parent
        visible: loginController.shiftStatus
    }*/
    Button{
        text: "Заглушка"
        x:parent.width-100
        y:parent.height-100

    }


}


