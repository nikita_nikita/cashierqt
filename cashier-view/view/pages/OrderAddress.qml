import QtQuick 2.0
import "../components/orders/"
import "../components/main/"
Item {
    AddressInputList{
        id:address
        anchors.centerIn: parent}

    ToPrevPageButton{
    }
    ToNextPageButton{
        onClicked: {
            orderController.orderObject.address.streetText = address.streetText
            orderController.orderObject.address.building = address.buildingText
            orderController.orderObject.address.entrance = parseInt(address.entranceText)
            orderController.orderObject.address.floor = parseInt(address.floorText)
            orderController.orderObject.address.room = parseInt(address.roomText)
            orderController.orderObject.address.commnt = address.commentText
            orderController.setAddressToOrder()
        }
    }
}
