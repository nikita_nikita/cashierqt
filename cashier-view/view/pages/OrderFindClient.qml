import QtQuick 2.0
import QtQuick.Controls 2.12
import "../components/Client"
import "../components/main"
Item {    

    ToPrevPageButton{}
    FindClientByPhone{
        width: parent.width/2
        height: 200
        onLoadClientList:{
            clientController.findClientByPhone()
        }
    }
    Button{
        width: parent.width/2
        height: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        background: Rectangle{
            anchors.fill: parent
            color: "#6666CC"
        }
        font.pointSize: 25
        text:"Пропустить"
        onClicked: router.toProductChoice()
    }
    ListView{
        clip: true
        anchors.fill: parent
        anchors.leftMargin: parent.width/2
        model: clientController.client_result_list_model
        delegate: FindClientComponent{
            phone: client_phone
            name: client_name
            onClientChoice:{
                console.log(index)
                clientController.choiceClient(index)
            }
        }
    }


}
