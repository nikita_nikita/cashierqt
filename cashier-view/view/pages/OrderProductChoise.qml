import QtQuick 2.0
import "../components/orders"
Item{

    // Список товаров в "чеке"
    ProductInOrderList{
        id:orderList
        orderModel: orderController.orderProductListModel
        anchors.fill: parent
        anchors.rightMargin:  parent.width/2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
    }
    MenuProductEditor{
        id:menuEditor
        anchors.left: parent.left
        anchors.bottom: orderList.bottom
        anchors.bottomMargin: 60
        width: parent.width/2
        height: 60
    }

    //Нижнаяя панель со скидками, купонами  и суммой
    BottomBar{
        orderPrice: orderController.orderProductListModel.orderCost
        orderPayd: orderController.orderPayd
        anchors.left: parent.left
        anchors.top: menuEditor.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: parent.width/2
    }
    //
    ProductSearch{
        id:input
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.leftMargin: parent.width/2
        height: 40
        onProductSerachTextChange: orderController.nameVisibleProduct.findName = inputText
    }

    // Список найденных продукутов
    ProductList{
        id:filterProductList
        anchors.top:input.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: input.left
        anchors.bottomMargin: 140
        visible: orderController.nameVisibleProduct.visible
        model:orderController.nameVisibleProduct
    }
    BottomStatusPanel{
    anchors.top: filterProductList.bottom
    anchors.left: filterProductList.left
    anchors.right: filterProductList.right
    anchors.bottom: parent.bottom
    statusModel: orderController.status_list_model
    }

    //Квадрат скрывающий поиск продуктов ( располагается на левой половине экрана)


    //    Rectangle{
    //    visible: filterProductList.visible
    //    color: "gray"
    //    anchors.fill: parent
    //    anchors.rightMargin: parent.width/2
    //    opacity: 0.7
    //    MouseArea{
    //    anchors.fill: parent
    //    onClicked:    orderController.nameVisibleProduct.findName = ""

    //    }
    //    }

    MofificationsPanel{
        anchors.fill: parent
        model: orderController.modificationGroups
        visible: orderController.modificationPanelStatus
    }

    PaymentTypePanel{
        anchors.fill: parent
        visible: orderController.paymentTypePanelStatus
        onClose: orderController.closePaymentpanel()
    }
    PaymentSdachaComponent{
        anchors.fill: parent
        visible: orderController.paymentPanelStatus
        onClose: orderController.closePaymentPanel()
//        onVisibleChanged: paymentType = orderController.getPaymentType()
    }



}
