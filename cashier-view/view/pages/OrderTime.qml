import QtQuick 2.0
import "../components/orders/"
import "../components/main/"
Item {
TimeInputComponent{
id:timeInput
anchors.centerIn: parent
}
ToPrevPageButton{
onClicked: router.toDeliveryTipe()
}
ToNextPageButton{
onClicked: {
if(timeInput.text !== ""){
orderController.orderObject.time = timeInput.text
}
else{
orderController.orderObject.time = "К ближайшему времени"
}
if(orderController.orderObject.type === 0){
router.toOrderDeliveryAddress()
}
else{
router.toOrderFindClient()
//router.toProductChoice()
}

}
}
}
