import QtQuick 2.0
import "../components/orders"
Item {
    Row{
        anchors.centerIn: parent
        spacing: 40
        DeliveryTypeButton{
            deliveryType:0
            visible: orderController.getAllow_delivery()
            onChoice:{
                orderController.createNewOrder(deliveryType)

            }

        }
        DeliveryTypeButton{
            deliveryType:1
            visible: orderController.getAllow_pickUp()
            onChoice:{
                orderController.createNewOrder(deliveryType)

            }
        }
        DeliveryTypeButton{
            visible: orderController.getAllow_inRoom()
            deliveryType:2
            onChoice:{
                orderController.createNewOrder(deliveryType)

            }
        }
    }
}
