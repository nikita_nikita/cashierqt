import QtQuick 2.0
import QtQuick.Controls 2.12
import "../components/orders"

//для себя
//из 3-х частей
// 1. Список продуктов
// 2. инфо о клиенте
// 3. инфо о адресе

Rectangle {

    color: "gray"
    ProductsInOrderUpdateComponent{
        anchors.fill: parent
        anchors.rightMargin: parent.width/1.5
        productModel: orderController.orderProductListModel
    }
    OrderClientInfo{
        anchors.fill: parent
        anchors.rightMargin: parent.width/3
        anchors.leftMargin: parent.width/3
        onShowStatusPanel: orderController.statusChangeOrderPanelVisible = true
    }

    AddressInOrderUpdateComponent{
        visible: orderController.orderObject.delivery === 1 ? true : false
        anchors.fill: parent
        anchors.left: parent.left
        anchors.leftMargin: parent.width/1.5
        street:  orderController.orderObject.address.streetText
        house:   orderController.orderObject.address.building
        corpus:  orderController.orderObject.address.entrance
        floor:   orderController.orderObject.address.floor
        room:    orderController.orderObject.address.room
        comment: orderController.orderObject.address.comment
    }

   Rectangle{       
       visible: orderController.orderObject.delivery === 1 ? false : true
       Text {
            anchors.centerIn: parent
           font.pointSize: 40
           text: "Без доставки"
       }
        anchors.fill: parent
        anchors.left: parent.left
        anchors.leftMargin: parent.width/1.5
    }


    ChangeOrderStatusPanel{
        onClose:     orderController.statusChangeOrderPanelVisible = false
        statusModel: orderController.status_list_model
        visible:     orderController.statusChangeOrderPanelVisible
    }
}
