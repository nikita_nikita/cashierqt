import QtQuick 2.12
import QtQuick.Layouts 1.12
import "../components/orders"
Item {
    ColumnLayout {
        spacing: 0
        anchors.fill: parent
        //фильтр по статусам
        FilterStatus{
            Layout.fillWidth: true
        }
        //список заказов
        OrderList {
            //            model: orderController.order_model
            model:orderController.orderFilterModel
            Layout.fillWidth: true
            Layout.fillHeight: true

        }
    }
    Rectangle{

        color:"red"
        width: 100
        height: 30
        x:parent.width-110
        y:parent.height-50
        Text {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            text: "Новый заказ"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                router.toDeliveryTipe()
            }
        }
    }


    SaveOrderSwitchButton{
        onShowSaveOrderList: orderController.setNotProcessedOrderModalVisible(true)
    }
    SaveOrderModal{
        saveOrderModal: orderController.saveOrderProxyModel
        visible:orderController.notProcessedOrderModalVisible
        onClose: orderController.setNotProcessedOrderModalVisible(false);
    }
}
